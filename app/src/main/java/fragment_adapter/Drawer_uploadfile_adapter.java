package fragment_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.trifft.admin.quichelp.Drawer_uploadfile;
import com.trifft.admin.quichelp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 1/9/2017.
 */
public class Drawer_uploadfile_adapter extends BaseAdapter {

    TextView filenametxt,filetypetxt,filesizetxt;
    Button viewbtn;
    Context context;
    LayoutInflater in;
    ArrayList<HashMap<String,String>> arrylist;
    HashMap<String,String> hashmap=new HashMap<String, String>();

    public Drawer_uploadfile_adapter(Context context, ArrayList<HashMap<String, String>> array) {
        this.context=context;
        arrylist=array;
    }

    @Override
    public int getCount() {
        return arrylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arrylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        in=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=in.inflate(R.layout.drawer_uploadfile_adapter,parent,false);
        filenametxt=(TextView)v.findViewById(R.id.filename);
        //filetypetxt=(TextView)v.findViewById(R.id.filetype);
        //filesizetxt=(TextView)v.findViewById(R.id.filesize);
        viewbtn=(Button)v.findViewById(R.id.view);
        hashmap=arrylist.get(position);

        filenametxt.setText(hashmap.get(Drawer_uploadfile.FILENAME));
        //filetypetxt.setText(hashmap.get(Drawer_uploadfile.FILETYPE));
        //filesizetxt.setText(hashmap.get(Drawer_uploadfile.FILESIZE));

        return v;
    }
}
