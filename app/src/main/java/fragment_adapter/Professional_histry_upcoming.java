package fragment_adapter;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.fragment.app.Fragment;

/**
 * Created by aduser on 12/12/2017.
 */

public class Professional_histry_upcoming extends Fragment {
    ListView list;
    public static String JOD_ID="jod_id",JOB_STATUS="job_status",
            DATE="date",TIME="time",PRICE="price",PAYMENT_STATUS="payment_status";

    String USER_ID= Serviceselectionpage.USER_ID;
    TextView empty_text;
    String JOB_status;
    Professional_upcoming_listadapter adapter;
    ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.drawer_call_history_upcoming,container,false);
        list=(ListView)v.findViewById(R.id.list);
        empty_text=(TextView)v.findViewById(R.id.empty_text);




        if(USER_ID==null)
        {
//list.setVisibility(View.GONE);
            empty_text.setVisibility(View.VISIBLE);
            empty_text.setText("Sign in your application");
        }
        else
        {

            new JobHistory().execute(USER_ID);
        }



        //new JobHistory().execute("24");

       /*list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

                MenuInflater mnu=actionMode.getMenuInflater();
                mnu.inflate(R.menu.message_menu,menu);


                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.delete:

                        Toast.makeText(getActivity(), "item removed", Toast.LENGTH_LONG).show();

                        actionMode.finish();
                        return true;


                    default:
                        break;
                }

                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }
        });*/





        return v;
    }


    String status="",message="";
    private class JobHistory extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(String... voids) {

            String result = null;

            String path=getActivity().getResources().getString(R.string.url_service)+"prof/Prof_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            //http://www.quichelp.com/json/prof/Prof_Json/data?service=prof_get_all_jobs&userid=9

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"prof_get_all_jobs"+"&userid="+voids[0],null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status",status);Log.e("message",message);


            ArrayList<HashMap<String,String>> arrayList=new ArrayList<HashMap<String, String>>();
            if(status.equals("true"))
            {
                Log.e("upcoming","loop1");

                JSONArray array= null;
                JSONArray array_get= null;
                try {
                    array = jsn_response.getJSONArray("alljobs");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for(int i=0;i<array.length();i++)
                {

                    Log.e("upcoming","loop2");

                    try {
                        array_get=array.getJSONArray(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for(int j=0;j<array_get.length();j++)
                    {
                        Log.e("upcoming","loop3");

                        JSONObject object;
                        try {
                            object =array_get.getJSONObject(j);

                            JOB_status=object.getString("job_status");
                           /* if(JOB_status.equals("new") || JOB_status.equals("open")) {
*/
                                HashMap<String, String> map = new HashMap<String, String>();

                                map.put("jod_id", object.getString("job_id"));
                                map.put("job_status", object.getString("job_status"));
                                map.put("date", object.getString("from_date"));
                                map.put("time", object.getString("from_time"));
                                //map.put("price", object.getString("price"));
                               // map.put("payment_status", object.getString("payment_status"));


                                arrayList.add(map);

                           // }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.e("JSONException",e.toString());
                        }
                    }



                }

            }

            return arrayList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true")) {

                adapter = new Professional_upcoming_listadapter(getActivity(), aVoid);
                list.setAdapter(adapter);



                if(adapter.isEmpty())
                {
                    empty_text.setVisibility(View.VISIBLE);
                    empty_text.setText("No Jobs generated");
                }
            }
        }
    }

}
