package fragment_adapter;

import com.trifft.admin.quichelp.Driverbooknow;
import com.trifft.admin.quichelp.Driverprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by MINE on 07-08-2017.
 */

public class Driver_adapter extends FragmentPagerAdapter {

    ViewPager ac;
    public Driver_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return new Driverprovide();
            case 1:
                return new Driverbooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}

