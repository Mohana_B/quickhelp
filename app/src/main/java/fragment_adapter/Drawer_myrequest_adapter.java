package fragment_adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;



/**
 * Created by Admin on 1/8/2017.
 */

public class Drawer_myrequest_adapter extends FragmentPagerAdapter {
    public Drawer_myrequest_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return new Drawer_myrequest_upcoming();
            case 1:
                return new Drawer_myrequest_history();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
