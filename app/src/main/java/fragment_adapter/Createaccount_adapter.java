package fragment_adapter;

import com.trifft.admin.quichelp.Createaccount_professional;
import com.trifft.admin.quichelp.Createaccount_users;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by Admin on 11/12/2016.
 */

public class Createaccount_adapter extends FragmentPagerAdapter {
    public Createaccount_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new Createaccount_users();
            case 1:
                return new Createaccount_professional();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
