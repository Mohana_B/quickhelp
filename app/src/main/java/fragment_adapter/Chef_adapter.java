package fragment_adapter;


import com.trifft.admin.quichelp.Chefbooknow;
import com.trifft.admin.quichelp.Chefprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Chef_adapter extends FragmentPagerAdapter {


    ViewPager chef;
    public Chef_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return new Chefprovide();
            case 1:
                return new Chefbooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
