package fragment_adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.trifft.admin.quichelp.Drawer_myrequest_upcoming_list_details;
import com.trifft.admin.quichelp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 1/8/2017.
 */
public class Drawer_myrequest_upcoming_listadapter extends BaseAdapter implements View.OnClickListener {
    TextView jobidtxt,jobstatustxt,datetxt;
    Button actionbtn;
    Context context;
    LayoutInflater in;
    ArrayList<HashMap<String,String>> arrylist;
    HashMap<String,String> hashmap=new HashMap<String, String>();


    public Drawer_myrequest_upcoming_listadapter(Context context, ArrayList<HashMap<String, String>> array)  {
        this.context=context;
        arrylist=array;
    }

    @Override
    public int getCount() {
        return arrylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arrylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        in=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=in.inflate(R.layout.drawer_callhistory_upcoming_listadapter,parent,false);
        jobidtxt=(TextView)v.findViewById(R.id.jobid);
        jobstatustxt=(TextView)v.findViewById(R.id.jobstatus);
        datetxt=(TextView)v.findViewById(R.id.date);
        actionbtn=(Button)v.findViewById(R.id.details);

        hashmap=arrylist.get(position);

        jobidtxt.setText(hashmap.get(Drawer_myrequest_upcoming.JOBID));
        jobstatustxt.setText(hashmap.get(Drawer_myrequest_upcoming.JOBSTATUS));
        datetxt.setText(hashmap.get(Drawer_myrequest_upcoming.DATE));

        actionbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context,Drawer_myrequest_upcoming_list_details.class);
                context.startActivity(in);
            }
        });

        return v;
    }

    @Override
    public void onClick(View v) {

    }
}

