package fragment_adapter;

import com.trifft.admin.quichelp.Cleaningbooknow;
import com.trifft.admin.quichelp.Cleaningprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Cleaning_adapter extends FragmentPagerAdapter {

    ViewPager page;
    public Cleaning_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return  new Cleaningprovide();
            case 1:
                return new Cleaningbooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
