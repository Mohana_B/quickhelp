package fragment_adapter;

import com.trifft.admin.quichelp.Paintingbooknow;
import com.trifft.admin.quichelp.Paintingprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by Admin on 11/10/2016.
 */

public class Paintingadapter extends FragmentPagerAdapter {
    ViewPager pger;
    public Paintingadapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new Paintingprovide();
            case 1:
                return new Paintingbooknow();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 2;
    }

}
