package fragment_adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
/**
 * Created by aduser on 12/12/2017.
 */

public class Professional_history_swipe_adapter extends FragmentPagerAdapter {
    public Professional_history_swipe_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:

                return new Professional_histry_upcoming();
            case 1:
                return new Professional_history_history();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
