package fragment_adapter;

import com.trifft.admin.quichelp.Servicelanding_Professionals;
import com.trifft.admin.quichelp.Servicelandingusers;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Servicelandingadapter extends FragmentPagerAdapter {

    public Servicelandingadapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new Servicelandingusers();
            case 1:
                return new Servicelanding_Professionals();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
