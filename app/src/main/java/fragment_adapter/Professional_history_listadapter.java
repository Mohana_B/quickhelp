package fragment_adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.trifft.admin.quichelp.Professional_history_list_details;
import com.trifft.admin.quichelp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by aduser on 12/12/2017.
 */

public class Professional_history_listadapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> arrayList;

    HashMap<String,String> map=new HashMap<String, String>();


    public Professional_history_listadapter(Context activity, ArrayList<HashMap<String, String>> aVoid) {

        this.context=activity;
        this.arrayList=aVoid;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView= inflater.inflate(R.layout.drawer_callhistory_history_listadapter, null);

        TextView job_id=(TextView)rowView.findViewById(R.id.jobid);

        TextView job_date=(TextView)rowView.findViewById(R.id.date);

        TextView job_status=(TextView)rowView.findViewById(R.id.jobstatus);

        Button details=(Button)rowView.findViewById(R.id.details);


        map=arrayList.get(i);

        final String JOD_ID=map.get(Professional_history_history.JOD_ID);
        final String JOB_STATUS=map.get(Professional_history_history.JOB_STATUS);
       // final String PRICE=map.get(Professional_history_history.PRICE);
        String DATE=map.get(Professional_history_history.DATE);
        final String TIME=map.get(Professional_history_history.TIME);
        //final String PAYMENT_STATUS=map.get(Professional_history_history.PAYMENT_STATUS);


        job_id.setText(JOD_ID);

        SimpleDateFormat inSDF = new SimpleDateFormat("yyyy-mm-dd");
        SimpleDateFormat outSDF = new SimpleDateFormat("dd-mm-yyyy");

        try {
            Date date = inSDF.parse(DATE);
            DATE = outSDF.format(date);

            job_date.setText(DATE);
        } catch (ParseException ex){
        }

        job_status.setText(JOB_STATUS);


        details.setText("View");

        final String finalDATE = DATE;
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent in=new Intent(context,Professional_history_list_details.class);
                in.putExtra("jod_id",JOD_ID);
                in.putExtra("job_status",JOB_STATUS);
                in.putExtra("date", finalDATE);
                in.putExtra("time",TIME);
               // in.putExtra("price",PRICE);
               // in.putExtra("payment_status",PAYMENT_STATUS);
                context.startActivity(in);
            }
        });


        return rowView;



    }
}
