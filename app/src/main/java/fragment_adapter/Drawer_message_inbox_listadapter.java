package fragment_adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.trifft.admin.quichelp.Drawer_message_inbox;
import com.trifft.admin.quichelp.Drawer_message_inbox_reply;
import com.trifft.admin.quichelp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 12/30/2016.
 */

public class Drawer_message_inbox_listadapter extends BaseAdapter {

    private SparseBooleanArray mSelectedItemsIds;
    Context context;
    LayoutInflater inflater;

    ArrayList<HashMap<String,String>> array_list;
SparseBooleanArray sarray;
    HashMap<String,String> hash=new HashMap<String, String>();
    public Drawer_message_inbox_listadapter(Context context, ArrayList<HashMap<String,String>> arr) {
        this.context = context;
        this.array_list=arr;

        mSelectedItemsIds=new SparseBooleanArray();
    }

    @Override
    public int getCount() {
        return array_list.size();
    }

    @Override
    public Object getItem(int position) {
        return array_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

      LayoutInflater in=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = in.inflate(R.layout.drawer_message_inbox_listadapter, null);

        TextView subject= (TextView) v.findViewById(R.id.subject);
        TextView date= (TextView) v.findViewById(R.id.date);
        Button view=(Button)v.findViewById(R.id.view);
        Button reply=(Button)v.findViewById(R.id.reply);

        hash=array_list.get(position);


        subject.setText(hash.get(Drawer_message_inbox.SUBJECT));

        date.setText(hash.get(Drawer_message_inbox.DATE));


        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in=new Intent(context,Drawer_message_inbox_reply.class);
                context.startActivity(in);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        return v;
    }


    public void remove(int arr) {
Log.e("removing position ",String.valueOf(arr));
        Toast.makeText(context,String.valueOf(arr),Toast.LENGTH_LONG).show();
        array_list.remove(arr);

        notifyDataSetChanged();
    }

    /*public List<WorldPopulation> getWorldPopulation() {
        return worldpopulationlist;
    }*/

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));

        boolean bb=!mSelectedItemsIds.get(position);
                Log.e("boolean", String.valueOf(bb));

    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value) {

            Log.e("if_part",String.valueOf(value));

            mSelectedItemsIds.put(position, value);
        }
        else {

            Log.e("else_part",String.valueOf(value));

            mSelectedItemsIds.delete(position);
            notifyDataSetChanged();
        }
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {

        Log.e("array",mSelectedItemsIds.toString());

        return mSelectedItemsIds;
    }

   /* public ArrayList<HashMap<String,String>> getWorldPopulation() {
        return arrsy;
    }

    public void toggleSelection(int position) {
        selectView(position, !sarray.get(position));
    }

    //@Override
    public void remove(ArrayList<HashMap<String, String>> arr) {
        arrsy.remove(arr);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return arrsy.size();
    }

    public void selectView(int position, boolean value) {
        if (value)
            sarray.put(position, value);
        else
            sarray.delete(position);
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return sarray;
    }


   /* List<WorldPopulation> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;
    LayoutInflater inflater;
    Context context;




    public Drawer_message_inbox_listadapter(Context context, int resourceId, List<WorldPopulation> worldpopulationlist)
    {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View view, ViewGroup parent) {

            view = inflater.inflate(R.layout.drawer_message_inbox_listadapter, parent,false);
            // Locate the TextViews in listview_item.xml

          TextView subject= (TextView) view.findViewById(R.id.subject);
          TextView date= (TextView) view.findViewById(R.id.date);

        subject.setText(worldpopulationlist.get(position).getSubject());


        date.setText(worldpopulationlist.get(position).getDate());



        return view;
    }



    @Override
    public void remove(WorldPopulation object) {
        worldpopulationlist.remove(object);
        Log.e("array",object.toString());
        notifyDataSetChanged();
    }

    public List<WorldPopulation> getWorldPopulation() {
        return worldpopulationlist;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {

        Log.e("array",mSelectedItemsIds.toString());

        return mSelectedItemsIds;
    }*/

}