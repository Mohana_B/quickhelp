package fragment_adapter;


import com.trifft.admin.quichelp.Drawer_call_history_history;
import com.trifft.admin.quichelp.Drawer_call_history_upcoming;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by Admin on 12/21/2016.
 */

public class Callhistory_adapter extends FragmentPagerAdapter {
    public Callhistory_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:

                return new Drawer_call_history_upcoming();
            case 1:
                return new Drawer_call_history_history();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
