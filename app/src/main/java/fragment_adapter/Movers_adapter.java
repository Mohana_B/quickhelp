package fragment_adapter;

import com.trifft.admin.quichelp.Moversbooknow;
import com.trifft.admin.quichelp.Moversprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Movers_adapter extends FragmentPagerAdapter {


    ViewPager pager;
    public  Movers_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return  new Moversprovide();
            case 1:
                return new Moversbooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
