package fragment_adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.trifft.admin.quichelp.R;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * Created by Admin on 1/8/2017.
 */

public class Drawer_myrequest_upcoming extends Fragment {
    ListView list;
    String[] jobid={"C41","C42","C43"};
    String[] jobstatus={"New","New","New"};
    String[] date={"21-01-2017","02-02-2017","20-02-2017"};
    ArrayList<HashMap<String,String>> array=new ArrayList<HashMap<String,String>>();
    public static String JOBID="jobids";
    public static String JOBSTATUS="jobstatuss";
    public static String  DATE="dates";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.drawer_myrequest_upcoming,container,false);
        list=(ListView)v.findViewById(R.id.list);
        for(int i=0;i<jobid.length;i++)
        {
            HashMap<String,String> map=new HashMap<String, String>();
            map.put("jobids",jobid[i]);
            map.put("jobstatuss",jobstatus[i]);
            map.put("dates",date[i]);
            array.add(map);
        }
        Drawer_myrequest_upcoming_listadapter adapter=new Drawer_myrequest_upcoming_listadapter(getActivity(),array);
        list.setAdapter(adapter);
        return v;
    }
}
