package fragment_adapter;

import com.trifft.admin.quichelp.Acservicebooknow;
import com.trifft.admin.quichelp.Acserviceprovide;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;



/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Acservice_adapter extends FragmentPagerAdapter {

ViewPager ac;
    public Acservice_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return new Acserviceprovide();
            case 1:
                return new Acservicebooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
