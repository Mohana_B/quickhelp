package fragment_adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.trifft.admin.quichelp.R;

import androidx.annotation.NonNull;

/**
 * Created by Admin on 12/30/2016.
 */

public class Drawer_message_sent_listadapter extends ArrayAdapter<String> {

    private final Activity context;
    //String[] from;
    String[] subject;
    String[] date;
    public Drawer_message_sent_listadapter(Activity context, String[] subject, String[] date) {
        super(context, R.layout.drawer_message_sent_listadapter,subject);

        this.context=context;
        //this.from=from;
        this.subject=subject;
        this.date=date;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.drawer_message_sent_listadapter, null, true);
        //TextView fromtxt=(TextView)rowView.findViewById(R.id.from);
        TextView subjecttxt=(TextView)rowView.findViewById(R.id.subject);
        TextView datetxt=(TextView)rowView.findViewById(R.id.date);
         Button view=(Button)rowView.findViewById(R.id.view);
        //ImageView view=(ImageView)rowView.findViewById(R.id.circleview);

        // fromtxt.setText(from[position]);
        subjecttxt.setText(subject[position]);
        datetxt.setText(date[position]);
/*
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context,drawer_callhistory_upcoming_list_details.class);
                context.startActivity(in);
            }
        });*/
        return rowView;
    }
}
