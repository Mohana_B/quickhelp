package fragment_adapter;

import com.trifft.admin.quichelp.Drawer_message_inbox;
import com.trifft.admin.quichelp.Drawer_message_sent;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by Admin on 12/30/2016.
 */

public class Drawer_message_adapter extends FragmentPagerAdapter {
    public Drawer_message_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return new Drawer_message_inbox();
            case 1:
                return new Drawer_message_sent();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
