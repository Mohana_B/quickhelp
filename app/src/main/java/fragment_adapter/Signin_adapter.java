package fragment_adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.trifft.admin.quichelp.Sign_user;
import com.trifft.admin.quichelp.Signin_professional;

/**
 * Created by PSDeveloper on 11/10/2016.
 */

public class Signin_adapter extends FragmentStatePagerAdapter {
    public Signin_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return  new Sign_user();
            case 1:
                return  new Signin_professional();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
