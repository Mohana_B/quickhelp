package fragment_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trifft.admin.quichelp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 1/9/2017.
 */
public class Drawer_myrequest_history_listadapter extends BaseAdapter{

    TextView jobidtxt,jobstatustxt,datetxt,timetxt;
    Context context;
    LayoutInflater in;
    ArrayList<HashMap<String,String>> arrylist;
    HashMap<String,String> hashmap=new HashMap<String, String>();


    public Drawer_myrequest_history_listadapter(Context context, ArrayList<HashMap<String, String>> array) {

        this.context=context;
        arrylist=array;
    }

    @Override
    public int getCount() {
        return arrylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arrylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        in=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=in.inflate(R.layout.drawer_myrequest_history_listadapter,parent,false);
        jobidtxt=(TextView)v.findViewById(R.id.jobid);
        jobstatustxt=(TextView)v.findViewById(R.id.jobstatus);
        datetxt=(TextView)v.findViewById(R.id.date);
        timetxt=(TextView)v.findViewById(R.id.time);
        hashmap=arrylist.get(position);

        jobidtxt.setText(hashmap.get(Drawer_myrequest_history.JOBID));
        jobstatustxt.setText(hashmap.get(Drawer_myrequest_history.JOBSTATUS));
        datetxt.setText(hashmap.get(Drawer_myrequest_history.DATE));
        timetxt.setText(hashmap.get(Drawer_myrequest_history.TIME));

       return v;
    }
}
