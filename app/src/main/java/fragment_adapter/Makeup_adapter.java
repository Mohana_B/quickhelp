package fragment_adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.trifft.admin.quichelp.makeUpModule.MakeupBooknow;
import com.trifft.admin.quichelp.makeUpModule.MakeupProvide;

/**
 * Created by Fatima.t on 13/03/2020.
 */

public class Makeup_adapter extends FragmentPagerAdapter {

    ViewPager ac;

    public Makeup_adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new MakeupProvide();
            case 1:
                return new MakeupBooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
