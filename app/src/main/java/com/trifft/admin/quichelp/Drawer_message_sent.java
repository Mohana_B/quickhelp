package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import fragment_adapter.Drawer_message_sent_listadapter;

/**
 * Created by Admin on 12/30/2016.
 */

public class Drawer_message_sent extends Fragment {
    ListView list;
    //String[] from={"admin@servfy.com","amala@servfy.com","noble@servfy.com"};
    String[] subject={"New Cleaning request","New Cleaning request","New Cleaning request"};
    String[] date={"30-12-2016 1:08 AM","30-12-2016 1:08 AM","30-12-2016 1:08 AM"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.drawer_message_sent,container,false);
        list=(ListView)v.findViewById(R.id.list);
        Drawer_message_sent_listadapter adapter=new Drawer_message_sent_listadapter(getActivity(),subject,date);
        list.setAdapter(adapter);
        return v;
    }
}
