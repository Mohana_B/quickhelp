package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class Mainscreen extends AppCompatActivity {

    static String shared_preference="Default_Language";
LinearLayout german,singapore;
    SharedPreferences shared;
    String navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainscreen);

        german=(LinearLayout) findViewById(R.id.german);
        singapore=(LinearLayout) findViewById(R.id.singapore);

        shared=getSharedPreferences(shared_preference,MODE_PRIVATE);
        final SharedPreferences.Editor ed=shared.edit();

        navigation=shared.getString("Value",null);
        if(navigation!=null) {
            if (navigation.equals("CHENNAI")) {
                Intent n = new Intent(Mainscreen.this, Serviceselectionpage.class);
                startActivity(n);
                finish();
            } else if (navigation.equals("MUMBAI")) {
                Intent n = new Intent(Mainscreen.this, Serviceselectionpage.class);
                startActivity(n);
                finish();
            }

        }


        german.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent n=new Intent(Mainscreen.this,Serviceselectionpage.class);
                ed.putString("Value","CHENNAI").commit();
                startActivity(n);
                finish();

            }
        });

        singapore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n=new Intent(Mainscreen.this,Serviceselectionpage.class);
                ed.putString("Value","MUMBAI").commit();
                startActivity(n);
                finish();
            }
        });

    }


}
