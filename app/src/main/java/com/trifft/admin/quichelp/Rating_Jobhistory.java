package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.utils.DialogManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by PSDeveloper on 1/9/2017.
 */

public class Rating_Jobhistory extends AppCompatActivity implements View.OnClickListener {

    ImageView language;
    LinearLayout backArrow;
    Button send, cancel;
    String JOB_ID;
    RatingBar rating_bar;
    Float rating;
    String feed_back;
    EditText name, feedback;
    ProgressDialog progressDialog;
    String USER_ID = Serviceselectionpage.USER_ID;
    String FIRSTNAME = Serviceselectionpage.FIRSTNAME;

    private Rating_Jobhistory thisActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_jobhistory);
        thisActivity = this;

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        language = (ImageView) findViewById(R.id.language);
        name = (EditText) findViewById(R.id.name);
        feedback = (EditText) findViewById(R.id.feedback);
        send = (Button) findViewById(R.id.send);
        cancel = (Button) findViewById(R.id.cancel);
        rating_bar = (RatingBar) findViewById(R.id.ratingBar);


        JOB_ID = getIntent().getExtras().getString("job_id");
        Log.e("JOB_ID", JOB_ID);
        backArrow.setOnClickListener(this);

        cancel.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Rating_Jobhistory.this, language));

        name.setText(FIRSTNAME);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rating = rating_bar.getRating();
                feed_back = feedback.getText().toString();

                if (rating.equals("") || rating.doubleValue() == 0) {
                    Toast.makeText(Rating_Jobhistory.this, "Rate our service", Toast.LENGTH_LONG).show();
                } else if (feed_back.equals("") || feed_back.length() == 0) {
                    Toast.makeText(Rating_Jobhistory.this, "Enter your feedback", Toast.LENGTH_LONG).show();
                } else {
                    new JobRating().execute();
                }


            }
        });

    }

    String status = "", message = "";

    private class JobRating extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Rating_Jobhistory.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {


            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            /*https://quichelp.com/json/Booking_Json/data/?service=job_rate&userid=191&job_id=AC29&rating=4&feedback=great job*/

//            String path=getResources().getString(R.string.url_service)+"Clean_Json/data?"; // Old
            String path = getResources().getString(R.string.url_service) + "Booking_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "job_rate" + "&userid=" + USER_ID +
                        "&job_id=" + JOB_ID + "&rating=" + rating + "&feedback=" + feed_back, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.e("response", result);

            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);

                Log.d("json", jsn_response.toString());


                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("sss", status);


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();

            if (status.equals("true")) {

                DialogManager.showSingleBtnPopup(thisActivity, new DialogMangerCallback() {
                    @Override
                    public void onOkClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), Drawerlayout_callhistory.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        intent.putExtra("getPage", "history");
                        startActivity(intent);
                    }

                    @Override
                    public void onCancelClick(View view) {

                    }
                }, getString(R.string.alert), message, getString(R.string.ok));
            } else {
                Toast.makeText(Rating_Jobhistory.this, message, Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageArrow:
            case R.id.cancel:
                onBackPressed();
                break;

        }
    }
}
