package com.trifft.admin.quichelp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.trifft.admin.quichelp.activities.BaseActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.QuickHelpLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;


/**
 * Created by PSDeveloper on 1/6/2017.
 */

public class Getprice_page extends BaseActivity implements View.OnClickListener, CommonInterface {

    ImageView language;
    LinearLayout backArrow;
    String job_id, price;
    TextView jodid, prie_value;
    ProgressDialog progressDialog;
    Button paynow, payByCash;

    private ProgressDialog dialog;

    //https://www.instamojo.com/@quichelp1708/

    SharedPreferences get_id;
    public String USER_ID, FIRSTNAME, LASTNAME, EMAIL, PHONENUM;

    private Getprice_page thisActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cleaning_get_a_price);
        thisActivity = this;

        paynow = (Button) findViewById(R.id.b4);
        payByCash = findViewById(R.id.pay_by_cash);

        jodid = (TextView) findViewById(R.id.jod_id);
        prie_value = (TextView) findViewById(R.id.price);
        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        language = (ImageView) findViewById(R.id.language);


        get_id = getSharedPreferences("My_id", MODE_PRIVATE);
        USER_ID = get_id.getString("my_id", null);
        EMAIL = get_id.getString("email", "");
        FIRSTNAME = get_id.getString("Firstname", "");
        LASTNAME = get_id.getString("Lastname", "");
        PHONENUM = get_id.getString("phone_num", "");

        job_id = getIntent().getExtras().getString("job_id");
        price = getIntent().getExtras().getString("price");


        backArrow.setOnClickListener(this);
        payByCash.setOnClickListener(this);

        language.setOnClickListener(new Language_change(Getprice_page.this, language));


        jodid.setText(job_id);
        prie_value.setText(price);

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("please wait...");
        dialog.setCancelable(false);


        paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (USER_ID == null) {
                    login_sign_up_dialog();
                } else {

                    if (!isOnline()) {
                        Toast.makeText(getApplicationContext(), "No network connection", Toast.LENGTH_SHORT).show();
                    } else if (price.replace("INR", "").trim().equals("0")) {
                        Toast.makeText(getApplicationContext(), "Amount not valid", Toast.LENGTH_SHORT).show();
                    } else {
                        price = price.replace("INR", "");

                        /*callInstamojoPay("aasai.muthu@gmail.com","9894264440",price,
                                job_id,"Aaasai");*/

                        QuickHelpLog.e("EMAIL", EMAIL);
                        QuickHelpLog.e("PHONENUM", PHONENUM);
                        QuickHelpLog.e("price", price);

                        callInstamojoPay(EMAIL, "8939415346", price, job_id, FIRSTNAME);
                        
                    }
                }

            }
        });
    }

    private void login_sign_up_dialog() {

        LayoutInflater in = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vv = in.inflate(R.layout.paynow_page_dialog, null);
        final Dialog dialog = new Dialog(Getprice_page.this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(vv);

        LinearLayout ok = (LinearLayout) vv.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent n = new Intent(getApplicationContext(), Sign_activity.class);
                n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(n);
            }
        });

        dialog.show();


    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageArrow:
                onBackPressed();
                break;
            case R.id.pay_by_cash:
                APIRequestHandler.getInstance().payByCashJob("update_payment_state", "app", "cash", job_id, thisActivity, this);

                break;

        }

    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    InstapayListener listener;
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                QuickHelpLog.e("onSuccess", response);
                new Send_token().execute(response);
            }

            @Override
            public void onFailure(int code, String reason) {

                QuickHelpLog.e("onFailure", reason);
                Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }

    private class Send_token extends AsyncTask<String, Void, Void> {

        String status = "", message = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(Getprice_page.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... voids) {

            String result = null;

            //as per instamojo documentaion
            String orderId = "", txnId = "", paymentId = "", token = "";

            voids[0] = voids[0].replace(":", ",");

            //getting the repose from intamojo
            JSONObject intamojo_response = null;
            try {
                intamojo_response = new JSONObject("{" + voids[0] + "}");

                orderId = intamojo_response.getString("orderId");
                txnId = intamojo_response.getString("txnId");

                paymentId = intamojo_response.getString("paymentId");
                token = intamojo_response.getString("token");

            } catch (JSONException e) {
                e.printStackTrace();
            }


            String path = getResources().getString(R.string.url_service) + "Payment_Json/data/?";


            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "payment_success" +
                        "&item_number=" + job_id +
                        "&txn_id=" + txnId +
                        "&amount=" + price +
                        "&currency=" + "INR" +
                        "&payment_status=" + "Done" +
                        "&firstname=" + FIRSTNAME +
                        "&lastname=" + LASTNAME +
                        "&email=" + EMAIL +
                        "&userid=" + USER_ID +

                        "&orderid=" + orderId +
                        "&paymentid=" + paymentId +
                        "&tokenid=" + token,
                        null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                QuickHelpLog.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status = object.getString("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if (status.equals("true")) {

                Toast.makeText(getApplicationContext(), "Your payment was successfully done", Toast.LENGTH_LONG)
                        .show();

                Intent intent = new Intent(getApplicationContext(), Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();

            }

        }
    }

    @Override
    public void onBackPressed() {
        /*Intent gotoHome = new Intent(this, Serviceselectionpage.class);
        startActivity(gotoHome);*/
        Intent i=new Intent(this,Drawerlayout_callhistory.class);
        i.putExtra("getPage","default");
        startActivity(i);
    }

    @Override
    public void onRequestSuccess(Object responseObj) {
        super.onRequestSuccess(responseObj);
        if (responseObj instanceof CommonResponse) {
            CommonResponse response = (CommonResponse) responseObj;
            if (response.isStatus()) {
                DialogManager.showSingleBtnPopup(thisActivity, new DialogMangerCallback() {
                    @Override
                    public void onOkClick(View view) {
                        onBackPressed();
                    }

                    @Override
                    public void onCancelClick(View view) {

                    }
                }, getString(R.string.alert), response.getMessage(), getString(R.string.ok));
            }
        }
    }
}
