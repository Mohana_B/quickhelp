package com.trifft.admin.quichelp.apiInterfaces;

import com.trifft.admin.quichelp.model.CheckDiscountAvailablResponse;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.model.DiscountantResponse;
import com.trifft.admin.quichelp.model.DriverServicTypeMasterResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Fatima.t on 29/4/2020.
 */

public interface APICommonInterface {

    @POST("/json/Login_Json/data")
    Call<CountryMasterModel> countryMaster(
            @Query("service") String serviceStr);

    @POST("/json/Booking_Json/data/")
    Call<CommonResponse> recreateOrEditJob(
            @Query("service") String serviceStr,
            @Query("userid") String userID,
            @Query("job_id")String jobId,
            @Query("pref_date")String prefDate,
            @Query("pref_time")String prefTime);

    @GET("/json/Booking_Json/data")
    Call<CommonResponse> payByCashJob(
            @Query("service") String serviceStr,
            @Query("source") String sourceStr,
            @Query("payment_status")String paymentStatus,
            @Query("job_id")String jobId);

    @POST("/json/Booking_Json/data")
    Call<CheckDiscountAvailablResponse> checkDiscountAvailable(
            @Query("service") String serviceStr,
            @Query("userid") String sourceStr);

    @POST("/json/Booking_Json/data")
    Call<DiscountantResponse> getDiscountInfo(
            @Query("service") String serviceStr,
            @Query("userid") String sourceStr,
            @Query("job_id") String jobId,
            @Query("price") String price,
            @Query("code") String code,
            @Query("rebate") String rebate);


//    https://quichelp.com/json/ServicePrice_Json/data?service=service_types&category=6
    @GET("/json/ServicePrice_Json/data")
    Call<DriverServicTypeMasterResponse> driverServiceTypes(
            @Query("service") String serviceStr,
            @Query("category") String category);

}
