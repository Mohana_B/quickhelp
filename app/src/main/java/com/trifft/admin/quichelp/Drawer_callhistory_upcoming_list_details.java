package com.trifft.admin.quichelp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.trifft.admin.quichelp.activities.BaseActivity;
import com.trifft.admin.quichelp.activities.JobRecreateEditActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.QuickHelpLog;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;

/**
 * Created by Admin on 12/22/2016.
 */
public class Drawer_callhistory_upcoming_list_details extends BaseActivity implements View.OnClickListener, CommonInterface {
    ImageView language;
    Button edit, cancel, paynowBtn, payByCashBtn;
    LinearLayout imageArrow;
    ProgressDialog progressDialog;
    String JOD_ID, JOB_STATUS, DATE, TIME, PRICE, PAYMENT_STATUS;
    TextView jod_id, job_status, date, time, price, payment_status;

    String USER_ID = Serviceselectionpage.USER_ID;
    String EMAIL = Serviceselectionpage.EMAIL;
    String FIRSTNAME = Serviceselectionpage.FIRSTNAME;
    String LASTNAME = Serviceselectionpage.LASTNAME;
    private Drawer_callhistory_upcoming_list_details thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_callhistory_upcoming_list_details);
        thisActivity = this;

        imageArrow = (LinearLayout) findViewById(R.id.imageArrow);
        language = (ImageView) findViewById(R.id.language);
        jod_id = (TextView) findViewById(R.id.jod_id);
        job_status = (TextView) findViewById(R.id.job_status);
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.time);
        price = (TextView) findViewById(R.id.price);
        payment_status = (TextView) findViewById(R.id.payment_status);
        cancel = (Button) findViewById(R.id.cancel);
        paynowBtn = (Button) findViewById(R.id.paynow);
        payByCashBtn = findViewById(R.id.pay_by_cash);

        edit = findViewById(R.id.edit);
        edit.setOnClickListener(this);
        payByCashBtn.setOnClickListener(this);


        JOD_ID = getIntent().getExtras().getString("jod_id");
        JOB_STATUS = getIntent().getExtras().getString("job_status");
        DATE = getIntent().getExtras().getString("date");
        TIME = getIntent().getExtras().getString("time");
        PRICE = getIntent().getExtras().getString("price");
        PAYMENT_STATUS = getIntent().getExtras().getString("payment_status");

        if (Utils.isValidStr(PAYMENT_STATUS)){
            if (PAYMENT_STATUS.equalsIgnoreCase("PAID")){
                paynowBtn.setVisibility(View.GONE);
                payByCashBtn.setVisibility(View.GONE);
            } else if (PAYMENT_STATUS.equalsIgnoreCase("CASH")){
                paynowBtn.setVisibility(View.VISIBLE);
                payByCashBtn.setVisibility(View.GONE);
            } else if (PAYMENT_STATUS.equalsIgnoreCase("NOT PAID")){
                paynowBtn.setVisibility(View.VISIBLE);
                payByCashBtn.setVisibility(View.VISIBLE);
            } else {
                paynowBtn.setVisibility(View.VISIBLE);
                payByCashBtn.setVisibility(View.VISIBLE);
        }
        }

        imageArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Drawer_callhistory_upcoming_list_details.this, language));
        paynowBtn.setOnClickListener(this);


        jod_id.setText(JOD_ID);
        job_status.setText(JOB_STATUS);
        price.setText(PRICE);
        date.setText(DATE);
        time.setText(TIME);
        payment_status.setText(PAYMENT_STATUS);
        Log.e("job_id", JOD_ID + " " + JOB_STATUS + " " + PRICE + " " + DATE + " " + TIME + " " + PAYMENT_STATUS);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new JobCancel().execute();

            }
        });
    }

    String status = "", message = "";



    private class JobCancel extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Drawer_callhistory_upcoming_list_details.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {


            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path = getResources().getString(R.string.url_service) + "Clean_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "job_cancel" + "&job_id=" + JOD_ID +
                        "&userid=" + USER_ID + "&email=" + EMAIL + "&firstname=" + FIRSTNAME + "&lastname=" + LASTNAME, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.e("response", result);

            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);

                Log.d("json", jsn_response.toString());


                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();

            if (status.equals("true")) {
                Toast.makeText(Drawer_callhistory_upcoming_list_details.this, "Your job has been cancelled", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), Drawerlayout_callhistory.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                intent.putExtra("getPage", "upcoming");
                startActivity(intent);
            } else {
                Toast.makeText(Drawer_callhistory_upcoming_list_details.this, message, Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageArrow:
                //onBackPressed();
                back_function();
              /*  Intent in=new Intent(getApplicationContext(),Drawerlayout_callhistory.class);
                in.putExtra("getPage","upcoming");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(in);*/
                break;
            case R.id.paynow:
                if (Utils.isNetworkConnection(thisActivity)){
                    String priceStr = "0";
                    if (Utils.isValidStr(price.getText().toString().trim())){
                        if (price.getText().toString().trim().contains("INR"))
                            priceStr = price.getText().toString().trim().replace("INR", "");
                        else
                            priceStr = price.getText().toString().trim();
                    }

                    if (!priceStr.equals("0"))
                        callInstamojoPay(EMAIL, "8939415346", priceStr, JOD_ID, FIRSTNAME);
                    else
                        Toast.makeText(getApplicationContext(), "Amount not valid", Toast.LENGTH_SHORT).show();

                } else {
                    DialogManager.showToast(thisActivity, getString(R.string.no_internet));
                }
                break;
            case R.id.edit:
                Intent gotoEditJob = new Intent(this, JobRecreateEditActivity.class);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_ID, JOD_ID);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_STATUS, JOB_STATUS);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_RECREATE_OR_EDIT, getString(R.string.edit));
                startActivityForResult(gotoEditJob, JobRecreateEditActivity.JOB_EDIT_RECREATE_REQUEST);
                break;
            case R.id.pay_by_cash:
                // call pay by Cash Api
                APIRequestHandler.getInstance().payByCashJob("update_payment_state","app", "cash",JOD_ID, thisActivity, this);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        back_function();
    }


    private void back_function() {
        Intent in = new Intent(getApplicationContext(), Drawerlayout_callhistory.class);
        in.putExtra("getPage", "upcoming");
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(in);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == JobRecreateEditActivity.JOB_EDIT_RECREATE_REQUEST){
            if (resultCode == RESULT_OK){
                onBackPressed();
            }
        }
    }

    @Override
    public void onRequestSuccess(Object responseObj) {
        super.onRequestSuccess(responseObj);
        if (responseObj instanceof CommonResponse) {
            CommonResponse response = (CommonResponse) responseObj;
            if (response.isStatus()) {
                DialogManager.showSingleBtnPopup(thisActivity, new DialogMangerCallback() {
                    @Override
                    public void onOkClick(View view) {
                        onBackPressed();
                    }

                    @Override
                    public void onCancelClick(View view) {

                    }
                }, getString(R.string.alert), response.getMessage(), getString(R.string.ok));
            }
        }
    }


    InstapayListener listener;
    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }
    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                QuickHelpLog.e("onSuccess", response);
                new Send_token().execute(response);
            }

            @Override
            public void onFailure(int code, String reason) {

                QuickHelpLog.e("onFailure", reason);
                Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG)
                        .show();
            }
        };
    }
    private class Send_token extends AsyncTask<String, Void, Void> {

        String status = "", message = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(thisActivity);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... voids) {

            String result = null;

            //as per instamojo documentaion
            String orderId = "", txnId = "", paymentId = "", token = "";

            voids[0] = voids[0].replace(":", ",");

            //getting the repose from intamojo
            JSONObject intamojo_response = null;
            try {
                intamojo_response = new JSONObject("{" + voids[0] + "}");

                orderId = intamojo_response.getString("orderId");
                txnId = intamojo_response.getString("txnId");

                paymentId = intamojo_response.getString("paymentId");
                token = intamojo_response.getString("token");

            } catch (JSONException e) {
                e.printStackTrace();
            }


            String path = getResources().getString(R.string.url_service) + "Payment_Json/data/?";


            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "payment_success" +
                        "&item_number=" + JOD_ID +
                        "&txn_id=" + txnId +
                        "&amount=" + price +
                        "&currency=" + "INR" +
                        "&payment_status=" + "Done" +
                        "&firstname=" + FIRSTNAME +
                        "&lastname=" + LASTNAME +
                        "&email=" + EMAIL +
                        "&userid=" + USER_ID +

                        "&orderid=" + orderId +
                        "&paymentid=" + paymentId +
                        "&tokenid=" + token,
                        null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                QuickHelpLog.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status = object.getString("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if (status.equals("true")) {

                Toast.makeText(getApplicationContext(), "Your payment was successfully done", Toast.LENGTH_LONG)
                        .show();

                Intent intent = new Intent(getApplicationContext(), Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();

            }

        }
    }
}
