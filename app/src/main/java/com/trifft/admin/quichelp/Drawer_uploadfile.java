package com.trifft.admin.quichelp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import fragment_adapter.Drawer_uploadfile_adapter;

/**
 * Created by Admin on 1/9/2017.
 */

public class Drawer_uploadfile extends AppCompatActivity implements View.OnClickListener{
    ImageView language;
    LinearLayout backArrow;
    ListView list;
    String[] filename={"servf","document","file"};
    //String[] filetype={"application","image","image/png"};
    //String[] filesize={"404","503","305"};

    ArrayList<HashMap<String,String>> array=new ArrayList<HashMap<String,String>>();
    public static String FILENAME="filenames";
    //public static String FILETYPE="filetypes";
   // public static String FILESIZE="filesizes";

    Button choosefile,upload;

    private static final int FILE_SELECT_CODE = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_uploadfile);

        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        language=(ImageView) findViewById(R.id.language);
        list=(ListView)findViewById(R.id.list);
        choosefile=(Button)findViewById(R.id.choosefile);
        upload=(Button)findViewById(R.id.upload);

        language.setOnClickListener(new Language_change(Drawer_uploadfile.this,language));
        backArrow.setOnClickListener(this);

       for(int i=0;i<filename.length;i++)
        {
            HashMap<String,String> map=new HashMap<String, String>();
            map.put("filenames",filename[i]);
            //map.put("filetypes",filetype[i]);
           // map.put("filesizes",filesize[i]);
            array.add(map);
        }

        Drawer_uploadfile_adapter adapter=new Drawer_uploadfile_adapter(Drawer_uploadfile.this,array);
        list.setAdapter(adapter);


        choosefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent in=new Intent(Intent.ACTION_GET_CONTENT);
                in.setType("image/*|application/pdf");
                in.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(Intent.createChooser(in,"Select your file"),FILE_SELECT_CODE);

                Toast.makeText(getApplicationContext(),"Select image/PDF file",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case FILE_SELECT_CODE:

                if(resultCode==RESULT_OK)
                {
                    Uri uri=data.getData();


                    Log.e("path",uri.toString());
                }


                break;
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageArrow:
                onBackPressed();
                break;

        }
    }
}
