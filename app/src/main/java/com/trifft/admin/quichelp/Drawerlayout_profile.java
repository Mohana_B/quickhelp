package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by PSDeveloper on 12/23/2016.
 */

public class Drawerlayout_profile extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, CommonInterface {

    ImageView imPro;
    LinearLayout heading;
    SharedPreferences check;
    String value;
    Button update;
    LinearLayout backArrow;
    ProgressDialog progressDialog;
    String USER_ID = Serviceselectionpage.USER_ID;

    String str_first_name, str_last_name, str_email, str_alt_email;
    String str_address_1, str_address_2, str_selectCity, str_state, select_Country, str_zip_code, str_mobile_num;

    EditText firstname, lastname, email, alt_email_id;
    EditText address1, address2, /*selectCountry,*/ /*selectCity,*/ selectState, zipcode, mobileNumber;

    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;

    private Drawerlayout_profile thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_profile);
        thisActivity = this;

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        imPro = (ImageView) findViewById(R.id.imageSignin);
        heading = (LinearLayout) findViewById(R.id.toolbarSingn);
        update = (Button) findViewById(R.id.update);

        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        alt_email_id = (EditText) findViewById(R.id.alt_email_id);
        address1 = (EditText) findViewById(R.id.address1);
        address2 = (EditText) findViewById(R.id.address2);

        spnCountry = findViewById(R.id.spn_country);
//        selectCity = (EditText) findViewById(R.id.selectcity);
        selectState = (EditText) findViewById(R.id.state);
        zipcode = (EditText) findViewById(R.id.zipcode);
        mobileNumber = (EditText) findViewById(R.id.phoneno);

        backArrow.setOnClickListener(this);

        check = getSharedPreferences("Default_Language", MODE_PRIVATE);
        value = check.getString("Value", "");

        /*selectCity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide_kyboard();
                return false;
            }
        });

        selectCity.setOnClickListener(new Select_country(Drawerlayout_profile.this, selectCity));*/

        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(thisActivity, android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(thisActivity, this);


        if (USER_ID != null) {
            //get user info from service

            if (isOnline()) {
                new Get_user_contact_info().execute();
            } else {
                Toast.makeText(getApplicationContext(), "No network connection to load contact details", Toast.LENGTH_SHORT).show();
            }

            if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
                firstname.setText(Serviceselectionpage.FIRSTNAME);
            if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
                lastname.setText(Serviceselectionpage.LASTNAME);
            if (Utils.isValidStr(Serviceselectionpage.EMAIL))
                email.setText(Serviceselectionpage.EMAIL);
            if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
                mobileNumber.setText(Serviceselectionpage.PHONENUM);

        }

       /* if(value.equals("CHENNAI"))
        {
            imPro.setVisibility(View.INVISIBLE);
            heading.setBackgroundResource(R.color.app_heading2);
            update.setBackgroundResource(R.drawable.ed_button2);

        }*/

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // str_alt_email=alt_email_id.getText().toString();
                str_first_name = firstname.getText().toString();
                str_last_name = lastname.getText().toString();
                str_email = email.getText().toString();

                str_alt_email = alt_email_id.getText().toString();
                str_address_1 = address1.getText().toString();
                str_address_2 = address2.getText().toString();
//                str_selectCity = selectCity.getText().toString();
                str_selectCity = spnCountry.getSelectedItem().toString();
                str_state = selectState.getText().toString();
                str_zip_code = zipcode.getText().toString();
                str_mobile_num = mobileNumber.getText().toString();


                if (!isOnline()) {
                    Toast.makeText(getApplicationContext(), "No network connection", Toast.LENGTH_SHORT).show();
                } else if (USER_ID == null) {
                    Toast.makeText(getApplicationContext(), "Please Sigin in your application", Toast.LENGTH_SHORT).show();
                } else if (str_alt_email.length() > 0 && !validEmail(str_alt_email)) {
                    /*if(!validEmail(str_alt_email))
                    {*/
                    Toast.makeText(getApplicationContext(), "Enter the valid alternate email id", Toast.LENGTH_SHORT).show();
                    //}
                    //Toast.makeText(getApplicationContext(),"Enter the alternate email id" ,Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(str_mobile_num)){
                    Toast.makeText(getApplicationContext(), "Enter the Phone number" ,Toast.LENGTH_SHORT).show();
                } else if (str_mobile_num.length() < 10){
                    Toast.makeText(getApplicationContext(), "Enter valid Phone number" ,Toast.LENGTH_SHORT).show();
                }else if (!Utils.isValidStr(str_selectCity)) {
                    DialogManager.showToast(thisActivity, "Please select country");
                } else if (str_selectCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(thisActivity, "Please select country");
                } else if (!Utils.isValidStr(str_zip_code)){
                    Toast.makeText(getApplicationContext(), "Enter the zipcode" ,Toast.LENGTH_SHORT).show();
                } else if (str_zip_code.length() < 6 || str_zip_code.length() > 6){
                    Toast.makeText(getApplicationContext(), "Enter valid zipcode" ,Toast.LENGTH_SHORT).show();
                }
                /*else if(!validEmail(str_alt_email))
                {
                    Toast.makeText(getApplicationContext(), "Enter the valid email id" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_mobile_num.length()==0 || str_mobile_num.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the Phone number" ,Toast.LENGTH_SHORT).show();
                }*/
               /* else if(str_mobile_num.length()==0 || str_mobile_num.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the Phone number" ,Toast.LENGTH_SHORT).show();
                }
                else if(str_address_1.length()==0 || str_address_1.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }
                else if(str_address_2.length()==0 || str_address_2.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_state.length()==0 || str_state.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the State" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_selectCity.length()==0 || str_selectCity.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Select the City" ,Toast.LENGTH_SHORT).show();
                }*/

               /* else if(str_zip_code.length()==0 || str_zip_code.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the zipcode" ,Toast.LENGTH_SHORT).show();
                }*/


                else {
                    //Toast.makeText(getApplicationContext(, "Database error..contact admin" ,Toast.LENGTH_SHORT).show();
                    // Edit Shared Preferences here
                    SharedPreferences my_id = getSharedPreferences("My_id", MODE_PRIVATE);
                    SharedPreferences.Editor editor = my_id.edit();
                    editor.putString("Firstname", str_first_name).putString("Lastname", str_last_name)
                            .putString("email", str_email).putString("phone_num",str_mobile_num).commit();


                    new Mytask().execute();
                }


            }
        });

    }

    private void hide_kyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private class Get_user_contact_info extends AsyncTask<Void, Void, Void> {

        String status = "", message = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Drawerlayout_profile.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path = getResources().getString(R.string.url_service) + "Home_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "getcontactinfo"
                        + "&userid=" + USER_ID, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);
                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equals("true")) {

                try {
                    JSONArray array = jsn_response.getJSONArray("sendmsg");

                    JSONObject get_array = array.getJSONObject(0);

                    str_address_1 = get_array.getString("address1");
                    str_address_2 = get_array.getString("address2");
                    str_state = get_array.getString("state");
                    str_selectCity = get_array.getString("city");
                    str_alt_email = get_array.getString("alt_email");
                    str_mobile_num = get_array.getString("phone_number");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            // Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if (status.equals("true")) {

                address1.setText(str_address_1);
                address2.setText(str_address_1);
                selectState.setText(str_address_1);
                int selectionPosition= countryAdapter.getPosition(str_selectCity);
                spnCountry.setSelection(selectionPosition);
//                selectCity.setText(str_selectCity);
                if (Utils.isValidStr(str_mobile_num))
                    mobileNumber.setText(str_mobile_num);
                alt_email_id.setText(str_alt_email);
            } else {

                // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    String status = "", message = "";

    private class Mytask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Drawerlayout_profile.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path = getResources().getString(R.string.url_service) + "Home_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "insertcontactinfo"
                        + "&email=" + str_email + "&firstname=" + str_first_name
                        + "&lastname=" + str_last_name + "&cityname=" + str_selectCity
                        + "&zipcode=" + str_zip_code
                        + "&address1=" + str_address_1 + "&address2=" + str_address_2
                        + "&countryname=" + "India" + "&statename=" + str_state
                        + "&phone=" + str_mobile_num + "&alternateemail=" + str_alt_email
                        + "&userid=" + USER_ID, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);
                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equals("true")) {


            }

            Log.e("status", status);
            Log.e("message", message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if (status.equals("true")) {
                onBackPressed();
                Toast.makeText(getApplicationContext(), "Profile has been updated..", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();


            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.imageArrow:
                onBackPressed();

                break;
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(thisActivity, " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(thisActivity, "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }
}
