package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trifft.admin.quichelp.model.JobItemModel;
import com.trifft.admin.quichelp.utils.QuickHelpLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fragment_adapter.Drawer_callhistory_upcoming_listadapter;

/**
 * Created by Admin on 12/21/2016.
 */

public class Drawer_call_history_upcoming extends Fragment {
    ListView list;
    public static String JOD_ID="jod_id",JOB_STATUS="job_status",
            DATE="date",TIME="time",PRICE="price",PAYMENT_STATUS="payment_status";
    ProgressDialog progressDialog;
     String USER_ID=Serviceselectionpage.USER_ID;
    TextView empty_text;
     String JOB_status;
    Drawer_callhistory_upcoming_listadapter adapter;

    ArrayList<HashMap<String,String>> arrayListJob=new ArrayList<HashMap<String, String>>();
//    ArrayList<JobItemModel> arrayListJob=new ArrayList<JobItemModel>();

    public static final String EXTRA_ALL_JOBS = "extraAllJobs";

    static Context mContext;
    static Drawer_call_history_upcoming mInstance = new Drawer_call_history_upcoming();



    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {


        /*Bundle bundle = getArguments();
        if (bundle != null){
            if (bundle.containsKey(EXTRA_ALL_JOBS)){
                arrayListJob = (ArrayList<JobItemModel>) bundle.getSerializable(EXTRA_ALL_JOBS);
            }
        }*/
        View v=inflater.inflate(R.layout.drawer_call_history_upcoming,container,false);
        list=(ListView)v.findViewById(R.id.list);
        empty_text=(TextView)v.findViewById(R.id.empty_text);
        mContext = getActivity();


        if(USER_ID==null) {
            empty_text.setVisibility(View.VISIBLE);
            empty_text.setText("Sign in your application");
        }
        else {
            new JobHistory().execute(USER_ID);
        }

       /* if (arrayListJob != null && arrayListJob.size() > 0){
            ArrayList<JobItemModel> arrayListJobMain = getUpcomingItems();
            if (arrayListJobMain != null && arrayListJobMain.size() > 0) {
                adapter = new Drawer_callhistory_upcoming_listadapter(getActivity(), arrayListJobMain);
                list.setAdapter(adapter);
            } else {
                empty_text.setVisibility(View.VISIBLE);
                empty_text.setText("No Jobs generated");
            }
        } else if(adapter != null && adapter.isEmpty()) {
            empty_text.setVisibility(View.VISIBLE);
            empty_text.setText("No Jobs generated");
        }*/



        //new JobHistory().execute("24");

       /*list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

                MenuInflater mnu=actionMode.getMenuInflater();
                mnu.inflate(R.menu.message_menu,menu);


                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.delete:

                        Toast.makeText(getActivity(), "item removed", Toast.LENGTH_LONG).show();

                        actionMode.finish();
                        return true;


                    default:
                        break;
                }

                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }
        });*/





        return v;
    }

    /*private ArrayList<JobItemModel> getUpcomingItems() {
        ArrayList<JobItemModel> tempList = new ArrayList<>();
        for (JobItemModel model : arrayListJob){
            if (model.getJob_status().equalsIgnoreCase("new") || model.getJob_status().equalsIgnoreCase("open"))
                tempList.add(model);
        }
        return tempList;
    }*/

    String status="",message="";
    private class JobHistory extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(String... voids) {

            String result = null;

            String path=getActivity().getResources().getString(R.string.url_service)+"Booking_Json/data/?";

            DefaultHttpClient client=new DefaultHttpClient();


            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"get_all_jobs"+"&userid="+voids[0],null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status",status);Log.e("message",message);
            QuickHelpLog.e("Job History Upcoming :" , result);

            Type listType = new TypeToken<List<JobItemModel>>() {}.getType();
            ArrayList<HashMap<String,String>> arrayList=new ArrayList<HashMap<String, String>>();
            if(status.equals("true"))
            {
                try {
                    JSONObject allJobs = jsn_response.getJSONObject("alljobs");

                    String cleaningArray = allJobs.getJSONArray("cleaning").toString();
                    ArrayList<JobItemModel> cleaningList = new Gson().fromJson(cleaningArray, listType);
                    if (cleaningList != null && cleaningList.size() > 0){
                        addItemsToList(cleaningList);
                    }

                    String moversArray = allJobs.getJSONArray("movers").toString();
                    ArrayList<JobItemModel> moversList = new Gson().fromJson(moversArray, listType);
                    if (moversList != null && moversList.size() > 0){
                        addItemsToList(moversList);
                    }

                    String paintingArray = allJobs.getJSONArray("painting").toString();
                    ArrayList<JobItemModel> paintingList = new Gson().fromJson(paintingArray, listType);
                    if (paintingList != null && paintingList.size() > 0){
                        addItemsToList(paintingList);
                    }

                    String acArray = allJobs.getJSONArray("ac service").toString();
                    ArrayList<JobItemModel> acList = new Gson().fromJson(acArray, listType);
                    if (acList != null && acList.size() > 0){
                        addItemsToList(acList);
                    }


                    String chefArray = allJobs.getJSONArray("chef").toString();
                    ArrayList<JobItemModel> chefList = new Gson().fromJson(chefArray, listType);
                    if (chefList != null && chefList.size() > 0){
                        addItemsToList(chefList);
                    }

                    String drivingArray = allJobs.getJSONArray("driving").toString();
                    ArrayList<JobItemModel> drivingList = new Gson().fromJson(drivingArray, listType);
                    if (drivingList != null && drivingList.size() > 0){
                        addItemsToList(drivingList);
                    }

                    String realStateArray = allJobs.getJSONArray("real estate").toString();
                    ArrayList<JobItemModel> realStateList = new Gson().fromJson(realStateArray, listType);
                    if (realStateList != null && realStateList.size() > 0){
                        addItemsToList(realStateList);
                    }

                    String beautyArray = allJobs.getJSONArray("Makeup Spa").toString();
                    ArrayList<JobItemModel> beautyList = new Gson().fromJson(beautyArray, listType);
                    if (beautyList != null && beautyList.size() > 0){
                        addItemsToList(beautyList);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                JSONArray array= null;
//                JSONArray array_get= null;
//                try {
//                    array = jsn_response.getJSONArray("alljobs");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                for(int i=0;i<array.length();i++) {
//
//                    try {
//                        array_get=array.getJSONArray(i);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    for(int j=0;j<array_get.length();j++)
//                    {
//
//                        JSONObject object;
//                        try {
//                            object =array_get.getJSONObject(j);
//
//                            JOB_status=object.getString("job_status");
//                            if(JOB_status.equals("new") || JOB_status.equals("open")) {
//
//                                HashMap<String, String> map = new HashMap<String, String>();
//
//                                map.put("jod_id", object.getString("job_id"));
//                                map.put("job_status", object.getString("job_status"));
//                                map.put("date", object.getString("date"));
//                                map.put("time", object.getString("time"));
//                                map.put("price", object.getString("price"));
//                                map.put("payment_status", object.getString("payment_status"));
//
//
//                                arrayList.add(map);
//
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//
//
//                }

            }

//            return arrayList;
            return arrayListJob;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true")) {

               adapter = new Drawer_callhistory_upcoming_listadapter(getActivity(), aVoid);
                list.setAdapter(adapter);

                if(adapter.isEmpty())
                {
                    empty_text.setVisibility(View.VISIBLE);
                    empty_text.setText("No Jobs generated");
                }
            }
        }
    }

    private void addItemsToList(ArrayList<JobItemModel> arrayList) {
        String jobStatus = "";
        try{
            for (JobItemModel model : arrayList){
                jobStatus=model.getJob_status();
                if(jobStatus.equals("new") || jobStatus.equals("open") || jobStatus.equals("active")) {
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("jod_id", model.getJob_id());
                    map.put("job_status", model.getJob_status());
                    map.put("date", model.getPref_date());
                    map.put("time",model.getPref_time() );
                    map.put("price", model.getPrice());
                    map.put("payment_status", model.getPayment_status());

                    arrayListJob.add(map);

                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void setListItems(ArrayList<HashMap<String, String>> upcomingListJob) {
//        Drawer_callhistory_upcoming_listadapter adapter = new Drawer_callhistory_upcoming_listadapter(mInstance.getActivity(), upcomingListJob);
//        mInstance.list.setAdapter(adapter);
    }




}
