package com.trifft.admin.quichelp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aduser on 1/30/2018.
 */

public class Pricing_adapter_class extends BaseAdapter {



    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> arrayList;

    HashMap<String,String> map=new HashMap<String, String>();



    public Pricing_adapter_class(Context activity, ArrayList<HashMap<String, String>> aVoid) {

        this.context=activity;
        this.arrayList=aVoid;
    }



    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView= inflater.inflate(R.layout.pricing_adapter, null);

        TextView city=(TextView)rowView.findViewById(R.id.city);
        TextView paint=(TextView)rowView.findViewById(R.id.paint);

        TextView movers=(TextView)rowView.findViewById(R.id.movers);

        TextView cleaning=(TextView)rowView.findViewById(R.id.cleaning);

        TextView clean_living=(TextView)rowView.findViewById(R.id.clean_living);
        TextView clean_bed=(TextView)rowView.findViewById(R.id.clean_bed);
        TextView clean_kitchen=(TextView)rowView.findViewById(R.id.clean_kitchen);
        TextView clean_toilet=(TextView)rowView.findViewById(R.id.clean_toilet);
        TextView call_chef_3CS=(TextView)rowView.findViewById(R.id.call_chef_3CS);
        TextView call_chef_3CN=(TextView)rowView.findViewById(R.id.call_chef_3CN);

        TextView call_chef_5CS=(TextView)rowView.findViewById(R.id.call_chef_5CS);
        TextView call_chef_5CN=(TextView)rowView.findViewById(R.id.call_chef_5CN);
        TextView aircon=(TextView)rowView.findViewById(R.id.aircon);
        TextView driving=(TextView)rowView.findViewById(R.id.driving);

        map=arrayList.get(i);


        //city.setText("City : "+map.get(Cleaningprovide.city));
        paint.setText("Painting : "+map.get(Cleaningprovide.paint));
        movers.setText("Movers : "+map.get(Cleaningprovide.movers));
        cleaning.setText("Cleaning : "+map.get(Cleaningprovide.cleaning));
        clean_living.setText("Clean Living : "+map.get(Cleaningprovide.clean_living));
        clean_bed.setText("Clean Bed : "+map.get(Cleaningprovide.clean_bed));
        clean_kitchen.setText("Clean Kitchen : "+map.get(Cleaningprovide.clean_kitchen));
        clean_toilet.setText("Clean Toilet : "+map.get(Cleaningprovide.clean_toilet));
        call_chef_3CS.setText("Chef 3CS : "+map.get(Cleaningprovide.call_chef_3CS));
        call_chef_3CN.setText("Chef 3CN : "+map.get(Cleaningprovide.call_chef_3CN));
        call_chef_5CS.setText("Chef 5CS : "+map.get(Cleaningprovide.call_chef_5CS));
        call_chef_5CN.setText("Chef 5CN : "+map.get(Cleaningprovide.call_chef_5CN));
        aircon.setText("Aircon : "+map.get(Cleaningprovide.aircon));
        driving.setText("Driving : "+map.get(Cleaningprovide.driving));




        return rowView;



    }
}
