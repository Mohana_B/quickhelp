package com.trifft.admin.quichelp;

import android.app.MediaRouteButton;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.DropDownSelection;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Realestatebooknow extends Fragment implements AdapterView.OnItemSelectedListener, CommonInterface {

    SharedPreferences check;
    String value;
    Button getaprice;
    ProgressDialog progressDialog;
    String first_name, last_name, email, selectedEstateType, mobileNumber, st_areaSqFt, date_data, toDateData, address_1, selectedCity, Zipcode, special_comments, typeofService;

    EditText firstname, lastname, etEmail, etEstateType, mobile_number, etAreaSqFt, date, toDateEt, address1, /*select_city,*/ zip_code, specialComments;

    RadioButton standard, executive, airbnb;

    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    private MediaRouteButton combo;

    private ArrayList<String> estateTypeList = new ArrayList<>();
//    private EditText time;
//    private String time_data;
    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.realestatebooknow, container, false);
        getaprice = (Button) v.findViewById(R.id.getaprice);


        estateTypeList.add("Apartment");
        estateTypeList.add("Bunglow / Independent house");
        estateTypeList.add("Landed Property");
        estateTypeList.add("Farm Property");
        estateTypeList.add("Business Property");
        estateTypeList.add("Airbnb / Similar Property");

        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        etEmail = v.findViewById(R.id.email);
        mobile_number = (EditText) v.findViewById(R.id.contractnum);
        etEstateType = v.findViewById(R.id.estate_type);
        etAreaSqFt = v.findViewById(R.id.area_sqft);
        date = (EditText) v.findViewById(R.id.fromDateEt);
        toDateEt = (EditText) v.findViewById(R.id.toDateEt);
        address1 = (EditText) v.findViewById(R.id.address1);
//        select_city = (EditText) v.findViewById(R.id.selectcity);
        spnCountry = v.findViewById(R.id.spn_country);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);

        /*select_rooms = (EditText) v.findViewById(R.id.selectrooms);
        approx_area = (EditText) v.findViewById(R.id.approxarea);
        kk = (LinearLayout) v.findViewById(R.id.head);
        time = (EditText) v.findViewById(R.id.time);
        address2 = (EditText) v.findViewById(R.id.address2);*/

        standard = (RadioButton) v.findViewById(R.id.standard);
        executive = (RadioButton) v.findViewById(R.id.executive);
        airbnb = v.findViewById(R.id.airbnb);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");


        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            etEmail.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);


        /*if (value.equals("CHENNAI")) {
            getaprice.setBackgroundResource(R.drawable.ed_button2);
            select_city.setText("CHENNAI");

        } else {
            select_city.setText("MUMBAI");
        }*/
        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);



//        select_rooms.setOnClickListener(new Popup_Ten_room(getActivity(), select_rooms, "rooms"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        toDateEt.setOnClickListener(new Date_picker(getActivity(), toDateEt));
//        time.setOnClickListener(new TimePicker(getActivity(), time));
        //select_city.setOnClickListener(new Select_country(getActivity(),select_city));


//        select_city.setOnClickListener(new Select_country(getActivity(), select_city));
        etEstateType.setOnClickListener(new DropDownSelection(getActivity(), etEstateType, estateTypeList));

        getaprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = etEmail.getText().toString().trim();
                mobileNumber = mobile_number.getText().toString();
                selectedEstateType = etEstateType.getText().toString();
                st_areaSqFt = etAreaSqFt.getText().toString().trim();
                date_data = date.getText().toString();
                toDateData = toDateEt.getText().toString();
                address_1 = address1.getText().toString();
                selectedCity = spnCountry.getSelectedItem().toString();
//                selectCity = select_city.getText().toString();
                Zipcode = zip_code.getText().toString();
                special_comments = specialComments.getText().toString();

//                selectRooms = select_rooms.getText().toString();
//                time_data = time.getText().toString();
//                address_2 = address2.getText().toString();
//                app_area = approx_area.getText().toString();


                if (!Utils.isValidStr(first_name)) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(last_name)) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(email)) {
                    Toast.makeText(getActivity(), "Enter the Email", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(mobileNumber)) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (!standard.isChecked() && !executive.isChecked() && !airbnb.isChecked()) {
                    Toast.makeText(getActivity(), "Select the service type", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(date_data)) {
                    Toast.makeText(getActivity(), "Select From Date", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(toDateData)){
                    Toast.makeText(getActivity(), "Select To Date", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(address_1)) {
                    Toast.makeText(getActivity(), "Enter Address", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(st_areaSqFt)) {
                    Toast.makeText(getActivity(), "Enter the Area in Square feets", Toast.LENGTH_SHORT).show();
                } else if (selectedEstateType.length() == 0) {
                    Toast.makeText(getActivity(), "Select Real Estate type ", Toast.LENGTH_SHORT).show();
                }else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (Zipcode.length() == 0) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                }else if (Zipcode.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                } /*else if (selectRooms.equals("")) {
                    Toast.makeText(getActivity(), "Select the Rooms", Toast.LENGTH_SHORT).show();
                } else if (time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                }*/ else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (standard.isChecked()) {
                        typeofService = "Standard";

                    }

                    if (executive.isChecked()) {
                        typeofService = "Executive";

                    }

                    if (airbnb.isChecked()) {
                        typeofService = "Airbnb";

                    }
                    new Mytask().execute();

                }
            }
        });

        return v;
    }


    private class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", jod_id = "", message = "",price = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Realestate_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*https://quichelp.com/json/Realestate_Json/data/?service=insert_realestate_job&
            userid=191&
            firstname=Shrikar&
            lastname=K&
            emailid=k.shrikar@gmail.com&
            contact=7620222896&
            select_city=Chennai&
            pincode=500058&
            area=100&
            sericetype=standard&
            estate_type=appartment&
            pref_date=03/10/2020&
            usr_time=10:30AM&
            address=Hyderabad&
            price=399&
            comments=test*/

            /*https://quichelp.com/json/Realestate_Json/data/?service=insert_realestate_job&
            userid=191&
            firstname=Shrikar&
            lastname=K&
            emailid=k.shrikar@gmail.com&
            contact=1234567890&
            select_city=Chennai&
            pincode=500058&
            area=100&
            sericetype=Executive&
            estate_type=appartment&
            from_date=03/10/2020&
            to_date=04/10/2020&
            address=Hyderabad&
            comments=test*/

            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "insert_realestate_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&lastname=" + last_name +
                        "&emailid=" + EMAIL_ID +
                        "&contact=" + mobileNumber +
                        "&select_city=" + selectedCity +
                        "&pincode=" + Zipcode +
                        "&area=" + st_areaSqFt +
                        "&sericetype=" + typeofService +
                        "&estate_type=" + selectedEstateType +
                        "&from_date=" + date_data +
                        "&to_date=" + toDateData +
                        "&address=" + address_1 +
                        "&comments=" + special_comments
//                        "&no_rooms=" + selectRooms +
//                        "&pref_time=" + time_data +

//                        "&frm_address2=" + address_2 +
                        , null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status = object.getString("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status);

            if (status.equals("true")) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }

}

