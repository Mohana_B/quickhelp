package com.trifft.admin.quichelp.apiInterfaces;

import android.app.Dialog;
import android.content.Context;

import com.trifft.admin.quichelp.model.CheckDiscountAvailablResponse;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.model.DiscountantResponse;
import com.trifft.admin.quichelp.model.DriverServicTypeMasterResponse;
import com.trifft.admin.quichelp.utils.AppConstants;
import com.trifft.admin.quichelp.utils.DialogManager;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fatima.t on 29/4/2020.
 */

public class APIRequestHandler {
    Dialog progress;
    private static final APIRequestHandler instance = new APIRequestHandler();
    public static APIRequestHandler getInstance() {
        return instance;
    }

    private APIRequestHandler() {

    }
    public APICommonInterface getApiService() {

        /*Gson gson = new GsonBuilder()
                .setLenient()
                .create();*/

        Retrofit mRetrofit = new Retrofit.Builder().baseUrl(AppConstants.AppURL)
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(/*gson*/))
                .client(createOkHttpClient()).build();
        return mRetrofit.create(APICommonInterface.class);
    }
    private OkHttpClient createOkHttpClient() {
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(60*1*1000, TimeUnit.MILLISECONDS);
        httpClient.writeTimeout(60*3*1000, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(60*3*1000, TimeUnit.MILLISECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);// Added by TARA

        //interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(interceptor);
        httpClient.connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT));
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();
                final HttpUrl originalHttpUrl = original.url();

                // Request customization: add request headers
                final Request.Builder requestBuilder = original.newBuilder();
                //requestBuilder.header("Accept", "application/json");


                final Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }

    public void countryMaster(Context context, final CommonInterface mInf) {

        String service = "getcountry";
        DialogManager.showProgress(context);

        Call<CountryMasterModel> odometerResponseCall = getApiService().countryMaster(service);
        odometerResponseCall.enqueue(new Callback<CountryMasterModel>() {
            @Override
            public void onResponse(Call<CountryMasterModel> call, retrofit2.Response<CountryMasterModel> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CountryMasterModel> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call, t);
            }
        });
    }

    public void recreateOrEditJob(String service, String userId, String jobId, String prefDate, String prefTime, Context context, final CommonInterface mInf) {

        DialogManager.showProgress(context);

        Call<CommonResponse> odometerResponseCall = getApiService().recreateOrEditJob(service, userId, jobId, prefDate, prefTime);
        odometerResponseCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call,t);
            }
        });
    }

    public void payByCashJob(String service, String sourceStr, String paymentStatus, String jobId, Context context, final CommonInterface mInf) {

        DialogManager.showProgress(context);

        Call<CommonResponse> odometerResponseCall = getApiService().payByCashJob(service, sourceStr, paymentStatus, jobId);
        odometerResponseCall.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call, t);
            }
        });
    }
    public void checkDiscountAvailable(String service, String userId, Context context, final CommonInterface mInf) {

        DialogManager.showProgress(context);

        Call<CheckDiscountAvailablResponse> odometerResponseCall = getApiService().checkDiscountAvailable(service, userId);
        odometerResponseCall.enqueue(new Callback<CheckDiscountAvailablResponse>() {
            @Override
            public void onResponse(Call<CheckDiscountAvailablResponse> call, retrofit2.Response<CheckDiscountAvailablResponse> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<CheckDiscountAvailablResponse> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call,t);
            }
        });
    }

    public void getDiscountInfo(String service, String userId, String jobId, String price, String code, String rebate, Context context, final CommonInterface mInf) {

        /*https://quichelp.com/json/Booking_Json/data/?service=get_discount&userid=191&job_id=AC30&price=998&currency=INR&code=5e9f2e70e500e&rebate=coupon*/
        DialogManager.showProgress(context);

        Call<DiscountantResponse> odometerResponseCall = getApiService().getDiscountInfo(service, userId, jobId, price, code, rebate);
        odometerResponseCall.enqueue(new Callback<DiscountantResponse>() {
            @Override
            public void onResponse(Call<DiscountantResponse> call, retrofit2.Response<DiscountantResponse> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<DiscountantResponse> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call,t);
            }
        });
    }

    public void driverServiceTypes(String service, String caregory, Context context, final CommonInterface mInf) {
        DialogManager.showProgress(context);

        Call<DriverServicTypeMasterResponse> odometerResponseCall = getApiService().driverServiceTypes(service, caregory);
        odometerResponseCall.enqueue(new Callback<DriverServicTypeMasterResponse>() {
            @Override
            public void onResponse(Call<DriverServicTypeMasterResponse> call, retrofit2.Response<DriverServicTypeMasterResponse> response) {
                if (response!=null){
                    DialogManager.hideProgress(context);
                    mInf.onRequestSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<DriverServicTypeMasterResponse> call, Throwable t) {
                DialogManager.hideProgress(context);
                mInf.onRequestFailure(call, t);
            }
        });
    }
}
