package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.utils.QuickHelpLog;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin on 11/11/2016.
 */

public class Moversbooknow extends Fragment implements View.OnClickListener {

    private static final String TAG = "MoversBookNow";

    SharedPreferences check;
    String value;
    Button movers;
    ProgressDialog progressDialog;
    String first_name, mobileNumber, last_name, email, date_data, time_data, address_1, address_2, special_comments, approx, Zipcode, Zipcode2,
            typeofService, proFessionals, selectCity, selectCity2, address_1_to, address_2_to;

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    EditText professionals, select_city, select_city_2;
    EditText firstname, lastname, email_id, mobile_number, approx_dis;
    EditText date, time, address1, address2, selectCountry, /*zipcode,*/ specialComments, address1to, address2to, zip_code, zip_codeto;
    double distance = 0;

    RadioButton personal, commercial;

    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;


    private int PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM = 1;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE_TO = 2;
    public static String addrsInfo = "", city = "", state = "", country = "", pin = "", landMark = "", /*address2 = "",*/ strAddresName, fromLatLong = "", toLatLong = "";
    private double km = 1.609344;
    private double distanceinkm;

    Spinner spnCountry;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.moversbooknow, container, false);
        movers = (Button) v.findViewById(R.id.getaprice);
        professionals = (EditText) v.findViewById(R.id.professionals);
        date = (EditText) v.findViewById(R.id.date);
        time = (EditText) v.findViewById(R.id.time);
        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        email_id = (EditText) v.findViewById(R.id.email);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        address1to = (EditText) v.findViewById(R.id.address1to);
        address2to = (EditText) v.findViewById(R.id.address2to);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        zip_codeto = (EditText) v.findViewById(R.id.zipcode_two);
        mobile_number = (EditText) v.findViewById(R.id.mobilenumber);
        approx_dis = (EditText) v.findViewById(R.id.approx_dis);
        approx_dis.setTag(approx_dis.getKeyListener());
        approx_dis.setKeyListener(null);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);
        // select_country=(EditText) v.findViewById(R.id.selectcountry);
        //select_country_2=(EditText) v.findViewById(R.id.selectcountry_two);
        select_city = (EditText) v.findViewById(R.id.selectcity);
        select_city_2 = (EditText) v.findViewById(R.id.selectcity1);
        personal = (RadioButton) v.findViewById(R.id.personal);
        commercial = (RadioButton) v.findViewById(R.id.commercial);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");


        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            email_id.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);

     /*   if(value.equals("CHENNAI"))
        {
            //movers.setBackgroundResource(R.drawable.ed_button2);
            select_city.setText("CHENNAI");
            select_city_2.setText("CHENNAI");
        }
        else{select_city.setText("MUMBAI");
            select_city_2.setText("MUMBAI");}*/

//        select_city.setOnClickListener(new Select_country(getActivity(), select_city));
//        select_city_2.setOnClickListener(new Select_country(getActivity(), select_city_2));
        address1.setOnClickListener(this);
        address1to.setOnClickListener(this);

        professionals.setOnClickListener(new Popup_fiveroom(getActivity(), professionals, "professional"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));

        movers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = email_id.getText().toString();
                proFessionals = professionals.getText().toString();
                date_data = date.getText().toString();
                time_data = time.getText().toString();

                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();
                address_1_to = address1to.getText().toString();
                address_2_to = address2to.getText().toString();
                Zipcode = zip_code.getText().toString();
                Zipcode2 = zip_codeto.getText().toString();
                mobileNumber = mobile_number.getText().toString();
                special_comments = specialComments.getText().toString();

                selectCity = select_city.getText().toString();
                selectCity2 = select_city_2.getText().toString();

                if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else if (first_name.length() == 0 || first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.length() == 0 || last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (!personal.isChecked() && !commercial.isChecked()) {
                    Toast.makeText(getActivity(), "Select the service type", Toast.LENGTH_SHORT).show();
                } else if (email.length() == 0 || email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (proFessionals.length() == 0 || proFessionals.equals("")) {
                    Toast.makeText(getActivity(), "Select the professionals", Toast.LENGTH_SHORT).show();
                } /*else if (approx.length() == 0 || approx.equals("")) {
                    Toast.makeText(getActivity(), "Enter the Approx distance", Toast.LENGTH_SHORT).show();
                }*/ else if (date_data.length() == 0 || date_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                } else if (time_data.length() == 0 || time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                }
               /* else if(address_1.length()==0 || address_1.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }
                else if(address_2.length()==0 || address_2.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }*/
               /* else if(select_city.length()==0 || select_city.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the City" ,Toast.LENGTH_SHORT).show();
                }*/
                else if (Zipcode.length() == 0 || Zipcode.equals("")) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                } else if (Zipcode2.length() == 0 || Zipcode2.equals("")) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                }
                else if (Zipcode.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode(from)", Toast.LENGTH_SHORT).show();
                } else if (Zipcode2.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode(to)", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() == 0 || mobileNumber.equals("")) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (special_comments.length() == 0 || special_comments.equals("")) {
                    Toast.makeText(getActivity(), "Enter the special comments", Toast.LENGTH_SHORT).show();
                } else {

                    if (personal.isChecked()) {
                        typeofService = "personal";
                    }

                    if (commercial.isChecked()) {
                        typeofService = "commercial";

                    }

                    new Mytask().execute();
                }


            }
        });


        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address1:
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), BuildConfig.PLACES_API_KEY);
                }
                List<Place.Field> fieldsFrom = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                Intent intentFrom = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fieldsFrom).build(getActivity());
                startActivityForResult(intentFrom, PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM);
                break;
            case R.id.address1to:
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), BuildConfig.PLACES_API_KEY);
                }
                List<Place.Field> fieldsTo = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
                Intent intentTo = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fieldsTo).build(getActivity());
                startActivityForResult(intentTo, PLACE_AUTOCOMPLETE_REQUEST_CODE_TO);
                break;
        }
    }

    private class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", jod_id = "", message = "", price = "";
        boolean status1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Movers_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*https://quichelp.com/json/Movers_Json/data/?service=insert_movers_job&
            userid=191&
            firstname=Shrikar&
            lastname=K&
            email=k.shrikar@gmail.com&
            phone=7620222896&
            service_type=personal&
            no_helpers=1&
            approx_distance=200&
            pref_date=03/10/2020&
            pref_time=10:30AM&
            frm_address1=Badangpet, Hyderabad&
            frm_address2=Telangana&
            frm_city=Hyderabad&
            frm_zip=5800085&
            to_address1=Viman nagar, Pune&
            to_address2=MH&
            to_city=Pune&
            to_zip=400018&
            sp_comments=test*/

            //https://quichelp.com/json/Movers_Json/data/?service=insert_movers_job
            // &userid=191
            // &firstname=Shrikar
            // &lastname=K
            // &email=k.shrikar@gmail.com
            // &phone=7620222896
            // &service_type=personal
            // &no_helpers=1
            // &approx_distance=200
            // &frm_address1=Badangpet, Hyderabad
            // &frm_address2=Telangana
            // &pref_date=03/10/2020
            // &pref_time=10:30AM
            // &frm_city=Hyderabad
            // &frm_zip=5800085
            // &to_address1=Viman nagar, Pune
            // &to_address2=MH
            // &to_city=Pune
            // &to_zip=400018
            // &sp_comments=test

            URI uri = null;

            String email_st = "";
            if (EMAIL_ID != null && !EMAIL_ID.equals(""))
                email_st = EMAIL_ID;
            else if (email != null && !email.equals(""))
                email_st = email;



            try {
                uri = new URI("https", path + "service=" + "insert_movers_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&lastname=" + last_name +
                        "&email=" + email_st +
                        "&phone=" + mobileNumber +
                        "&service_type=" + typeofService +
                        "&no_helpers=" + proFessionals +
                        "&approx_distance=" + distanceinkm +
                        "&frm_address1=" + address_1 +
                        "&frm_address2=" + address_2 +
                        "&pref_date=" + date_data +
                        "&pref_time=" + time_data +
                        "&frm_city=" + selectCity +
                        "&frm_zip=" + Zipcode +
                        "&to_address1=" + address_1_to +
                        "&to_address2=" + address_2_to +
                        "&to_city=" + selectCity2 +
                        "&to_zip=" + Zipcode2 +
                        "&sp_comments=" + special_comments, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status1 = object.getBoolean("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Log.e("status",status);

            if (status1 == true) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status1 == true) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_FROM) {
            if (resultCode == RESULT_OK) {

                Place place = Autocomplete.getPlaceFromIntent(data);
                updateDetails(place, getString(R.string.from));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                QuickHelpLog.i("MoversBookNow", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_TO){
            if (resultCode == RESULT_OK) {

                Place place = Autocomplete.getPlaceFromIntent(data);
                updateDetails(place, getString(R.string.to));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                QuickHelpLog.i("MoversBookNow", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private void updateDetails(Place place, String fromTo) {
        strAddresName = place.getName().toString();
        QuickHelpLog.i(TAG, "Place: " + place.getName());
        LatLng latLang = place.getLatLng();

        if (getString(R.string.from).equals(fromTo))
            fromLatLong = latLang.latitude+","+latLang.longitude;
        else
            toLatLong = latLang.latitude+","+latLang.longitude;

        String[] addresses = place.getAddress().toString().split(",");
        if (addresses != null && addresses.length >= 4) {

            addrsInfo = place.getAddress().toString().replace((addresses[(addresses.length - 3)] + ","), "")
                    .replace((addresses[(addresses.length - 2)] + ","), "")
                    .replace(addresses[(addresses.length - 1)], "");
            city = addresses[(addresses.length - 3)];
            String stateList = addresses[(addresses.length - 2)].trim();
            state = stateList.replaceAll("\\d", "");
            pin = addresses[(addresses.length - 2)].replaceAll("\\D", "");
            country = addresses[addresses.length - 1];
            landMark = "";
        } else {
            if (addresses != null && addresses.length == 3) {
                addrsInfo = "";
                city = addresses[0];
                state = addresses[1].replaceAll("\\d", "").trim();
                country = addresses[2];
                landMark = "";
                pin = addresses[1].replaceAll("\\D", "").trim();
            } else {
                addrsInfo = "";
                city = addresses[0];
                state = "";
                country = "";
                landMark = "";
                pin = "";
            }
        }

        if (Utils.isValidStr(pin)){
            if (getString(R.string.from).equals(fromTo))
                zip_code.setText(pin+"");
            else
                zip_codeto.setText(pin+"");
        }

        if (Utils.isValidStr(city)){
            if (getString(R.string.from).equals(fromTo))
                select_city.setText(city);
            else
                select_city_2.setText(city);

        }

        if (Utils.isValidStr(place.getAddress())){
            if (getString(R.string.from).equals(fromTo))
                address1.setText(place.getAddress());
            else
                address1to.setText(place.getAddress());

        }

        if (Utils.isValidStr(state)) {
            if (getString(R.string.from).equals(fromTo))
                address2.setText(state);
            else
                address2to.setText(state);
            if (Utils.isValidStr(fromLatLong) && Utils.isValidStr(toLatLong)) {
                String[] fromLatLongArray = fromLatLong.split(",");
                String[] toLatLongArray = toLatLong.split(",");
                distance = calculateDistance(Double.valueOf(fromLatLongArray[0]), Double.valueOf(fromLatLongArray[1]), Double.valueOf(toLatLongArray[0]), Double.valueOf(toLatLongArray[1]));
                distanceinkm = (distance * km);
            approx_dis.setText(distanceinkm+"");
            }

        }

        if (Utils.isValidStr(place.getAddress())){
            /*getLocation.setText("");
            getLocation.setText(place.getAddress());*/
        }

    }


    private double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


}