package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import fragment_adapter.Movers_adapter;

/**
 * Created by Admin on 11/11/2016.
 */

public class Movers extends AppCompatActivity implements View.OnClickListener {
    static ViewPager viewpage;
    TextView whatweprovider, booknow;
    View provider, booknows;
    ImageView language;
    TextView textSign;
    DrawerLayout dLayout;
    ListView ltst1;
    ImageView image;
    LinearLayout id;
    SharedPreferences check,get_id;
    LinearLayout l3,l4;
    String value;
    TextView drawer_profile,drawer_callhistory,drawer_change_password,drawer_message,drawer_logout;

    String USER_ID=Serviceselectionpage.USER_ID;
    TextView drawer_user_privacy_policy,drawer_prof_privacy_policy,drawer_user_terms_condtns;

    TextView change_password_professional,job_history_prof,message_prof,profile_professional;

    View drawer_profile_view,drawer_callhistory_view,drawer_change_password_view,change_password_professional_view,
            profile_professional_view,job_history_prof_view;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movers);

        drawer_user_privacy_policy=(TextView)findViewById(R.id.user_privacy);
        drawer_user_terms_condtns=(TextView)findViewById(R.id.uesr_terms_cond);
        drawer_user_terms_condtns.setText("TERMS & CONDITIONS");
        drawer_prof_privacy_policy=(TextView)findViewById(R.id.professional_privacy) ;

        viewpage = (ViewPager) findViewById(R.id.viewpager);
        whatweprovider = (TextView) findViewById(R.id.t3);
        language=(ImageView) findViewById(R.id.imageFr);
        id=(LinearLayout) findViewById(R.id.listLay);
        l3=(LinearLayout) findViewById(R.id.drawer2);
        l4=(LinearLayout) findViewById(R.id.drawerHeader);
        dLayout=(DrawerLayout) findViewById(R.id.drawer1);
        image=(ImageView) findViewById(R.id.image);
        textSign=(TextView) findViewById(R.id.textView1);
        booknow = (TextView) findViewById(R.id.t4);
        provider = (View) findViewById(R.id.v3);
        booknows = (View) findViewById(R.id.v4);
        drawer_profile=(TextView) findViewById(R.id.profile);
        drawer_callhistory=(TextView)findViewById(R.id.callhistory);
        drawer_change_password=(TextView) findViewById(R.id.change_password);
        drawer_message=(TextView)findViewById(R.id.message);
        drawer_logout=(TextView) findViewById(R.id.logout);

        drawer_profile_view=(View) findViewById(R.id.profile_view);
        drawer_callhistory_view=(View)findViewById(R.id.callhistory_view);
        drawer_change_password_view=(View) findViewById(R.id.change_password_view);

        change_password_professional_view=(View) findViewById(R.id.change_password_prof_view);
        profile_professional_view=(View) findViewById(R.id.profile_prof_view);
        job_history_prof_view=(View) findViewById(R.id.job_prof_view);

        change_password_professional=(TextView) findViewById(R.id.change_password_prof);
        profile_professional=(TextView) findViewById(R.id.profile_prof);
        job_history_prof=(TextView) findViewById(R.id.job_prof);

        LinearLayout professionla_linear=(LinearLayout) findViewById(R.id.professional_linear);
        LinearLayout user_linear=(LinearLayout) findViewById(R.id.user_linear);

        check=getSharedPreferences("Default_Language",MODE_PRIVATE);
        value=check.getString("Value","");

       /* if(value.equals("CHENNAI"))
        {
            language.setVisibility(View.INVISIBLE);
            id.setBackgroundResource(R.color.app_heading2);
            l3.setBackgroundResource(R.color.app_heading2);
            l4.setBackgroundResource(R.color.app_heading2);
        }*/


        if(USER_ID!=null)
        {
            textSign.setVisibility(View.GONE);
            drawer_logout.setVisibility(View.VISIBLE);
        }

        get_id=getSharedPreferences("My_id",MODE_PRIVATE);
        if(get_id.getString("USER","").equals("true"))
        {
            user_linear.setVisibility(View.VISIBLE);
            professionla_linear.setVisibility(View.GONE);
        }

        if(get_id.getString("PROF","").equals("true"))
        {
            user_linear.setVisibility(View.GONE);
            professionla_linear.setVisibility(View.VISIBLE);
        }

        if(USER_ID!=null)
        {
            //Signin.setVisibility(View.GONE);
            drawer_logout.setVisibility(View.VISIBLE);


            if(get_id.getString("USER","").equals("true"))
            {
                drawer_profile.setVisibility(View.VISIBLE);
                drawer_callhistory.setVisibility(View.VISIBLE);
                drawer_change_password.setVisibility(View.VISIBLE);

                drawer_profile_view.setVisibility(View.VISIBLE);
                drawer_callhistory_view.setVisibility(View.VISIBLE);
                drawer_change_password_view.setVisibility(View.VISIBLE);

            }
            if(get_id.getString("PROF","").equals("true"))
            {
                change_password_professional.setVisibility(View.VISIBLE);
                profile_professional.setVisibility(View.VISIBLE);
                job_history_prof.setVisibility(View.VISIBLE);

                change_password_professional_view.setVisibility(View.VISIBLE);
                profile_professional_view.setVisibility(View.VISIBLE);
                job_history_prof_view.setVisibility(View.VISIBLE);
            }
        }


        whatweprovider.setOnClickListener(this);
        booknow.setOnClickListener(this);
        provider.setOnClickListener(this);
        booknows.setOnClickListener(this);
        Movers_adapter adapter = new Movers_adapter(getSupportFragmentManager());
        viewpage.setAdapter(adapter);

        image.setOnClickListener(this);
        drawer_profile.setOnClickListener(this);
        drawer_callhistory.setOnClickListener(this);
        drawer_change_password.setOnClickListener(this);

        drawer_message.setOnClickListener(this);
        drawer_logout.setOnClickListener(this);
        textSign.setOnClickListener(this);
        drawer_user_privacy_policy.setOnClickListener(this);
        drawer_prof_privacy_policy.setOnClickListener(this);
        drawer_user_terms_condtns.setOnClickListener(this);

        profile_professional.setOnClickListener(this);
        job_history_prof.setOnClickListener(this);
        change_password_professional.setOnClickListener(this);

        viewpage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (viewpage.getCurrentItem() == 0) {
                    provider.setVisibility(View.VISIBLE);
                    booknows.setVisibility(View.INVISIBLE);
                }
                else {
                    provider.setVisibility(View.INVISIBLE);
                    booknows.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        language.setOnClickListener(new Language_change(Movers.this,language));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.t3:
                viewpage.setCurrentItem(0);
                break;
            case R.id.t4:
                if (Utils.isValidStr(USER_ID))
                    viewpage.setCurrentItem(1);
                else
                    DialogManager.showToast(this, getString(R.string.not_logged_in_msg));
                break;

            case R.id.image:
                dLayout.openDrawer(id);
                break;
            case R.id.textView1:
                Intent n=new Intent(Movers.this,Sign_activity.class);
               // n.putExtra("unique_id","movers");
                startActivity(n);
                break;
            case R.id.profile:
                Intent intent=new Intent(Movers.this,Drawerlayout_profile.class);
                startActivity(intent);

                break;
            case R.id.change_password:
                Intent password=new Intent(Movers.this,Change_password.class);
                startActivity(password);

                break;
            case R.id.callhistory:
                Intent i=new Intent(Movers.this,Drawerlayout_callhistory.class);
                i.putExtra("getPage","default");
                startActivity(i);

                break;
            case R.id.message:
                Intent msg=new Intent(Movers.this,Drawer_message.class);
                startActivity(msg);
                break;
            case R.id.logout:
                SharedPreferences.Editor sign_out=get_id.edit();
                sign_out.putString("my_id",null).
                        putString("email",null).putString("Firstname",null)
                        .putString("Lastname",null).putString("phone_num",null)
                        .putString("USER","false").putString("PROF","false").commit();

                onRestart();
                Toast.makeText(getApplicationContext(),"Successfully SignOut",Toast.LENGTH_SHORT).show();
                //finish();
                break;
            case R.id.user_privacy:
                Intent user_privacy=new Intent(getApplicationContext(),Privacy_policy_user.class);
                startActivity(user_privacy);

                break;
            case R.id.uesr_terms_cond:
                Intent uesr_terms_cond=new Intent(getApplicationContext(),Privacy_policy_terms_conditions.class);
                startActivity(uesr_terms_cond);

                break;
            case R.id.professional_privacy:
                Intent professional_privacy=new Intent(getApplicationContext(),Privacy_policy_professionals.class);
                startActivity(professional_privacy);
                break;
            case R.id.change_password_prof:
                Intent password_pro=new Intent(getApplicationContext(),Change_password.class);
                password_pro.setAction("prof");
                startActivity(password_pro);
                break;
            case R.id.job_prof:
                Intent job_pro=new Intent(getApplicationContext(),Professional_job_history.class);
                job_pro.putExtra("getPage","default");
                startActivity(job_pro);
                break;
            case R.id.profile_prof:
                Intent profile_prof=new Intent(getApplicationContext(),My_profile_professional.class);
                //intent.putExtra("unique_id","landing_page");
                startActivity(profile_prof);

                break;
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}