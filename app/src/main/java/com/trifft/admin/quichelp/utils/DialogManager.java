package com.trifft.admin.quichelp.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;

import static com.trifft.admin.quichelp.utils.Utils.getDialog;

/**
 * Created by fatima on 19-10-2019.
 */
public class DialogManager implements View.OnTouchListener {

    private static Dialog mDialog;
    private static Button mDialogBtn, mDialogCancelBtn, mCancelBtn;
    private static TextView mDialogAlertTxt;
    private static TextView mDialogTitleTxt;
    public static Dialog progress;


    public static void showToast(Context context, String message) {
        Toast mToast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        TextView mToastTxt = mToast.getView().findViewById(
                android.R.id.message);
        mToast.show();
    }


    private static String getMessage(String mMessage) {
        return (mMessage.trim().charAt(mMessage.trim().length() - 1) == '.' || mMessage.charAt(mMessage.length() - 1) == '?') ? mMessage : mMessage + ".";
    }


    public static void showProgress(Context mContext) {
        if(progress!=null && progress.isShowing()) {
            progress.dismiss();
            progress =null;
        }
        progress = getLoadingprogress(mContext);
        progress.show();
    }

    public static void hideDialog(Context cxt) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public static void hideProgress(Context mContext) {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    private static Dialog getLoadingprogress(Context mContext) {
        mDialog = getDialog(mContext, R.layout.progress);
        mDialog.setCancelable(false);

        return mDialog;
    }

    public static void showSingleBtnPopup(final Context mContext,
                                          final DialogMangerCallback mDialoginterface, String mTitle,
                                          String mMessage, String mBtnTxt) {

        /*final Dialog*/ mDialog = getDialog(mContext, R.layout.popup_single_btn);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        ViewGroup root = mDialog.findViewById(R.id.parent_view_for_font);

//        mCancelBtn = mDialog.findViewById(R.id.cancel_dialog);
        mDialogAlertTxt = mDialog.findViewById(R.id.alert_text);
        mDialogTitleTxt = mDialog.findViewById(R.id.header_txt);

        mDialogBtn = mDialog.findViewById(R.id.process_btn);
        mDialogTitleTxt.setText(mTitle);
        mDialogAlertTxt.setText(getMessage(mMessage));
        if (Utils.isValidStr(mBtnTxt))
            mDialogBtn.setText(mBtnTxt);
        else
            mDialogBtn.setText(mContext.getString(R.string.ok));
        // if you want only dismiss on btn click then dont implement ok/cancel method in class where ever you are using this, just pass null in place of second parameter of showSingleBtnPopup method

        mDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                if (mDialoginterface != null)
                    mDialoginterface.onOkClick(view);
            }
        });

        mDialog.show();
    }

    public static void showSingleBtnEditTxtPopup(final Context mContext,
                                                 final DialogMangerCallback mDialoginterface, String mTitle, String hint, String enteredData, int maxLength, String mBtnTxt) {

       /* final Dialog*/ mDialog = getDialog(mContext, R.layout.popup_single_btn_edit_text);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        mCancelBtn = mDialog.findViewById(R.id.cancel_btn);
        mDialogTitleTxt = mDialog.findViewById(R.id.header_txt);
        mDialogBtn = mDialog.findViewById(R.id.btnOk);
        final EditText mEditText = mDialog.findViewById(R.id.edit_text_value);

        mDialogTitleTxt.setText(mTitle);
        mDialogBtn.setText(mBtnTxt);
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        mEditText.setHint(hint);
        mEditText.setText(enteredData);
        // if you want only dismiss on btn click then dont implement ok/cancel method in class where ever you are using this, just pass null in place of second parameter of showSingleBtnPopup method
        mDialogBtn.setOnClickListener(v -> {
            mDialog.dismiss();
            if (mDialoginterface != null) {
                v.setTag(mEditText.getText().toString());
                mDialoginterface.onOkClick(v);
            }
        });
        mCancelBtn.setOnClickListener(v -> mDialog.dismiss());
        mDialog.show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }


    public static void showAlert(final Context mContext, String mTitle,
                                 String mMessage, String mBtnTxt) {

        mDialog = getDialog(mContext, R.layout.popup_single_btn);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        ViewGroup root = mDialog.findViewById(R.id.parent_view_for_font);

//        mCancelBtn = mDialog.findViewById(R.id.cancel_dialog);
        mDialogAlertTxt = mDialog.findViewById(R.id.alert_text);
        mDialogTitleTxt = mDialog.findViewById(R.id.header_txt);

        mDialogBtn = mDialog.findViewById(R.id.process_btn);
//        mDialogCancelBtn = mDialog.findViewById(R.id.cancel_btn);
        mDialogCancelBtn.setVisibility(View.GONE);
        mDialogTitleTxt.setText(mTitle);
        mDialogAlertTxt.setText(getMessage(mMessage));
        mDialogBtn.setText(mBtnTxt);

        mDialogBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }


    public static void showConfirmPopup(final Context mContext,
                                        final DialogMangerCallback mDialoginterface, String mTitle,
                                        String mMessage, String positiveBtn, String negativeBtn) {
        mDialog = getDialog(mContext, R.layout.popup_general_confirm);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        ViewGroup root = mDialog.findViewById(R.id.parent_view_for_font);

        mCancelBtn = mDialog.findViewById(R.id.cancel_dialog);
        mDialogAlertTxt = mDialog.findViewById(R.id.alert_text);
        mDialogTitleTxt = mDialog.findViewById(R.id.header_txt);

        mDialogBtn = mDialog.findViewById(R.id.process_btn);

        mDialogTitleTxt.setText(mTitle);
        mDialogAlertTxt.setText(getMessage(mMessage));
        mDialogBtn.setText(positiveBtn);
        mCancelBtn.setText(negativeBtn);

        mDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                mDialoginterface.onOkClick(view);
            }
        });

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                mDialoginterface.onCancelClick(view);
            }
        });

        mDialog.show();
    }
}
