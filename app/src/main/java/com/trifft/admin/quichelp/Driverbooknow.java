package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.model.DriverServicTypeMasterResponse;
import com.trifft.admin.quichelp.model.IdNameModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by MINE on 07-08-2017.
 */

public class Driverbooknow extends Fragment implements AdapterView.OnItemSelectedListener, CommonInterface {

    String first_name, mobileNumber, last_name, email, date_data, time_data, date_data_to, time_data_to,
            special_comments, selectedCity, zipcode_st, overnite_st, serviceType_st, noOfDrivers_st;
    ProgressDialog progressDialog;
    SharedPreferences check;
    String value;
    Button ac;
    EditText no_of_driver, to_date, to_time;

    EditText firstname, lastname, date, time, /*select_city,*/
            specialComments, address1, address2,
            zip_code, mobile_number, email_id, overnite;

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    private Date start, end;
    private EditText total_time;
    Spinner spnCountry, spnServcieType, spnOverNight;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter, serviceTypeAdapter, overNightAdapter;
    private ArrayList<String> serviceTypeList = new ArrayList<>();
    private String serviceTypeFirsItem = "Domestic/Outstation", overNightFirstItem = "Overnight Driver Required";
    private ArrayList<String> overNightList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.driverbooknow, container, false);

        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        //address1=(EditText) v.findViewById(R.id.address1);
        //address2=(EditText) v.findViewById(R.id.address2);
        email_id = (EditText) v.findViewById(R.id.email);
        zip_code=(EditText) v.findViewById(R.id.zincodeET);
        mobile_number = (EditText) v.findViewById(R.id.phone_num);
        no_of_driver = (EditText) v.findViewById(R.id.no_of_driver);
//        overnite = (EditText) v.findViewById(R.id.overnite);
        spnOverNight = v.findViewById(R.id.spn_overNight);
        date = (EditText) v.findViewById(R.id.date);
        time = (EditText) v.findViewById(R.id.time);
        to_date = (EditText) v.findViewById(R.id.to_date);
        to_time = (EditText) v.findViewById(R.id.to_time);
        ac = (Button) v.findViewById(R.id.getaprice);
        total_time = (EditText) v.findViewById(R.id.totaltime);
        //select_country=(EditText) v.findViewById(R.id.selectcountry);
//        select_city = (EditText) v.findViewById(R.id.select_city);

        spnCountry = v.findViewById(R.id.spn_country);
        spnServcieType = v.findViewById(R.id.spn_typeOfService);

        specialComments = (EditText) v.findViewById(R.id.specialcomments);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            email_id.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);

        getServiceTypeMaster();

        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);

        serviceTypeList.add(serviceTypeFirsItem);
        serviceTypeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, serviceTypeList);
        spnServcieType.setAdapter(serviceTypeAdapter);
        spnServcieType.setOnItemSelectedListener(this);

        overNightList.add(overNightFirstItem);
        overNightList.add("Yes");
        overNightList.add("No");
        overNightAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, overNightList);
        spnOverNight.setAdapter(overNightAdapter);
        spnOverNight.setOnItemSelectedListener(this);
     /*  if(value.equals("CHENNAI"))
        {
            //ac.setBackgroundResource(R.drawable.ed_button2);
            select_city.setText("CHENNAI");
           // select_city.setEnabled(false);
        }
        else{select_city.setText("MUMBAI");}*/

        no_of_driver.setOnClickListener(new Popup_fiveroom(getActivity(), no_of_driver, "driver"));
//        overnite.setOnClickListener(new Popup_fiveroom(getActivity(), overnite, "driver"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));
        to_date.setOnClickListener(new Date_picker(getActivity(), to_date));
        to_time.setOnClickListener(new TimePicker(getActivity(), to_time));

        //select_city.setOnClickListener(new Select_country(getActivity(),select_city));

//        select_city.setOnClickListener(new Select_country(getActivity(), select_city));

        to_time.addTextChangedListener(new TextWatcher() {
            private Date enddate;
            private Date startdate;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                date_data = date.getText().toString();
                time_data = time.getText().toString();
                date_data_to = to_date.getText().toString();
                time_data_to = to_time.getText().toString();

                if (!date.equals("") && !time.equals("") && !to_date.equals("") && !to_time.equals("")) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd-MM-yyyyHH:mm");
                    String start = date_data + time_data;
                    String end = date_data_to + time_data_to;

                    try {
                        startdate = dateFormat.parse(start);
                        enddate = dateFormat.parse(end);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (enddate == null && startdate == null) {

                    } else {

                        long diff = enddate.getTime() - startdate.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;

                        if (enddate.before(startdate)) {

                            Toast.makeText(getActivity(), "Please enter correct date value", Toast.LENGTH_SHORT).show();
                            total_time.setVisibility(View.GONE);

                        } else if (days > 9) {
                            total_time.setVisibility(View.VISIBLE);
                            total_time.setText("Total time: " + days + "Days" + hours + "Hours");
                        } else if (days >= 1) {
                            total_time.setVisibility(View.VISIBLE);
                            total_time.setText("Total time: " + days + "Day" + hours + "Hours");
                        } else if (days <= 0) {
                            total_time.setVisibility(View.VISIBLE);
                            total_time.setText("Total time: " + hours + "Hours");
                        }


                    }
                } else {
                    total_time.setVisibility(View.GONE);
                }
            }
        });


        //select_city.setOnClickListener(new Select_city_listner(getActivity(),select_city,select_country.getText().toString()));


        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 /* https://quichelp.com/json/Driving_Json/data/?
            service=insert_driving_job&
            userid=191&
            firstname=Shrikar&
            lastname=K&
            email=k.shrikar%40gmail.com&
            phone=07620222896&
            from_city=Chennai&
            pincode=500058&
            no_drivers=1&
            from_date=03/10/2020&
            from_time=8:30AM&
            to_date=03/10/2020&
            to_time=10:30AM&
            night_driving=no&
            price=&
            sp_comments=test&
            service_type=outstation*/

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = email_id.getText().toString();
                mobileNumber = mobile_number.getText().toString();
                selectedCity = spnCountry.getSelectedItem().toString();
                zipcode_st = zip_code.getText().toString();
                noOfDrivers_st = no_of_driver.getText().toString().trim();
                date_data = date.getText().toString();
                time_data = time.getText().toString();
                date_data_to = to_date.getText().toString();
                time_data_to = to_time.getText().toString();
                overnite_st = /*overnite.getText().toString();*/spnOverNight.getSelectedItem().toString();
                serviceType_st = spnServcieType.getSelectedItem().toString();
                special_comments = specialComments.getText().toString();

                //address_1=address1.getText().toString();
                //address_2=address2.getText().toString();
//                selectCity = select_city.getText().toString();


                if (first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.equals("")) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (date_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the From date", Toast.LENGTH_SHORT).show();
                } else if (time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the From time", Toast.LENGTH_SHORT).show();
                } else if (date_data_to.equals("")) {
                    Toast.makeText(getActivity(), "Select the To date", Toast.LENGTH_SHORT).show();
                } else if (time_data_to.equals("")) {
                    Toast.makeText(getActivity(), "Select the To time", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select city");
                } else if (Utils.isValidStr(selectedCity) && selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select city");
                } else if (!Utils.isValidStr(zipcode_st)){
                    DialogManager.showToast(getActivity(), "Please enter zipcode");
                }else if (overnite_st.equalsIgnoreCase(overNightFirstItem)){
                    DialogManager.showToast(getActivity(), "Please select Over Night driving required or not");
                } else if (serviceType_st.equalsIgnoreCase(serviceTypeFirsItem)) {
                    DialogManager.showToast(getActivity(), "Please select Domestic/Outstation");
                } else if (noOfDrivers_st.equalsIgnoreCase(getString(R.string.noOfDrivers))) {
                    DialogManager.showToast(getActivity(), "Please select No Of drivers required");
                } else if (!Utils.isValidStr(noOfDrivers_st)) {
                    DialogManager.showToast(getActivity(), "Please select No Of drivers required");
                } else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {
                    new Mytask().execute();
                }
            }
        });


        return v;
    }

    private void getServiceTypeMaster() {
        /*String service = "service_types";
        String category = "6";
        APIRequestHandler.getInstance().driverServiceTypes(service, category, getActivity(), this);*/

        new ServiceTypeMasterAsync().execute();
    }

    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            } else if (responseObj instanceof DriverServicTypeMasterResponse) {
                DriverServicTypeMasterResponse response = (DriverServicTypeMasterResponse) responseObj;
                if (response.isStatus()) {
                    String message = response.getMessage();
                    DriverServicTypeMasterResponse actualResponse = new Gson().fromJson(message, DriverServicTypeMasterResponse.class);
                    if (actualResponse.getDomestic() != null) {
                        IdNameModel domestic = actualResponse.getDomestic().get(0);
                        serviceTypeList.add(domestic.getName());
                    }
                    if (actualResponse.getOutstation() != null){
                        IdNameModel outstation = actualResponse.getOutstation().get(0);
                        serviceTypeList.add(outstation.getName());
                    }
                     serviceTypeAdapter.notifyDataSetChanged();
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Service Types Master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }


    private class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", price = "", jod_id = "", message = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            /* https://quichelp.com/json/Driving_Json/data/?
            service=insert_driving_job&
            userid=191&
            firstname=Shrikar&
            lastname=K&
            email=k.shrikar%40gmail.com&
            phone=07620222896&
            from_city=Chennai&
            pincode=500058&
            no_drivers=1&
            from_date=03/10/2020&
            from_time=8:30AM&
            to_date=03/10/2020&
            to_time=10:30AM&
            night_driving=no&
            price=&
            sp_comments=test&
            service_type=outstation*/

            String path = getActivity().getResources().getString(R.string.url_service) + "Driving_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();


            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "insert_driving_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&lastname=" + last_name +
                        "&email=" + email +
                        "&phone=" + mobileNumber +
                        "&from_city=" + selectedCity +
                        "&pincode=" +zipcode_st+
                        "&no_drivers=" +  noOfDrivers_st+
                        "&from_date=" + date_data +
                        "&from_time=" + time_data + "" +
                        "&to_date=" + date_data_to +
                        "&to_time=" + time_data_to +
                        "&night_driving=" + overnite_st +
                        "&price=" + "" +
                        /*"&frm_address1=" + "" +
                        "&frm_address2=" + "" +*/
                        "&sp_comments=" + special_comments +
                        "&service_type="+serviceType_st, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status = object.getString("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status);

            if (status.equals("true")) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), "Please select country");
                break;
            case R.id.spn_typeOfService:
                spnServcieType.setTag(serviceTypeList.get(i));
                break;
            case R.id.spn_overNight:
                spnOverNight.setTag(overNightList.get(i));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private class ServiceTypeMasterAsync extends AsyncTask<Void, Void, Void> {

        String status_M = "", message_M = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /*progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            /* https://quichelp.com/json/ServicePrice_Json/data?service=service_types&category=6*/

            String path = getActivity().getResources().getString(R.string.url_service) + "ServicePrice_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();


            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "service_types" +
                        "&category=" + "6", null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status_M = object.getString("status");
                message_M = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status_M);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status_M.equals("true")) {
                DriverServicTypeMasterResponse actualResponse = new Gson().fromJson(message_M, DriverServicTypeMasterResponse.class);
                if (actualResponse.getDomestic() != null) {
                    IdNameModel domestic = actualResponse.getDomestic().get(0);
                    serviceTypeList.add(domestic.getName());
                }
                if (actualResponse.getOutstation() != null){
                    IdNameModel outstation = actualResponse.getOutstation().get(0);
                    serviceTypeList.add(outstation.getName());
                }
                serviceTypeAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getActivity(), message_M, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
