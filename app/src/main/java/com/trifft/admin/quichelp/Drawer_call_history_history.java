package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trifft.admin.quichelp.model.JobItemModel;
import com.trifft.admin.quichelp.utils.QuickHelpLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fragment_adapter.Drawer_callhistory_history_listadapter;

/**
 * Created by Admin on 12/21/2016.
 */

public class Drawer_call_history_history extends Fragment {
    ListView list;
    ProgressDialog progressDialog;
    public static String JOD_ID="jod_id",JOB_STATUS="job_status",
            DATE="date",TIME="time",PRICE="price",PAYMENT_STATUS="payment_status";

    String USER_ID=Serviceselectionpage.USER_ID;
    TextView empty_text;

    String JOB_status;

    ArrayList<HashMap<String,String>> arrayListJob=new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String,String>> upcomingListJob=new ArrayList<HashMap<String, String>>();
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {

        View vv=inflater.inflate(R.layout.drawer_call_history_history,container,false);
        list=(ListView)vv.findViewById(R.id.list);

        empty_text=(TextView)vv.findViewById(R.id.empty_text);

        if(USER_ID==null)
        {
            //list.setVisibility(View.GONE);
            empty_text.setVisibility(View.VISIBLE);
            empty_text.setText("Sign in your application");
        }
        else
        {

            new JobHistory().execute(USER_ID);
        }

        //new JobHistory().execute("24");


        return vv;
    }

    String status="",message="";
    private class JobHistory extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(String... voids) {

            String result = null;

            /*//www.quichelp.com/json/*/
//            String path=getActivity().getResources().getString(R.string.url_service)+"Clean_Json/data?";
            String path=getActivity().getResources().getString(R.string.url_service)+"Booking_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            /* OLD
            //www.quichelp.com/json/Clean_Json/data?service="+"get_all_jobs"+"&userid=*/
            /* NEW
            https://www.quichelp.com/json/Booking_Json/data/?service=get_all_jobs&userid=191*/

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"get_all_jobs"+"&userid="+voids[0],null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status",status);Log.e("message",message);
            QuickHelpLog.e("Job History :" , result);

            Type listType = new TypeToken<List<JobItemModel>>() {}.getType();
            ArrayList<HashMap<String,String>> arrayList=new ArrayList<HashMap<String, String>>();
            if(status.equals("true")) {
                try {
                    JSONObject allJobs = jsn_response.getJSONObject("alljobs");

                    String cleaningArray = allJobs.getJSONArray("cleaning").toString();
                    ArrayList<JobItemModel> cleaningList = new Gson().fromJson(cleaningArray, listType);
                    if (cleaningList != null && cleaningList.size() > 0){
                        addItemsToList(cleaningList);
                    }

                    String moversArray = allJobs.getJSONArray("movers").toString();
                    ArrayList<JobItemModel> moversList = new Gson().fromJson(moversArray, listType);
                    if (moversList != null && moversList.size() > 0){
                        addItemsToList(moversList);
                    }

                    String paintingArray = allJobs.getJSONArray("painting").toString();
                    ArrayList<JobItemModel> paintingList = new Gson().fromJson(paintingArray, listType);
                    if (paintingList != null && paintingList.size() > 0){
                        addItemsToList(paintingList);
                    }

                    String acArray = allJobs.getJSONArray("ac service").toString();
                    ArrayList<JobItemModel> acList = new Gson().fromJson(acArray, listType);
                    if (acList != null && acList.size() > 0){
                        addItemsToList(acList);
                    }


                    String chefArray = allJobs.getJSONArray("chef").toString();
                    ArrayList<JobItemModel> chefList = new Gson().fromJson(chefArray, listType);
                    if (chefList != null && chefList.size() > 0){
                        addItemsToList(chefList);
                    }

                    String drivingArray = allJobs.getJSONArray("driving").toString();
                    ArrayList<JobItemModel> drivingList = new Gson().fromJson(drivingArray, listType);
                    if (drivingList != null && drivingList.size() > 0){
                        addItemsToList(drivingList);
                    }

                    String realStateArray = allJobs.getJSONArray("real estate").toString();
                    ArrayList<JobItemModel> realStateList = new Gson().fromJson(realStateArray, listType);
                    if (realStateList != null && realStateList.size() > 0){
                        addItemsToList(realStateList);
                    }

                    String beautyArray = allJobs.getJSONArray("Makeup Spa").toString();
                    ArrayList<JobItemModel> beautyList = new Gson().fromJson(beautyArray, listType);
                    if (beautyList != null && beautyList.size() > 0){
                        addItemsToList(beautyList);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            /*{
                JSONArray array=jsn_response.optJSONArray("alljobs");

                for(int i=0;i<array.length();i++) {

                    JSONObject object;
                    try {
                        object =array.getJSONObject(i);


                        JOB_status=object.getString("job_status");
                        if(JOB_status.equals("cancelled") || JOB_status.equals("expired") || JOB_status.equals("closed")) {
                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("jod_id", object.getString("job_id"));
                            map.put("job_status", object.getString("job_status"));
                            map.put("date", object.getString("date"));
                            map.put("time", object.getString("time"));
                            map.put("price", object.getString("price"));
                            map.put("payment_status", object.getString("payment_status"));


                            arrayList.add(map);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            return arrayList;*/

            return arrayListJob;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true")) {
                Drawer_callhistory_history_listadapter adapter=new Drawer_callhistory_history_listadapter(getActivity(),aVoid);
                list.setAdapter(adapter);

                if (upcomingListJob != null && upcomingListJob.size() > 0){
                    Drawer_call_history_upcoming.setListItems(upcomingListJob);
                }

                if(adapter.isEmpty())
                {
                    empty_text.setVisibility(View.VISIBLE);
                    empty_text.setText("No Jobs generated");
                }
            }
        }
    }

    private void addItemsToList(ArrayList<JobItemModel> arrayList) {
        upcomingListJob.clear();
        String jobStatus = "";
        try{
            for (JobItemModel model : arrayList){
                jobStatus=model.getJob_status();
                if(jobStatus.equals("cancelled") || jobStatus.equals("expired") || jobStatus.equals("closed")|| jobStatus.equals("archiv")) {
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("jod_id", model.getJob_id());
                    map.put("job_status", model.getJob_status());
                    map.put("date", model.getPref_date());
                    map.put("time",model.getPref_time() );
                    map.put("price", model.getPrice());
                    map.put("payment_status", model.getPayment_status());

                    arrayListJob.add(map);

                } else if (jobStatus.equals("new") || jobStatus.equals("open")|| jobStatus.equals("active")){
                    HashMap<String, String> map = new HashMap<String, String>();

                    map.put("jod_id", model.getJob_id());
                    map.put("job_status", model.getJob_status());
                    map.put("date", model.getPref_date());
                    map.put("time",model.getPref_time() );
                    map.put("price", model.getPrice());
                    map.put("payment_status", model.getPayment_status());

                    upcomingListJob.add(map);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

}
