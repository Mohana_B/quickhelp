package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by PSDeveloper on 11/10/2016.
 */

public class Signin_professional extends Fragment implements View.OnClickListener{

    String PROFESSIONAL_ID,FIRSTNAME,LASTNAME,EMAIL,PHONE_NUM;

    ProgressDialog progressDialog;
    TextView text,forgot_password;
    SharedPreferences check;
    String value;
    Button login;
    EditText username,password;
    String user_name,pass_word;
    String pattern2,pattern1,name;
    ImageView user,pssd;

    SharedPreferences my_id;


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.user_professional,container,false);
        text=(TextView)v.findViewById(R.id.professionaltxt);

        forgot_password=(TextView) v.findViewById(R.id.forgot_password);

        login=(Button)v.findViewById(R.id.button);

        password=(EditText) v.findViewById(R.id.password);
        username=(EditText) v.findViewById(R.id.username);
        forgot_password.setOnClickListener(this);

        check=getActivity().getSharedPreferences("Default_Language",getActivity().MODE_PRIVATE);
        value=check.getString("Value","");

       /* if(value.equals("CHENNAI"))
        {
            signin_prof.setBackgroundResource(R.drawable.ed_button2);
            text.setTextColor(getActivity().getResources().getColor(R.color.app_heading2));
        }*/


        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n=new Intent(getActivity(),Createaccount.class);
                n.putExtra(Createaccount.EXTRA_PROF_USER, true);
                startActivity(n);
            }
        });



        my_id=getActivity().getSharedPreferences("My_id",getActivity().MODE_PRIVATE);

        pattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        pattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_name=username.getText().toString();
                pass_word=password.getText().toString();

                if(user_name.length()==0||user_name.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the username" ,Toast.LENGTH_SHORT).show();
                }
                else if(pass_word.length()==0||pass_word.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the password" ,Toast.LENGTH_SHORT).show();
                }
                else if(!validEmail(user_name))
                {
                    Toast.makeText(getActivity(), "Enter the valid email id" ,Toast.LENGTH_SHORT).show();
                }
                else if(!isOnline())
                {
                    Toast.makeText(getActivity(), "No network connection" ,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    new Mytask().execute();
                }

            }
        });



        return v;
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(pattern2);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    String status="",message="";
    private class Mytask extends AsyncTask<Void,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Authenticating!Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path=getActivity().getResources().getString(R.string.url_service)+"prof/Prof_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"prof_login"+"&email="+user_name+"&password="+pass_word,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(status.equals("true"))
            {
                try {
                    PROFESSIONAL_ID=jsn_response.getString("prof_id");
                    FIRSTNAME=jsn_response.getString("firstname");
                    LASTNAME=jsn_response.getString("lastname");
                    EMAIL=jsn_response.getString("email");
                    //PHONE_NUM=jsn_response.getString("phonenumber");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                my_id=getActivity().getSharedPreferences("My_id",getActivity().MODE_PRIVATE);
                SharedPreferences.Editor editor=my_id.edit();
                editor.putString("my_id",PROFESSIONAL_ID).putString("Firstname",FIRSTNAME).putString("Lastname",LASTNAME)
                        .putString("email",EMAIL).putString("phone_num","").putString("PROF","true").commit();
                Log.e("user_id",PROFESSIONAL_ID +" "+FIRSTNAME+" "+LASTNAME+ " "+EMAIL+" ");
            }

            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if(status.equals("true")) {


                Log.e("username", user_name);
                // database.insertData(user_name);
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                //if (getPage.equals("landing_page")) {

            /*   Intent intent = new Intent(getActivity(), Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //getActivity().finish();
                startActivity(intent);*/


                Intent i=new Intent(getActivity(),Drawerlayout_callhistory.class);
                i.putExtra("getPage","default");
                startActivity(i);

              /*  else if(getPage.equals("painting"))
                {
                    Intent intent = new Intent(getActivity(), Painting.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("cleaning"))
                {
                    Intent intent = new Intent(getActivity(), Cleaning.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("chef"))
                {
                    Intent intent = new Intent(getActivity(), Chef.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("movers"))
                {
                    Intent intent = new Intent(getActivity(), Movers.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("ac_service"))
                {
                    Intent intent = new Intent(getActivity(), Acservice.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }*/

            }




            else {

                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();



            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.professionaltxt:
                Intent n=new Intent(getActivity(),Createaccount.class);
                n.putExtra(Createaccount.EXTRA_PROF_USER, true);
                startActivity(n);
                break;
            case R.id.im1:

                username.setText("");

                break;

            case R.id.im2:
                password.setText("");
                break;
            case R.id.forgot_password:
                Intent intent = new Intent(getActivity(), Forgot_password_professionals.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TAG", "Called OnActivityResult");
    }
}
