package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Admin on 12/31/2016.
 */

public class Drawer_message_inbox_reply extends AppCompatActivity {
TextView to;
    LinearLayout backArrow;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_message_inbox_reply);

        to=(TextView)findViewById(R.id.to);
        to.setBackgroundResource(R.color.view);

        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
