package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import fragment_adapter.Drawer_myrequest_adapter;

/**
 * Created by Admin on 1/8/2017.
 */

public class Drawer_myrequest extends AppCompatActivity implements View.OnClickListener{
    Drawer_myrequest_adapter swipe;
    TextView upcoming,history;
    View upcomingview,historyview;
    ViewPager pager;
    LinearLayout backArrow;
    TextView tSignin;
    ImageView language;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_myrequest);

        upcomingview=(View)findViewById(R.id.v3);
        historyview=(View)findViewById(R.id.v4);
        upcoming=(TextView)findViewById(R.id.upcoming);
        history=(TextView)findViewById(R.id.history);
        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        language=(ImageView) findViewById(R.id.language);


        swipe=new Drawer_myrequest_adapter(getSupportFragmentManager());
        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(swipe);


        history.setOnClickListener(this);
        upcoming.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Drawer_myrequest.this,language));

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(pager.getCurrentItem()==0)
                {
                    upcomingview.setVisibility(View.VISIBLE);
                    historyview.setVisibility(View.INVISIBLE);
                }
                else
                {
                    upcomingview.setVisibility(View.INVISIBLE);
                    historyview.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.upcoming:
                pager.setCurrentItem(0);
                break;
            case R.id.history:
                pager.setCurrentItem(1);
                break;
            case R.id.imageArrow:

                onBackPressed();
                break;
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}

