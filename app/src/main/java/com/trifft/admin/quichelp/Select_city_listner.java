package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import androidx.appcompat.app.AlertDialog;

/**
 * Created by PSDeveloper on 1/5/2017.
 */

public class Select_city_listner implements View.OnClickListener {

    Context context;
    ProgressDialog progressDialog;
    EditText select_city;

    String get_country;

    public Select_city_listner(Context activity, EditText select_city, String s) {

        this.context=activity;
        this.select_city=select_city;
        this.get_country=s;

    }

    @Override
    public void onClick(View view) {

        if(!isOnline())
        {
            Toast.makeText(context,"No network connection",Toast.LENGTH_LONG).show();
        }
        else
        {
            new Get_city().execute(get_country);
        }



    }

String status="",message="";
    String get_city_service;

    String[] array;
    ArrayAdapter<String> adapter;
    private class Get_city extends AsyncTask<String,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... voids) {

            String result=null;


            String path=context.getString(R.string.url_service)+"Login_Json/data/?";

            DefaultHttpClient client =new DefaultHttpClient();

            URI uri = null;
            try {
                 uri=new URI("https",path+"service="+"getcountrycity"+"&country="+voids[0],null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            try {
                Log.e("uri",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post= null;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object=new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                status=object.getString("status");
                message=object.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
Log.e("ststus,meessage", status +" "+message);
            if(status.equals("true"))
            {

                JSONObject ob = null;
                try {
                   ob=object.getJSONObject("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    get_city_service=ob.getString("city");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();
            Log.e("city", get_city_service);
            if(status.equals("true"))
            {

                array=get_city_service.split(",");
                //Log.e("length",array.length());
                if(array.length>1)
                {

                    AlertDialog.Builder alert=new AlertDialog.Builder(context);
                    alert.setTitle("Select your city");

                    adapter=new ArrayAdapter<String>(context,android.R.layout.simple_list_item_activated_1, array);
                     alert.setAdapter(adapter, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {

                           select_city.setText(adapter.getItem(i));

                           Toast.makeText(context,"City added Successfully", Toast.LENGTH_SHORT).show();

                       }
                   }).show();

                }
                else
                {

                    select_city.setText(get_city_service);

                    Toast.makeText(context,"City added Successfully", Toast.LENGTH_SHORT).show();

                }



            }
            else
            {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }


    }




    private  boolean isOnline()
    {
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }
}
