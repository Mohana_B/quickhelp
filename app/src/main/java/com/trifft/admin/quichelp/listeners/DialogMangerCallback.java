package com.trifft.admin.quichelp.listeners;

import android.view.View;

/**
 * Created by fatima on 19-10-2019.
 */

public interface DialogMangerCallback {

	void onOkClick(View view);
	/*default*/ void onCancelClick(View view);/*{}*/

}
