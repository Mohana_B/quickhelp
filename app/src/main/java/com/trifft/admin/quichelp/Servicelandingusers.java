package com.trifft.admin.quichelp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.trifft.admin.quichelp.listeners.RecyclerItemClickListener;
import com.trifft.admin.quichelp.makeUpModule.MakeupMainActivity;
import com.trifft.admin.quichelp.model.ServiceModel;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin on 11/10/2016.
 */

public class Servicelandingusers extends Fragment {
    RelativeLayout painting, cleaning, moving, chef, acservice, driver;

    String value;
    SharedPreferences myshar;
    RelativeLayout ac;
    TextView txt;

    TextView header_text;
    private RelativeLayout realestate;
    private RelativeLayout makeup;
    private GridLayout gridLayout;
    RecyclerView ServiceRecyclerView;
    MyListAdapter serviceAdapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.servicelandingusers, container, false);  // TARA HIDE

        ServiceRecyclerView = (RecyclerView) v.findViewById(R.id.serviceRecylcer);
        ac = (RelativeLayout) v.findViewById(R.id.ac);
        //txt=(TextView) v.findViewById(R.id.hd6);
        painting = (RelativeLayout) v.findViewById(R.id.paint);
        cleaning = (RelativeLayout) v.findViewById(R.id.clean);
        moving = (RelativeLayout) v.findViewById(R.id.mov);
        chef = (RelativeLayout) v.findViewById(R.id.chef);
        realestate = (RelativeLayout) v.findViewById(R.id.realestateimg);
        makeup = (RelativeLayout) v.findViewById(R.id.makeup);
        header_text = (TextView) v.findViewById(R.id.header_text);
        driver = (RelativeLayout) v.findViewById(R.id.driver);
        myshar = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = myshar.getString("Value", "");


        ArrayList<ServiceModel> arrayList = new ArrayList<ServiceModel>();
        arrayList.add(new ServiceModel("Ac Service", "Get top class AC service for all brands", R.drawable.ac));
        arrayList.add(new ServiceModel("Makeup Service", "It’s not about just Makeup,it’s about experience", R.drawable.makeup));
        arrayList.add(new ServiceModel("Painting", "A fresh coat for a fresh start", R.drawable.paint));
        arrayList.add(new ServiceModel("Real estate", "Get your property managed by professionals while you sit back and relax", R.drawable.realestate));
        arrayList.add(new ServiceModel("Cleaning", "Let your Home shine, get the best cleaning services", R.drawable.clean));
        arrayList.add(new ServiceModel("Movers", "Move & Relax, choose our Professional Movers & Packers", R.drawable.movers));
        arrayList.add(new ServiceModel("Driver", "Never get late with our professional and experienced drivers", R.drawable.driver));
        arrayList.add(new ServiceModel("Chef", "Our Chefs promises to give a tease to your taste buds", R.drawable.chef));


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        ServiceRecyclerView.setLayoutManager(linearLayoutManager);
        serviceAdapter = new MyListAdapter(Servicelandingusers.this, arrayList);
        ServiceRecyclerView.setAdapter(serviceAdapter); // set the Adapter to RecyclerView
        ServiceRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), ServiceRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                       // Toast.makeText(getApplicationContext(), arrayList.get(position).getName(), Toast.LENGTH_SHORT).show();

                        switch (position) {
                            case 0:
                                Intent intent = new Intent(getActivity(), Acservice.class);
                                startActivity(intent);

                                break;
                            case 1:
                                Intent Mintent = new Intent(getActivity(), MakeupMainActivity.class);
                                startActivity(Mintent);

                                break;
                            case 2:
                                Intent in = new Intent(getActivity(), Painting.class);
                                startActivity(in);

                                break;
                            case 3:
                                Intent Rinten = new Intent(getActivity(), Realestate.class);
                                startActivity(Rinten);

                                break;
                            case 4:
                                Intent i = new Intent(getActivity(), Cleaning.class);
                                startActivity(i);
                                break;
                            case 5:
                                Intent inten = new Intent(getActivity(), Movers.class);
                                startActivity(inten);

                                break;
                            case 6:
                                Intent intents = new Intent(getActivity(), Driver.class);
                                startActivity(intents);

                                break;
                            case 7:
                                Intent intnt = new Intent(getActivity(), Chef.class);
                                startActivity(intnt);
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
       /* if(value.equals("CHENNAI"))
        {
            header_text.setTextColor(getActivity().getResources().getColor(R.color.app_heading2));

        }
        else
        {
            // ac.setVisibility(View.INVISIBLE);
            //txt.setVisibility(View.INVISIBLE);
            //acService.setVisibility(View.INVISIBLE);
        }*/

        painting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), Painting.class);
                startActivity(in);
            }
        });
        cleaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                   /* String res="status=success:orderId=a089f02724ed4a8db6c069f6d30b3245:txnId=None:paymentId=MOJO7918005A76494611:token=qyFwLidQ0aBNNWlsmwHx1gHFhlt6A1";

                    //as per instamojo documentaion
                    String orderId="",txnId="",paymentId="",token="";

                res=res.replace(":",",");

                    //getting the repose from intamojo
                    JSONObject intamojo_response = null;
                    try {
                        intamojo_response=new JSONObject("{"+res+"}");

                        orderId=intamojo_response.getString("orderId");
                        txnId=intamojo_response.getString("txnId");

                        paymentId=intamojo_response.getString("paymentId");
                        token=intamojo_response.getString("token");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                Log.e("makeText",orderId+" "+txnId+" "+" "+paymentId+" "+token);
                Toast.makeText(getActivity(),orderId+" "+txnId+" "+paymentId+" "+token,Toast.LENGTH_SHORT).show();*/


                Intent i = new Intent(getActivity(), Cleaning.class);
                startActivity(i);
            }
        });
        moving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent inten = new Intent(getActivity(), Movers.class);
                startActivity(inten);
            }
        });
        chef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intnt = new Intent(getActivity(), Chef.class);
                startActivity(intnt);
            }
        });
        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Acservice.class);
                startActivity(intent);

            }
        });
        driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Driver.class);
                startActivity(intent);
            }
        });

        realestate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent inten = new Intent(getActivity(), Realestate.class);
                startActivity(inten);
            }
        });

        makeup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MakeupMainActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
        ArrayList<ServiceModel> Namesarraylist;
        Servicelandingusers context;

        public MyListAdapter(Servicelandingusers servicelandingusers, ArrayList<ServiceModel> arrayList) {
            this.context = servicelandingusers;
            this.Namesarraylist = arrayList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.service_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.name.setText(Namesarraylist.get(position).getName());
            holder.Subname.setText(Namesarraylist.get(position).getSubName());
            holder.Image.setImageResource(Namesarraylist.get(position).getImage());
            if ( position % 2 == 0 ){
                holder.Layout.setBackgroundResource(R.drawable.home_gradient_maths);
            }else {
                holder.Layout.setBackgroundResource(R.drawable.home_gradient_pink);

            }


        }


        @Override
        public int getItemCount() {
            return Namesarraylist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView name, Subname;// init the item view's
            ImageView Image;
            RelativeLayout Layout;

            public ViewHolder(View itemView) {
                super(itemView);
                Layout = (RelativeLayout) itemView.findViewById(R.id.relative);
                Image = (ImageView) itemView.findViewById(R.id.serimage);
                name = (TextView) itemView.findViewById(R.id.hdtxt);
                Subname = (TextView) itemView.findViewById(R.id.subtxt);
            }
        }
    }

/*
    public class ServiceAdapter extends RecyclerView.Adapter {
        ArrayList<ServiceModel> Namesarraylist;
        Servicelandingusers context;


        public ServiceAdapter(Servicelandingusers servicelandingusers, ArrayList<ServiceModel> arrayList) {
            this.context = servicelandingusers;
            this.Namesarraylist = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            // implement setOnClickListener event on item view.
            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // display a toast with person name on item click
                    Toast.makeText(getApplicationContext(), Namesarraylist.get(position).getName(), Toast.LENGTH_SHORT).show();
                }
            });


        }


        @Override
        public int getItemCount() {
            return Namesarraylist.size();
        }
        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name,Subname;// init the item view's
            ImageView Image;
            public MyViewHolder(View itemView) {
                super(itemView);
                // get the reference of item view's
                Image = (ImageView) itemView.findViewById(R.id.serimage);
                name = (TextView) itemView.findViewById(R.id.hdtxt);
                Subname = (TextView) itemView.findViewById(R.id.subtxt);
            }
        }
    }
*/
}
