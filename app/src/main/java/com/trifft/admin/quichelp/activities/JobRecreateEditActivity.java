package com.trifft.admin.quichelp.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.trifft.admin.quichelp.Date_picker;
import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.TimePicker;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

public class JobRecreateEditActivity extends BaseActivity implements View.OnClickListener, CommonInterface {
    private JobRecreateEditActivity thisActivity;

    Toolbar mToolBar;
    TextView headerTitle;
    ImageView imgBackNav;

    private TextView jobIdTv, jobStatusTv;
    private EditText dateEt, timeEt;
    private Button recreatEditBtn;

    public static final String EXTRA_JOB_ID = "extraJobId";
    public static final String EXTRA_JOB_STATUS = "extraJobStatus";
    public static final String EXTRA_RECREATE_OR_EDIT = "recreateOrEditJob";
    public static final int JOB_EDIT_RECREATE_REQUEST = 1001;

    String USER_ID = Serviceselectionpage.USER_ID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_recreate_edit);
        thisActivity = this;

        mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        headerTitle = findViewById(R.id.hedder_text);
        imgBackNav = findViewById(R.id.imgBacknav);
        headerTitle.setText("Recreate/Edit Job");
        imgBackNav.setOnClickListener(this);

        initializeViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(EXTRA_JOB_ID))
                jobIdTv.setText(String.valueOf(bundle.getString(EXTRA_JOB_ID)));
            if (bundle.containsKey(EXTRA_JOB_STATUS))
                jobStatusTv.setText(bundle.getString(EXTRA_JOB_STATUS));
            if (bundle.containsKey(EXTRA_RECREATE_OR_EDIT)) {
                headerTitle.setText(bundle.getString(EXTRA_RECREATE_OR_EDIT));
                recreatEditBtn.setText(bundle.getString(EXTRA_RECREATE_OR_EDIT));
            }
        }
    }

    private void initializeViews() {
        jobIdTv = findViewById(R.id.jre_jobId_tv);
        jobStatusTv = findViewById(R.id.jre_jobStatus_tv);
        dateEt = findViewById(R.id.jre_prefDate_et);
        timeEt = findViewById(R.id.jre_prefTime_et);
        recreatEditBtn = findViewById(R.id.jre_button);

        dateEt.setOnClickListener(new Date_picker(thisActivity, dateEt));
        timeEt.setOnClickListener(new TimePicker(thisActivity, timeEt));
        recreatEditBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBacknav:
                onBackPressed();
                break;
            case R.id.jre_button:
                if (checkValidation()) {
                    String service = "";
                    if (Utils.isValidStr(recreatEditBtn.getText().toString().trim())) {
                        if (getString(R.string.edit).equalsIgnoreCase(recreatEditBtn.getText().toString().trim()))
                            service = "booking_update";
                        else if (getString(R.string.recreate).equalsIgnoreCase(recreatEditBtn.getText().toString().trim()))
                            service = "recreate";
                        APIRequestHandler.getInstance().recreateOrEditJob(service, USER_ID, jobIdTv.getText().toString().trim(), dateEt.getText().toString().trim(), timeEt.getText().toString().trim(), thisActivity, this);

                    }
                }
                break;
        }
    }

    private boolean checkValidation() {
        if (!Utils.isValidStr(dateEt.getText().toString().trim())) {
            DialogManager.showToast(thisActivity, "Please select preferred Date");
            return false;
        } else if (!Utils.isValidStr(timeEt.getText().toString().trim())) {
            DialogManager.showToast(thisActivity, "Please Select preferred time");
            return false;
        }
        return true;
    }

    @Override
    public void onRequestSuccess(Object responseObj) {
        super.onRequestSuccess(responseObj);

        if (responseObj instanceof CommonResponse) {
            CommonResponse response = (CommonResponse) responseObj;
            if (response.isStatus()) {
                DialogManager.showToast(thisActivity, response.getMessage());
                DialogManager.showSingleBtnPopup(thisActivity, new DialogMangerCallback() {
                    @Override
                    public void onOkClick(View view) {
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onCancelClick(View view) {

                    }
                }, getString(R.string.congratulations), response.getMessage(), getString(R.string.ok));
            }
        }
    }
}
