package com.trifft.admin.quichelp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.trifft.admin.quichelp.activities.JobRecreateEditActivity;

/**
 * Created by Admin on 12/22/2016.
 */

public class Drawer_callhistory_history_list_details extends AppCompatActivity implements View.OnClickListener {
    ImageView language;
    Button recreate, rate;
    LinearLayout imageArrow;

    String JOD_ID, JOB_STATUS, DATE, TIME, PRICE, PAYMENT_STATUS;
    TextView jod_id, job_status, date, time, price, payment_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_callhistory_history_list_details);

        imageArrow = (LinearLayout) findViewById(R.id.imageArrow);
        language = (ImageView) findViewById(R.id.language);
        recreate = (Button) findViewById(R.id.recreate);
        jod_id = (TextView) findViewById(R.id.jod_id);
        job_status = (TextView) findViewById(R.id.job_status);
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.time);
        price = (TextView) findViewById(R.id.price);
        payment_status = (TextView) findViewById(R.id.payment_status);
        rate = (Button) findViewById(R.id.rate);


        JOD_ID = getIntent().getExtras().getString("jod_id");
        JOB_STATUS = getIntent().getExtras().getString("job_status");
        DATE = getIntent().getExtras().getString("date");
        TIME = getIntent().getExtras().getString("time");
        PRICE = getIntent().getExtras().getString("price");
        PAYMENT_STATUS = getIntent().getExtras().getString("payment_status");

        // TARA- depends on job status hide and show buttons
        // when job is closed, no recreate only rate
        // when job is expired, no rate only recreate
        if (JOB_STATUS.equals("closed")) {
            rate.setVisibility(View.VISIBLE);
        }

        rate.setOnClickListener(this);
        imageArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Drawer_callhistory_history_list_details.this, language));
        recreate.setOnClickListener(this);

        jod_id.setText(JOD_ID);
        job_status.setText(JOB_STATUS);
        price.setText(PRICE);
        date.setText(DATE);
        time.setText(TIME);
        payment_status.setText(PAYMENT_STATUS);

        Log.e("job_id", JOD_ID + " " + JOB_STATUS + " " + PRICE + " " + DATE + " " + TIME + " " + PAYMENT_STATUS);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageArrow:

                back_function();
                //onBackPressed();
              /*  Intent in=new Intent(getApplicationContext(),Drawerlayout_callhistory.class);
               in.putExtra("getPage","history");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(in);*/
                break;
            case R.id.rate:
                Intent n = new Intent(this, Rating_Jobhistory.class);
                n.putExtra("job_id", JOD_ID);
                n.putExtra("job_status", JOB_STATUS);
                startActivity(n);
                break;
            case R.id.recreate:
                Intent gotoEditJob = new Intent(this, JobRecreateEditActivity.class);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_ID, JOD_ID);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_STATUS, JOB_STATUS);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_RECREATE_OR_EDIT, getString(R.string.recreate));
                startActivityForResult(gotoEditJob, JobRecreateEditActivity.JOB_EDIT_RECREATE_REQUEST);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        back_function();
    }

    private void back_function() {
        Intent in = new Intent(getApplicationContext(), Drawerlayout_callhistory.class);
        in.putExtra("getPage", "history");
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(in);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == JobRecreateEditActivity.JOB_EDIT_RECREATE_REQUEST){
            if (resultCode == RESULT_OK){
                onBackPressed();
            }
        }
    }
}
