package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.trifft.admin.quichelp.activities.FBLoginActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


/**
 * Created by PSDeveloper on 11/10/2016.
 */

public class Sign_user extends Fragment implements View.OnClickListener{

private static final int REQ_GOTO_FB_LOGN_SCREEN = 4001;
    ProgressDialog progressDialog;
    private CallbackManager manager;

    String f_name,l_name,f_id,f_gender,f_email;
    private FacebookCallback<LoginResult> fResult;

    {
        fResult = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken token = loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        Log.i("LoginActivity2", object.toString());

                        try {
                            f_name=object.getString("first_name");
                            l_name=object.getString("last_name");
                            f_id=object.getString("id");
                            f_email=object.getString("email");
                            f_gender=object.getString("gender");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                      /*  Intent n=new Intent(getActivity(), Facebook_sample.class);
                        n.putExtra("email",f_email);
                        n.putExtra("name",f_name+l_name);
                        n.putExtra("id",f_id);
                        n.putExtra("gender",f_gender);
                        startActivity(n);*/



                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
                //new fbcretelogin().execute();

            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        manager=CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
    }


    TextView create_account,forgot_password;
    SharedPreferences check;
    String value;
    Button login, fbCustomBtn;
    AutoCompleteTextView username;
    EditText password;
    String user_name,pass_word;
    ImageView user,pssd;
    String status,message;
    String USER_ID,FIRSTNAME,LASTNAME,EMAIL,PHONE_NUM;
    SharedPreferences my_id;


    String getPage= Sign_activity.getPage;

    Database database;
    ArrayAdapter<String> adapter;
    ArrayList<String> arraylist_database;
    String getData_email;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.user_signin,container,false);
        username=(AutoCompleteTextView) v.findViewById(R.id.username);
        password=(EditText) v.findViewById(R.id.password);
        login=(Button)v.findViewById(R.id.button);
        create_account=(TextView)v.findViewById(R.id.create_account_txt);
        user=(ImageView) v.findViewById(R.id.im1);
        pssd=(ImageView) v.findViewById(R.id.im2);
        forgot_password=(TextView) v.findViewById(R.id.forgot_password);
        fbCustomBtn = v.findViewById(R.id.fb_custom_btn);


        forgot_password.setOnClickListener(this);
        create_account.setOnClickListener(this);

        check=getActivity().getSharedPreferences("Default_Language",getActivity().MODE_PRIVATE);
        value=check.getString("Value","");

      /*  if(value.equals("CHENNAI"))
        {
            login.setBackgroundResource(R.drawable.ed_button2);
            create_account.setTextColor(getActivity().getResources().getColor(R.color.app_heading2));

        }*/

       /* database=new Database(getActivity());
        database.database();*/

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_name=username.getText().toString();
                pass_word=password.getText().toString();

                if(user_name.length()==0||user_name.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the username" ,Toast.LENGTH_SHORT).show();
                }
                else if(pass_word.length()==0||pass_word.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the password" ,Toast.LENGTH_SHORT).show();
                }
                else if(!isOnline())
                {
                    Toast.makeText(getActivity(), "No network connection" ,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    new Mytask().execute();
                }

            }
        });


        //TARA
        LoginButton login=(LoginButton)v.findViewById(R.id.login_button);
        login.setReadPermissions("email");
        login.setFragment(this);
        login.registerCallback(manager,fResult);
        fbCustomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*login.performClick();*/
                Intent gotoFbLogin = new Intent(getActivity(), FBLoginActivity.class);
//                startActivityForResult(gotoFbLogin, REQ_GOTO_FB_LOGN_SCREEN );
                startActivity(gotoFbLogin);
            }
        });

        my_id=getActivity().getSharedPreferences("My_id",getActivity().MODE_PRIVATE);
        getData_email=my_id.getString("email","");
        username.setText(getData_email);


        return v;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*LoginButton login=(LoginButton)view.findViewById(R.id.login_button);

        login.setReadPermissions("email");
        login.setFragment(this);
        login.registerCallback(manager,fResult);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == REQ_GOTO_FB_LOGN_SCREEN){

            if (resultCode == RESULT_OK){
                // goto ServicesPage
                Intent intent = new Intent(getActivity(), Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                // show login again // load page again / refresh page
            }

        } else {*/
            manager.onActivityResult(requestCode, resultCode, data);
//        }
    }

    public void logout()
    {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken!=null){
            LoginManager.getInstance().logOut();}
    }

    private class Mytask extends AsyncTask<Void,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Authenticating!Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path=getActivity().getResources().getString(R.string.url_service)+"Login_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"login"+"&email="+user_name+"&password="+pass_word,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(status.equals("true"))
            {
                try {
                    USER_ID=jsn_response.getString("userid");
                    FIRSTNAME=jsn_response.getString("firstname");
                    LASTNAME=jsn_response.getString("lastname");
                    EMAIL=jsn_response.getString("email");
                    PHONE_NUM=jsn_response.getString("phonenumber");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                my_id=getActivity().getSharedPreferences("My_id",getActivity().MODE_PRIVATE);
                SharedPreferences.Editor editor=my_id.edit();
                editor.putString("my_id",USER_ID).putString("Firstname",FIRSTNAME).putString("Lastname",LASTNAME)
                        .putString("email",EMAIL).putString("phone_num",PHONE_NUM).putString("USER","true").commit();
                Log.e("user_id",USER_ID +" "+FIRSTNAME+" "+LASTNAME+ " "+EMAIL+" "+PHONE_NUM);
            }

            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if(status.equals("true")) {


                Log.e("username", user_name);
                // database.insertData(user_name);
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                //if (getPage.equals("landing_page")) {

                Intent intent = new Intent(getActivity(), Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //getActivity().finish();
                startActivity(intent);
              /*  else if(getPage.equals("painting"))
                {
                    Intent intent = new Intent(getActivity(), Painting.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("cleaning"))
                {
                    Intent intent = new Intent(getActivity(), Cleaning.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("chef"))
                {
                    Intent intent = new Intent(getActivity(), Chef.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("movers"))
                {
                    Intent intent = new Intent(getActivity(), Movers.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else if(getPage.equals("ac_service"))
                {
                    Intent intent = new Intent(getActivity(), Acservice.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }*/

            }

            else {

                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();



            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.create_account_txt:
                Intent n=new Intent(getActivity(), Createaccount.class);
                startActivity(n);
                break;
            case R.id.im1:

                username.setText("");

                break;

            case R.id.im2:
                password.setText("");
                break;
            case R.id.forgot_password:
                Intent intent = new Intent(getActivity(), Forgot_password_user.class);
                startActivity(intent);
                break;
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }



    private class fbcretelogin extends AsyncTask<Void,Void,Void>
    {

       String phnnum ="123456789";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Creating account...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;
           // https://www.quichelp.com/login/fblogin?userData={%22id%22:%22108649627216697%22,%22first_name%22:%22Quichelp%22,%22last_name%22:%22Serv%22,%22email%22:%22social.quichelp@gmail.com%22
            //String path="//www.servfy.com/json/Register_Json/data?";
            //http://www.quichelp.in/json/Register_Json/data?service=registeruser&email=abc@yahoo.com&password=1111&firstname=shrikar&lastname=chari&phonenumber=1234567890
            String path=getActivity().getResources().getString(R.string.url_service)+"Register_Json/data?";
            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {

                uri=new URI("https",path+"service="+"registeruser"+"&email="+f_email+"&password="+pass_word+"&firstname="+f_name
                        +"&lastname="+l_name+"&phonenumber="+phnnum,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_responses = null;
            try {
                jsn_responses=new JSONObject(result);
                status=jsn_responses.getString("status");
                message=jsn_responses.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true"))
            {
                if(status.equals("true")) {

                    if(user_name != null)
                    {

                        Log.e("username", user_name);
                        // database.insertData(user_name);

                        //if (getPage.equals("landing_page")) {

                        /*Intent intent = new Intent(getActivity(), Serviceselectionpage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        //getActivity().finish();
                        startActivity(intent);*/
                    }

                }
            }
            else
            {
                Toast.makeText(getActivity(), message ,Toast.LENGTH_SHORT).show();
                //firstname.setText("");lastname.setText("");password.setText("");confirmPssd.setText("");email_id.setText("");
                //phonenumber.setText("");
                //firstname.requestFocus();
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                new Mytask().execute();
            }
        }
    }

}
