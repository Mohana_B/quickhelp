package com.trifft.admin.quichelp.utils;

import android.util.Log;

import com.trifft.admin.quichelp.BuildConfig;

/**
 * Created by fatima on 19-10-2019.
 */

public class QuickHelpLog {
    private static final boolean displayLog = BuildConfig.print_log;

    public static void i(String tag, String message){
        if(displayLog)
            Log.i(tag, message);
    }

    public static void d(String tag, String message){
        if(displayLog)
            Log.d(tag, message);
    }
    public static void e(String tag, String message){
        if(displayLog)
            Log.e(tag, message);
    }
    public static void e(String tag, String message, Exception e){
        if(displayLog)
            Log.e(tag, message, e);
    }
    public static void w(String tag, String message){
        if(displayLog)
            Log.w(tag, message);
    }
    public static void w(String tag, String message, Exception e){
        if(displayLog)
            Log.w(tag, message, e);
    }
}
