package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/11/2016.
 */

public class Cleaningbooknow extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CommonInterface {

    SharedPreferences check;
    String value;
    Button get_a_price;
    ProgressDialog progressDialog;
    String first_name, last_name, email, living_room = "", bed_room = "", kitchen_room = "", toilet_room = "", cleaning_hours = "", typeofService;
    String date_data, time_data, address_1, address_2,/* select_Country,*/ selectedCity, zip_code, mobile_num, special_comments;

    EditText firstname, lastname, email_id, livingroom, bedroom, kitchenroom, toiletroom, cleaninghours;
    EditText date, time, address1, address2, /*selectCountry,*/ zipcode, mobileNumber, specialComments;
    private RadioButton standardCleaningRb, deepCleaningRb;

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    SharedPreferences my_id;
    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    ImageView combo, combo1, combo2, combo3;
    ImageView close, close1, close2, close3;
    String BED_ROOM;

    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.cleaningbooknow, container, false);

        combo = (ImageView) v.findViewById(R.id.combo);
        close = (ImageView) v.findViewById(R.id.close);
        combo1 = (ImageView) v.findViewById(R.id.combo1);
        close1 = (ImageView) v.findViewById(R.id.close1);
        combo2 = (ImageView) v.findViewById(R.id.combo2);
        close2 = (ImageView) v.findViewById(R.id.close2);
        combo3 = (ImageView) v.findViewById(R.id.combo3);
        close3 = (ImageView) v.findViewById(R.id.close3);

        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        email_id = (EditText) v.findViewById(R.id.email);
        livingroom = (EditText) v.findViewById(R.id.livingrooms);
        bedroom = (EditText) v.findViewById(R.id.bedrooms);
        kitchenroom = (EditText) v.findViewById(R.id.kitchenrooms);
        toiletroom = (EditText) v.findViewById(R.id.toiletrooms);
        cleaninghours = (EditText) v.findViewById(R.id.cleaninghours);
        date = (EditText) v.findViewById(R.id.date);
        time = (EditText) v.findViewById(R.id.time);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        spnCountry = v.findViewById(R.id.spn_country);
//        selectCountry = (EditText) v.findViewById(R.id.selectcountry);
        // selectCity=(EditText) v.findViewById(R.id.selectcity);
        zipcode = (EditText) v.findViewById(R.id.zipcode);
        mobileNumber = (EditText) v.findViewById(R.id.mobilenumber);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);
        get_a_price = (Button) v.findViewById(R.id.getaprice);

        standardCleaningRb = (RadioButton) v.findViewById(R.id.cl_standard_rb);
        deepCleaningRb = (RadioButton) v.findViewById(R.id.cl_deep_rb);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        my_id = getActivity().getSharedPreferences("My_id", getActivity().MODE_PRIVATE);
        USER_ID = my_id.getString("my_id", null);

        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            email_id.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobileNumber.setText(Serviceselectionpage.PHONENUM);

        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);

    /*    if(value.equals("CHENNAI"))
        {
            //get_a_price.setBackgroundResource(R.drawable.ed_button2);
            selectCountry.setText("CHENNAI");
        }
        else{selectCountry.setText("MUMBAI");}*/

        close.setOnClickListener(this);
        close1.setOnClickListener(this);
        close2.setOnClickListener(this);
        close3.setOnClickListener(this);
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));

        standardCleaningRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    deepCleaningRb.setChecked(false);
                }
            }
        });
        deepCleaningRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    standardCleaningRb.setChecked(false);
                }
            }
        });

//        selectCountry.setOnClickListener(new Select_country(getActivity(), selectCountry));
        cleaninghours.setOnClickListener(new Popup_cleaning_hours(getActivity(), cleaninghours));
        livingroom.setOnClickListener(new Popup_fiveroom(getActivity(), livingroom, "Living room"));
        bedroom.setOnClickListener(new Popup_Ten_room(getActivity(), bedroom, "Bed room"));
        kitchenroom.setOnClickListener(new Popup_fiveroom(getActivity(), kitchenroom, "Kitchen room"));
        toiletroom.setOnClickListener(new Popup_fiveroom(getActivity(), toiletroom, "Toilet room"));

        livingroom_textchange_listner();
        bedroom_textchange_listner();
        kitchenroom_textchange_listner();
        toiletroom_textchange_listner();


        get_a_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = email_id.getText().toString();
                living_room = livingroom.getText().toString();
                bed_room = bedroom.getText().toString();
                kitchen_room = kitchenroom.getText().toString();
                toilet_room = toiletroom.getText().toString();
                cleaning_hours = cleaninghours.getText().toString();
                date_data = date.getText().toString();
                time_data = time.getText().toString();
                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();
//                select_Country = selectCountry.getText().toString();

                selectedCity = spnCountry.getSelectedItem().toString();
                zip_code = zipcode.getText().toString();
                mobile_num = mobileNumber.getText().toString();
                special_comments = specialComments.getText().toString();


                if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else if (first_name.length() == 0 || first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.length() == 0 || last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (email.length() == 0 || email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (!standardCleaningRb.isChecked() && !deepCleaningRb.isChecked()) {
                    Toast.makeText(getActivity(), "Select the service type", Toast.LENGTH_SHORT).show();
                } else if (living_room.equals("") && bed_room.equals("") && kitchen_room.equals("") && toilet_room.equals("")) {
                    Toast.makeText(getActivity(), "Select any room", Toast.LENGTH_SHORT).show();
                }
               /* else if(living_room.length()==0 || living_room.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the Living room" ,Toast.LENGTH_SHORT).show();
                }
                else if(bed_room.length()==0 || bed_room.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the Bed room" ,Toast.LENGTH_SHORT).show();
                }
                else if(kitchen_room.length()==0 || kitchen_room.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the Kitchen room" ,Toast.LENGTH_SHORT).show();
                }
                else if(toilet_room.length()==0 || toilet_room.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the Toilet room" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(cleaning_hours.length()==0 || cleaning_hours.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the Cleaning hours" ,Toast.LENGTH_SHORT).show();
                }*/
                else if (date_data.length() == 0 || date_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                } else if (time_data.length() == 0 || time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                }
               /* else if(address_1.length()==0 || address_1.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }
                else if(address_2.length()==0 || address_2.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }*/
               /* else if(select_city.length()==0 || select_city.equals(""))
                {
                    Toast.makeText(getActivity(), "Select the City" ,Toast.LENGTH_SHORT).show();
                }*/else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select country");
                }
                else if (zip_code.length() == 0 || zip_code.equals("")) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                }else if (zip_code.length() < 6 ) {
                    Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                } else if (mobile_num.length() == 0 || mobile_num.equals("")) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                }
                /*else if(special_comments.length()==0 || special_comments.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the special comments" ,Toast.LENGTH_SHORT).show();
                }*/

                else {
                    //Toast.makeText(getActivity(), "Database error..contact admin" ,Toast.LENGTH_SHORT).show();
                    if (standardCleaningRb.isChecked()) {
                        typeofService = "standard";
                    }

                    if (deepCleaningRb.isChecked()) {
                        typeofService = "deep";
                    }
                    new Mytask().execute();
                }
            }
        });

        return v;
    }


    private void livingroom_textchange_listner() {

        livingroom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                combo.setVisibility(View.INVISIBLE);
                close.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void bedroom_textchange_listner() {
        bedroom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                combo1.setVisibility(View.INVISIBLE);
                close1.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void kitchenroom_textchange_listner() {

        kitchenroom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                combo2.setVisibility(View.INVISIBLE);
                close2.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void toiletroom_textchange_listner() {

        toiletroom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                combo3.setVisibility(View.INVISIBLE);
                close3.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    String status = "", message = "", jod_id = "", price = "";

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.close:

                livingroom.setText("");
                combo.setVisibility(View.VISIBLE);
                close.setVisibility(View.INVISIBLE);

                break;
            case R.id.close1:

                bedroom.setText("");
                combo1.setVisibility(View.VISIBLE);
                close1.setVisibility(View.INVISIBLE);

                break;
            case R.id.close2:

                kitchenroom.setText("");
                combo2.setVisibility(View.VISIBLE);
                close2.setVisibility(View.INVISIBLE);

                break;
            case R.id.close3:

                toiletroom.setText("");
                combo3.setVisibility(View.VISIBLE);
                close3.setVisibility(View.INVISIBLE);

                break;
        }
    }

    private class Mytask extends AsyncTask<Void, Void, Void> {

        boolean status1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Clean_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;

            /*https://quichelp.com/json/Clean_Json/data/?service=insert_clean_job_forindia&
            userid=191&
            firstname=Shrikar&
            lastname=kodamgulawar&
            email=k.shrikar@gmail.com&
            phone=1234567899&
            service_type=standard&
            no_living_rooms=1&
            no_bed_rooms=0&
            no_kitchen=0&
            no_toilet=0&
            from_city=Chennai&
            from_zip=1212111&
            pref_date=2020-02-29&
            pref_time=3:00PM&
            frm_address1=Hyderabad&
            sp_comments=wewewe&
            frm_address2=dsdsd*/
            // insert_clean_job


            String email_st = "";
            if (EMAIL_ID != null && !EMAIL_ID.equals(""))
                email_st = EMAIL_ID;
            else if (email != null && !email.equals(""))
                email_st = email;

            try {
                uri = new URI("https", path + "service=" + "insert_clean_job_forindia" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&email=" + email_st +
                        "&from_city=" + selectedCity +
                        "&from_zip=" + zip_code +
                        "&phone=" + mobile_num +
                        "&no_living_rooms=" + living_room.trim() +
                        "&no_bed_rooms=" + bed_room.trim() +
                        "&pref_date=" + date_data +
                        "&pref_time=" + time_data +
                        "&frm_address1=" + address_1 +
                        "&sp_comments=" + special_comments +
                        "&lastname=" + last_name +
                        "&frm_address2=" + address_2 +
                        "&no_kitchen=" + kitchen_room.trim() +
                        "&no_toilet=" + toilet_room.trim() +
                        "&service_type=" + typeofService
                        , null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status1 = object.getBoolean("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status);
            Log.e("message", message);

            if (status1 == true) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status1 == true) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }
}
