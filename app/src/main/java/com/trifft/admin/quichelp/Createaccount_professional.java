package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trifft.admin.quichelp.activities.JobRecreateEditActivity;
import com.trifft.admin.quichelp.model.IdNameModel;
import com.trifft.admin.quichelp.utils.QuickHelpLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/12/2016.
 */

public class Createaccount_professional extends Fragment {

    String first_name,last_name,email,password_st="",confirm_password="";
    String address_1,address_2,select_Country,state_st,select_city_st,zip_code,mobile_num,special_comments,special_service/*, selectedService*/;

    String get_skills="";
    String willint_to_yes_no="",type_of_job="",payment_mtd="";

    EditText firstname,lastname,email_id,password,con_password;
    EditText address1,address2,selectCountry,state,selectcity,zipcode,mobileNumber,specialComments,specialservice/*, selectServiceEt*/;
    ProgressDialog progressDialog;


    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    SharedPreferences my_id;
    String USER_ID;
    ImageView combo,combo1,combo2,combo3;
    ImageView close,close1,close2,close3;
    String BED_ROOM;

    SharedPreferences check;
    String value;
    Button create_professional;

    RadioButton yes,no,part_time,full_time,cash,paypal,bank_trans;
    CheckBox cleaning,painting,movers,chef,driving,aircon,Makeup;


//    private ArrayList<IdNameModel> servicesList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.createaccount_professional,container,false);

        yes=(RadioButton) v.findViewById(R.id.yes);
        no=(RadioButton) v.findViewById(R.id.no);
        part_time=(RadioButton) v.findViewById(R.id.part_tm);
        full_time=(RadioButton) v.findViewById(R.id.full_tm);
        cash=(RadioButton) v.findViewById(R.id.cash);
        paypal=(RadioButton) v.findViewById(R.id.paypal);
        bank_trans=(RadioButton) v.findViewById(R.id.bank);

        cleaning= v.findViewById(R.id.clean);
        painting=v.findViewById(R.id.paint);
        movers=v.findViewById(R.id.movers);
        chef=v.findViewById(R.id.chef);
        driving=v.findViewById(R.id.drive);
        aircon=v.findViewById(R.id.ac);
        Makeup=v.findViewById(R.id.beauty);


        create_professional=(Button)v.findViewById(R.id.register);

        firstname=(EditText) v.findViewById(R.id.firstname);
        lastname=(EditText) v.findViewById(R.id.lastname);
        email_id=(EditText) v.findViewById(R.id.email);
        address1=(EditText) v.findViewById(R.id.address1);
        address2=(EditText) v.findViewById(R.id.address2);
        state=(EditText) v.findViewById(R.id.state);
        selectCountry=(EditText) v.findViewById(R.id.selectcountry);
        selectcity=(EditText) v.findViewById(R.id.selectcity);
        zipcode=(EditText) v.findViewById(R.id.zipcode);
        mobileNumber=(EditText) v.findViewById(R.id.mobilenumber);
        specialComments=(EditText) v.findViewById(R.id.specialcomments);
//        selectServiceEt = v.findViewById(R.id.selectServices);

//        new ServicesMasterDataTask().execute();

        password=(EditText) v.findViewById(R.id.password);
        con_password=(EditText) v.findViewById(R.id.confirm_password);

        specialservice=(EditText) v.findViewById(R.id.specialservice);


        check=getActivity().getSharedPreferences("Default_Language",getActivity().MODE_PRIVATE);
        value=check.getString("Value","");

        my_id=getActivity().getSharedPreferences("My_id",getActivity().MODE_PRIVATE);
        USER_ID=my_id.getString("my_id",null);

        selectcity.setOnClickListener(new Select_country(getActivity(),selectcity));
//        selectServiceEt.setOnClickListener(new DropDownSelectionIdNameModel(getActivity(), selectServiceEt, servicesList));

        /*if(value.equals("CHENNAI"))
        {
            create_professional.setBackgroundResource(R.drawable.ed_button2);
        }*/


        create_professional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = email_id.getText().toString();
                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();

                select_city_st = selectcity.getText().toString();
                state_st = state.getText().toString();
                select_Country = selectCountry.getText().toString();

                zip_code = zipcode.getText().toString();
                mobile_num = mobileNumber.getText().toString();

                password_st = password.getText().toString();
                confirm_password = con_password.getText().toString();

                special_comments = specialComments.getText().toString();
                special_service = specialservice.getText().toString();

//                selectedService = selectServiceEt.getText().toString().trim();


                if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else if (first_name.length() == 0 || first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.length() == 0 || last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                }
                else if (password_st.length() == 0 || password_st.equals("")) {
                    Toast.makeText(getActivity(), "Enter the password", Toast.LENGTH_SHORT).show();
                } else if (confirm_password.length() == 0 || confirm_password.equals("")) {
                    Toast.makeText(getActivity(), "Enter the confirm password", Toast.LENGTH_SHORT).show();
                }
                else if(!password_st.equals(confirm_password))
                {
                    Toast.makeText(getActivity(), "Password mismatch" ,Toast.LENGTH_SHORT).show();
                }
                else if (email.length() == 0 || email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                }
                /*
                else if (zip_code.length() == 0 || zip_code.equals("")) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                }*/ else if (mobile_num.length() == 0 || mobile_num.equals("")) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobile_num.length() < 10 ) {
                    Toast.makeText(getActivity(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                } /*else if (!Utils.isValidStr(selectedService)){
                    Toast.makeText(getActivity(), "Select Type of services provided", Toast.LENGTH_SHORT).show();
                }*/
               /* else if (!yes.isChecked()&&!no.isChecked()) {
                    Toast.makeText(getActivity(), "Select preferences", Toast.LENGTH_SHORT).show();
                }
                else if (!part_time.isChecked()&&!full_time.isChecked()) {
                    Toast.makeText(getActivity(), "Select type of job", Toast.LENGTH_SHORT).show();
                }
                else if (!cash.isChecked()&&!paypal.isChecked()&&!bank_trans.isChecked()) {
                    Toast.makeText(getActivity(), "Select the payment method", Toast.LENGTH_SHORT).show();
                }*/

                /*else if(special_comments.length()==0 || special_comments.equals(""))
                {
                    Toast.makeText(getActivity(), "Enter the special comments" ,Toast.LENGTH_SHORT).show();
                }*/

                else {

                    /* if(yes.isChecked())
                    {

                        willint_to_yes_no="yes";
                    }

                    if(no.isChecked())
                    {
                        willint_to_yes_no="no";
                    }

                    if(part_time.isChecked())
                    {
                        type_of_job="Part time";
                    }

                    if(full_time.isChecked())
                    {
                        type_of_job="Full time";
                    }

                    if(cash.isChecked())
                    {

                        payment_mtd="Cash";
                    }
                    if(paypal.isChecked())
                    {
                        payment_mtd="Paypal";
                    }
                    if(bank_trans.isChecked())
                    {
                        payment_mtd="Bank transfer";
                    }
*/
                    if(cleaning.isChecked())
                    {
                        get_skills=get_skills+"Cleaning,";
                    }
                    if(painting.isChecked())
                    {
                        get_skills=get_skills+"Painting,";
                    }
                    if(movers.isChecked())
                    {
                        get_skills=get_skills+"Movers,";
                    }
                    if(driving.isChecked())
                    {
                        get_skills=get_skills+"Driving,";
                    }
                    if(aircon.isChecked())
                    {
                        get_skills=get_skills+"Ac,";
                    }
                    if(chef.isChecked())
                    {
                        get_skills=get_skills+"Chef,";
                    }
                    if(Makeup.isChecked())
                    {
                        get_skills=get_skills+"Beautician,";
                    }

                    new MyTask_create_user().execute();
                }
            }

            });



        return v;
    }


    private class MyTask_create_user extends AsyncTask<Void,Void,Void>
    {

        String status="",message="";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog=new ProgressDialog(getActivity());
            progressDialog.setMessage("Creating account...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Register_Json/data?";

            String path=getActivity().getResources().getString(R.string.url_service)+"prof/Prof_Json/data?";
            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"prof_registeruser"+"&firstname="+first_name
                        +"&lastname="+last_name+"&email="+email+"&password="+password_st+"&address1="+address_1+
                        "&address2="+address_2+"&country="+select_Country+"&state="+state_st+"&city="+select_city_st+"" +
                        "&zip_code="+zip_code+"&phone_number="+mobile_num+"&willing_tenkm_work="+willint_to_yes_no+
                        "&job_type="+type_of_job+"&payment_type="+payment_mtd+"&skills="+get_skills+"&special_service="+special_service
                        +"&comments="+special_comments,null);

                /*uri=new URI("https",path+"service="+"prof_registeruser"+"&firstname="+first_name
                        +"&lastname="+last_name+"&email="+email+"&password="+password_st+"&address1="+address_1+
                        "&address2="+address_2+"&country="+select_Country+"&state="+state_st+"&city="+select_city_st+"" +
                        "&zip_code="+zip_code+"&phone_number="+mobile_num+"&willing_tenkm_work="+willint_to_yes_no+
                        "&job_type="+type_of_job+"&payment_type="+payment_mtd+"&skills="+*//*selectedService*//*"TARA"+"&special_service="+special_service
                        +"&comments="+special_comments,null);*/
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true"))
            {
                final AlertDialog.Builder alert=new AlertDialog.Builder(getActivity());
                alert.setTitle("Account created successfully..");
                alert.setMessage("Verification mail has been sent to your mail id..");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                        Intent intent=new Intent(getActivity(),Sign_activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        //getActivity().finish();
                        startActivity(intent);
                    }
                }).show();

            }
            else
            {
                Toast.makeText(getActivity(), message ,Toast.LENGTH_SHORT).show();
                //firstname.setText("");lastname.setText("");password.setText("");confirmPssd.setText("");email_id.setText("");
               // phonenumber.setText("");
                //firstname.requestFocus();
            }
        }
    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private class ServicesMasterDataTask extends AsyncTask<Void, Void, Void> {
        String status = "", message = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            /* getString(R.string.url_service) : //www.quichelp.com/json/*/
            String path = getActivity().getResources().getString(R.string.url_service) + "ServicePrice_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*https://quichelp.com/json/ServicePrice_Json/data?service=service_types&category=all*/
            URI uri = null;

            try {
                uri = new URI("https", path + "service=service_types&category=all", null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } /*catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpGet get;
            try {
                get = new HttpGet(uri.toURL().toString());

                HttpResponse response = client.execute(get);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            if (result != null) {

                QuickHelpLog.e("ServicesMasterData", result);
                try {
                    object = new JSONObject(result);

                    status = object.getString("status");
                    message = object.getString("message");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (object != null){
                    Type listType = new TypeToken<List<IdNameModel>>() {}.getType();
                    String allServiceArray = object.getJSONArray("message").toString();
                    ArrayList<IdNameModel> serviceListTemp = new Gson().fromJson(allServiceArray, listType);

                    /*servicesList.clear();
                    servicesList.addAll(serviceListTemp);
                    new DropDownSelectionIdNameModel(servicesList);*/
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            Toast.makeText(getActivity(), "Please select the city again", Toast.LENGTH_SHORT).show();

        }
    }

}


