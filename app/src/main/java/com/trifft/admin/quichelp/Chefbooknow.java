package com.trifft.admin.quichelp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/11/2016.
 */

public class Chefbooknow extends Fragment implements AdapterView.OnItemSelectedListener, CommonInterface {

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    ProgressDialog progressDialog;
    SharedPreferences check;
    String value;
    Button chef;
    EditText no_of_person, email;

    String first_name, mobileNumber, last_name, date_data, time_data, FOOD_type, no_of_Persons, address_1, address_2, special_comments, selectedCity, Zipcode;

    EditText firstname, lastname, date, time, /*select_city,*/ specialComments, address1, address2,
            zip_code, mobile_number, food_type;

    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID;//EMAIL_ID=Serviceselectionpage.EMAIL;
    private EditText food_course;
    private String FOOD_course;

    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.chefbooknow, container, false);

        chef = (Button) v.findViewById(R.id.getaprice);
        no_of_person = (EditText) v.findViewById(R.id.no_of_person);
        date = (EditText) v.findViewById(R.id.date);
        email = (EditText) v.findViewById(R.id.email);
        time = (EditText) v.findViewById(R.id.time);
        mobile_number = (EditText) v.findViewById(R.id.phonenumber);
        date = (EditText) v.findViewById(R.id.date);
//        select_city = (EditText) v.findViewById(R.id.selectcity);

        spnCountry = v.findViewById(R.id.spn_country);
        time = (EditText) v.findViewById(R.id.time);
        food_type = (EditText) v.findViewById(R.id.foodtype);
        food_course = (EditText) v.findViewById(R.id.foodcourse);
        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);


        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            email.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);

     /* if(value.equals("CHENNAI"))
      {
          //chef.setBackgroundResource(R.drawable.ed_button2);
          select_city.setText("CHENNAI");
      }
      else{select_city.setText("MUMBAI");
      }*/


        no_of_person.setOnClickListener(new Popup_fiveroom(getActivity(), no_of_person, "persons"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));

//        select_city.setOnClickListener(new Select_country(getActivity(), select_city));
        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);

        food_type.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_type_dialog();

            }
        });

        food_course.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_course_dialog();

            }
        });


        chef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                date_data = date.getText().toString();
                EMAIL_ID = email.getText().toString();
                FOOD_type = food_type.getText().toString();
                FOOD_course = food_course.getText().toString();
                no_of_Persons = no_of_person.getText().toString();
                time_data = time.getText().toString();
                mobileNumber = mobile_number.getText().toString();
                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();
                selectedCity = spnCountry.getSelectedItem().toString();
                Zipcode = zip_code.getText().toString();
                special_comments = specialComments.getText().toString();


                if (first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (EMAIL_ID.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(EMAIL_ID)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (FOOD_type.equals("")) {
                    Toast.makeText(getActivity(), "Select the Food type", Toast.LENGTH_SHORT).show();
                } else if (FOOD_course.equals("")) {
                    Toast.makeText(getActivity(), "Select the Food type", Toast.LENGTH_SHORT).show();
                } else if (no_of_Persons.equals("")) {
                    Toast.makeText(getActivity(), "Select the No of persons", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.equals("")) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (date_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                } else if (time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (Zipcode.length() == 0) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                } else if (Zipcode.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                } else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {

                    new Mytask().execute();
                }
            }
        });
        return v;
    }

    TextView normal3, special3, normal5, special5;

    private void food_type_dialog() {
       /* LayoutInflater in=(LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.foodtype_dialog,null);*/

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.foodtype_dialog);

        normal3 = (TextView) dialog.findViewById(R.id.normal_three);
        special3 = (TextView) dialog.findViewById(R.id.special_three);
        normal5 = (TextView) dialog.findViewById(R.id.normal_five);
        special5 = (TextView) dialog.findViewById(R.id.special_five);

        normal3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_type.setText("veg");

                dialog.dismiss();

            }
        });

        special3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_type.setText("veg");
                dialog.dismiss();
            }
        });

        normal5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_type.setText("nonveg");
                dialog.dismiss();
            }
        });
        special5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_type.setText("nonveg");
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void food_course_dialog() {
       /* LayoutInflater in=(LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.foodtype_dialog,null);*/

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.foodcourse_dialog);

        normal3 = (TextView) dialog.findViewById(R.id.normal_three);
        special3 = (TextView) dialog.findViewById(R.id.special_three);
        normal5 = (TextView) dialog.findViewById(R.id.normal_five);
        special5 = (TextView) dialog.findViewById(R.id.special_five);

        normal3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_course.setText("3CN");

                dialog.dismiss();

            }
        });

        special3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_course.setText("5CN");
                dialog.dismiss();
            }
        });

        normal5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_course.setText("3CN");
                dialog.dismiss();
            }
        });
        special5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                food_course.setText("5CN");
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    private class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", jod_id = "", message = "", price = "";
        boolean status1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Callchef_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();


            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "insert_callchef_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&lastname=" + last_name +
                        "&email=" + EMAIL_ID +
                        "&phone=" + mobileNumber +
                        "&food_type=" + FOOD_type +
                        "&food_course=" + FOOD_course +
                        "&no_person=" + no_of_Persons +
                        "&frm_address1=" + address_1 +
                        "&frm_address2=" + address_2 +
                        "&pref_date=" + date_data +
                        "&pref_time=" + time_data +
                        "&frm_city=" + selectedCity +
                        "&frm_zip=" + Zipcode +
                        "&sp_comments=" + special_comments, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status1 = object.getBoolean("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status);

            if (status1 == true) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status1 == true) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }
}
