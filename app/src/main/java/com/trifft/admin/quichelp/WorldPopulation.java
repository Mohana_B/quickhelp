package com.trifft.admin.quichelp;

/**
 * Created by MINE on 10-02-2017.
 */

public class WorldPopulation {

    private String subject;
    private String date;

    public WorldPopulation(String subject,
                           String date) {
        this.subject = subject;
        this.date = date;

    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
