package com.trifft.admin.quichelp;


import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by PSDeveloper on 11/18/2016.
 */

public class Popup_Ten_room implements View.OnClickListener {

    TextView close;
    Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b10;
    Context context;
    EditText select_rooms;

    String bed_room="Bed room",room="rooms";
    String check_room,store_room_value;

    public Popup_Ten_room(Context con, EditText bedroom,String data) {
        context=con;
        select_rooms=bedroom;
check_room=data;

    }

    String val;
    @Override
    public void onClick(View view) {




        LayoutInflater in=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_10_rooms,null);

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(vv);
        dialog.show();
        close=(TextView) vv.findViewById(R.id.btn_close);
        b1 = (Button)vv.findViewById(R.id.button1);
        b2 = (Button)vv.findViewById(R.id.button2);
        b3 = (Button)vv.findViewById(R.id.button3);
        b4 = (Button)vv.findViewById(R.id.button4);
        b5 = (Button)vv.findViewById(R.id.button5);
        b6 = (Button)vv.findViewById(R.id.button6);
        b7 = (Button)vv.findViewById(R.id.button7);
        b8 = (Button)vv.findViewById(R.id.button8);
        b9 = (Button)vv.findViewById(R.id.button9);
        b10 = (Button)vv.findViewById(R.id.button10);

        if(check_room.equals(bed_room))
        {
            store_room_value=bed_room;
        }
        else
        {
            store_room_value=room;
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b1.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b2.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b3.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b4.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b5.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b6.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b7.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b8.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b9.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b10.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });






    }
}
