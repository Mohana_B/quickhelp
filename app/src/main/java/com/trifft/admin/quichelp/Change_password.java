package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by PSDeveloper on 12/27/2016.
 */

public class Change_password extends AppCompatActivity implements View.OnClickListener{

    ImageView language;
    LinearLayout backArrow;
    EditText email,new_Password,confirm_password;
    Button update;
    String email_id,pssd,cnfm_pssd;
    String status,message;
    ProgressDialog progressDialog;
    String action="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);

        email=(EditText)findViewById(R.id.email);
        new_Password=(EditText)findViewById(R.id.new_password);
        confirm_password=(EditText)findViewById(R.id.confirm_password);
        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        language=(ImageView) findViewById(R.id.language);
        update=(Button) findViewById(R.id.update);

        email.setText(Serviceselectionpage.EMAIL);

        backArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Change_password.this,language));

        action=getIntent().getAction();



        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                email_id=email.getText().toString();
                pssd=new_Password.getText().toString();
                cnfm_pssd=confirm_password.getText().toString();

                if(email_id.length()==0||email_id.equals(""))
                {
                    Toast.makeText(Change_password.this, "Enter the email" ,Toast.LENGTH_SHORT).show();
                }
                else if(pssd.length()==0||pssd.equals(""))
                {
                    Toast.makeText(Change_password.this, "Enter the New Password" ,Toast.LENGTH_SHORT).show();
                }
                else if(cnfm_pssd.length()==0||cnfm_pssd.equals(""))
                {
                    Toast.makeText(Change_password.this, "Enter the Confirm Password" ,Toast.LENGTH_SHORT).show();
                }
                else if(!pssd.equals(cnfm_pssd))
                {
                    Toast.makeText(Change_password.this, "Password mismatch" ,Toast.LENGTH_SHORT).show();
                }
                else
                {
new MyTask().execute();
                }

            }
        });

    }

    private class MyTask extends AsyncTask<Void,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog=new ProgressDialog(Change_password.this);
            progressDialog.setMessage("Updating password...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String temp_path="";

            if(action.equals("user"))
            {
                http://www.quichelp.com/json/prof/Prof_Json/data?service=prof_updatepassword&email=aasai.muthu17@gmail.com&password=test

                temp_path="Login_Json/data?";
            }

            if(action.equals("prof"))
            {
                temp_path="prof/Prof_Json/data?";
            }

            String path=getResources().getString(R.string.url_service)+temp_path;

            DefaultHttpClient client=new DefaultHttpClient();


            String service_name="";

            if(action.equals("user"))
            {
                service_name="updatepassword";
            }

            if(action.equals("prof"))
            {
                service_name="prof_updatepassword";
            }

            URI uri = null;

            try {
                uri=new URI("https",path+"service="+service_name+"&email="+email_id+"&password="+pssd,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if(status.equals("true"))
            {
                Toast.makeText(Change_password.this, message, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Change_password.this, Serviceselectionpage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                //getActivity().finish();
                startActivity(intent);
            }
            else
            {
                Toast.makeText(Change_password.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {

            case R.id.imageArrow:

                onBackPressed();
                break;

        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
