package com.trifft.admin.quichelp;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by PSDeveloper on 11/18/2016.
 */

public class Popup_fiveroom implements View.OnClickListener {


    TextView header,close;
    Button b1,b2,b3,b4,b5;
    Context context;
    EditText select_rooms;
    String living_room="Living room",kitchen_room="Kitchen room",toilet_room="Toilet room",rooms="rooms",
            persons="persons",aircon="aircon",driver="driver",profrssional="professional";


    String check_room,store_room_value;
    public Popup_fiveroom(Context con, EditText bedroom,String data) {
        context=con;
        select_rooms=bedroom;
        check_room=data;
    }

String val;
    @Override
    public void onClick(View view) {

        LayoutInflater in=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_5_rooms,null);
        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(vv);
        close=(TextView) vv.findViewById(R.id.btn_close);
        header=(TextView) vv.findViewById(R.id.header);
        b1 = (Button)vv.findViewById(R.id.button1);
        b2 = (Button)vv.findViewById(R.id.button2);
        b3 = (Button)vv.findViewById(R.id.button3);
        b4 = (Button)vv.findViewById(R.id.button4);
        b5 = (Button)vv.findViewById(R.id.button5);
        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        if(check_room.equals(living_room))
        {
            store_room_value=living_room;
        }
        else if(check_room.equals(kitchen_room))
        {
            store_room_value=kitchen_room;
        }
        else if(check_room.equals(toilet_room))
        {
            store_room_value=toilet_room;
        }
        else if(check_room.equals(persons))
        {
            header.setText("NO OF PERSONS");
            store_room_value=persons;
        }
        else if(check_room.equals(aircon))
        {
            header.setText("NO OF AIRCON");
            store_room_value=aircon;
        }
        else if(check_room.equals(driver))
        {
            header.setText("NO OF DRIVER");
            store_room_value=driver;
        }
        else if(check_room.equals(profrssional))
        {
            header.setText("PROFESSIONALS");
            store_room_value=profrssional;
        }



        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b1.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b2.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b3.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b4.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b5.getText().toString();
                select_rooms.setText(val+" "+store_room_value);
                dialog.dismiss();

            }
        });







    }
}

