package com.trifft.admin.quichelp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.QuickHelpLog;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

public class FBLoginActivity extends AppCompatActivity {

    //    private static final String EMAIL = "email";
    CallbackManager callbackManager;

    String f_name, l_name, f_id, f_gender, f_email;

    private FBLoginActivity fbLoginActivity;
    ProgressDialog progressDialog;

    String status, message, myAppUserId;
    String user_name, pass_word;
    String USER_ID, FIRSTNAME, LASTNAME, EMAIL, PHONE_NUM;
    SharedPreferences my_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_login);
        fbLoginActivity = this;

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
//        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        callbackManager = CallbackManager.Factory.create();

        loginButton.performClick();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        QuickHelpLog.e("TAG", "loggin succ");

                        AccessToken token = loginResult.getAccessToken();

                        GraphRequest request = GraphRequest.newMeRequest(token, new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                Log.i("LoginActivity", response.toString());
                                Log.i("LoginActivity2", object.toString());
                                callLoginAPI(object, response);


                            }

                        });


                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,location"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        QuickHelpLog.e("TAG", "loggin cancel");
                        finish(); // close screen and go back
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        QuickHelpLog.e("TAG", "loggin error");
                        finish(); // close screen and go back
                    }
                });

    }

    private void callLoginAPI(JSONObject object, GraphResponse response) {
        try {
            f_name = object.getString("first_name");
            l_name = object.getString("last_name");
            f_id = object.getString("id");
            f_email = object.getString("email"); // getting email, commented o test
            f_gender = object.getString("gender");
//            f_phNumber = object.getString("")
        } catch (JSONException e) {
            e.printStackTrace();
        }

        my_id = getSharedPreferences("My_id", MODE_PRIVATE);
        SharedPreferences.Editor editor = my_id.edit();
        editor.putString("my_id", f_id).putString("Firstname", f_name).putString("Lastname", l_name)
                .putString("email", f_email)/*.putString("phone_num",PHONE_NUM)*/.putString("USER", "true").commit();

        QuickHelpLog.e("user_id", USER_ID + " " + FIRSTNAME + " " + LASTNAME + " " + EMAIL + " " + PHONE_NUM);

        if (f_email != null && !f_email.trim().equals("")) {
            QuickHelpLog.e("TAG", " Email found");
            Utils.setEmail(f_email, fbLoginActivity);
            new fbcretelogin().execute();

        } else {
            // No Email found, show a dialog
            QuickHelpLog.e("TAG", "No Email found");

            DialogManager.showSingleBtnEditTxtPopup(fbLoginActivity, new DialogMangerCallback() {
                @Override
                public void onOkClick(View view) {
                    if (Utils.isValidStr(view.getTag().toString().trim()) && Utils.isValidEmail(view.getTag().toString().trim())) {
                        // check here valid email
                        Utils.setEmail(view.getTag().toString().trim(), fbLoginActivity);
                        navToDashboard();
//                        new fbcretelogin().execute();
                    } else {
                        DialogManager.showToast(fbLoginActivity, "Email should not be empty");
                    }

                }

                @Override
                public void onCancelClick(View view) {

                }
            }, "Alert", "For further process enter EMAIL address which you use frequently", "", 56, "ok");

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class fbcretelogin extends AsyncTask<Void, Void, Void> {

        String phnnum =/*"123456789"*/"";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(fbLoginActivity);
            progressDialog.setMessage("Login account...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;
            // https://www.quichelp.com/login/fblogin?userData={%22id%22:%22108649627216697%22,%22first_name%22:%22Quichelp%22,%22last_name%22:%22Serv%22,%22email%22:%22social.quichelp@gmail.com%22
            //String path="//www.servfy.com/json/Register_Json/data?";
            //http://www.quichelp.in/json/Register_Json/data?service=registeruser&email=abc@yahoo.com&password=1111&firstname=shrikar&lastname=chari&phonenumber=1234567890
//            https://quichelp.com/json/Register_Json/data?service=registeruser&email=jadavnirav8@gmail.com&password=null&firstname=Nirav&lastname=Jadav&phonenumber=123456789&oauthid=2659390200747213&oauth_provider=facebook

            String path = getResources().getString(R.string.url_service) + "Register_Json/data?";
            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {

                uri = new URI("https", path + "service=" + "registeruser" + "&email=" + f_email + "&password=" + pass_word + "&firstname=" + f_name
                        + "&lastname=" + l_name + "&phonenumber=" + phnnum + "&oauthid=" + f_id + "&oauth_provider=facebook", null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_responses = null;
            try {
                jsn_responses = new JSONObject(result);
                status = jsn_responses.getString("status");
                message = jsn_responses.getString("message");
                myAppUserId = jsn_responses.getString("userid");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("status", status);
            Log.e("message", message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if (status.equals("true")) {
                if (user_name != null) {

                    Log.e("username", user_name);
                    // database.insertData(user_name);

                    //if (getPage.equals("landing_page")) {

                    // store user Id in local here....... TARA

                    SharedPreferences.Editor editor = my_id.edit();
                    editor.putString("my_id", myAppUserId).commit();
                    navToDashboard();
                } else {
                    SharedPreferences.Editor editor = my_id.edit();
                    editor.putString("my_id", myAppUserId).commit();
                    navToDashboard();
                }

            } else {
                //  Toast.makeText(fbLoginActivity, message ,Toast.LENGTH_SHORT).show();
                //Toast.makeText(fbLoginActivity, message, Toast.LENGTH_SHORT).show();

                SharedPreferences.Editor editor = my_id.edit();
                editor.putString("my_id", myAppUserId).commit();

                new Mytask().execute();
            }
        }
    }

    private class Mytask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(fbLoginActivity);
            progressDialog.setMessage("Authenticating!Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path = getResources().getString(R.string.url_service) + "Login_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "login" + "&email=" + user_name + "&password=" + pass_word, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);
                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equals("true")) {
                try {
                    USER_ID = jsn_response.getString("userid");
                    FIRSTNAME = jsn_response.getString("firstname");
                    LASTNAME = jsn_response.getString("lastname");
                    EMAIL = jsn_response.getString("email");
                    PHONE_NUM = jsn_response.getString("phonenumber");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                my_id = getSharedPreferences("My_id", MODE_PRIVATE);
                SharedPreferences.Editor editor = my_id.edit();
                editor.putString("my_id", USER_ID).putString("Firstname", FIRSTNAME).putString("Lastname", LASTNAME)
                        .putString("email", EMAIL).putString("phone_num", PHONE_NUM).putString("USER", "true").commit();
                Log.e("user_id", USER_ID + " " + FIRSTNAME + " " + LASTNAME + " " + EMAIL + " " + PHONE_NUM);
            }

            Log.e("status", status);
            Log.e("message", message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if (status.equals("true")) {


                Log.e("username", user_name);
                // database.insertData(user_name);
                //  Toast.makeText(fbLoginActivity, message, Toast.LENGTH_SHORT).show();

                navToDashboard();

            } else {
                // Toast.makeText(fbLoginActivity, message, Toast.LENGTH_SHORT).show();
                setResult(RESULT_CANCELED);
                finish();

                navToDashboard();

                /*// not_activated
                if (message.equalsIgnoreCase("not_activated")){
                    navToDashboard();
                }*/

            }
        }
    }

    private void navToDashboard() {
        Intent intent = new Intent(fbLoginActivity, Serviceselectionpage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //getActivity().finish();
        startActivity(intent);
    }
}
