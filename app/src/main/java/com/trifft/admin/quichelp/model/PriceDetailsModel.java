package com.trifft.admin.quichelp.model;

import java.util.ArrayList;

public class PriceDetailsModel {
    private String id;
    private String name;
    private String price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return getName() + " : " + getPrice();
    }

    public static String printFullList(ArrayList<PriceDetailsModel> arraylist){
        StringBuilder stringBuilder = new StringBuilder();
        for (PriceDetailsModel model : arraylist){
            stringBuilder.append("\n").append(model.getName()).append(" : ").append(model.getPrice());
        }
        return stringBuilder.toString();
    }
}
