package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import fragment_adapter.Signin_adapter;

/**
 * Created by PSDeveloper on 11/10/2016.
 */

public class Sign_activity extends AppCompatActivity implements View.OnClickListener {

   ViewPager page;
    View user,professional;
    TextView users,professionals;
    ImageView imPro;
    SharedPreferences check;
    LinearLayout l3,l4,backArrow;
    String value;
    public static String getPage;

    public static final String EXTRA_PROF_USER = "extraProfessionalUser";
    private boolean isProfUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);

        Intent intent = getIntent();
        if (intent != null){
            if (intent.getExtras() != null && intent.getExtras().containsKey(EXTRA_PROF_USER))
                isProfUser = intent.getBooleanExtra(EXTRA_PROF_USER, false);
        }


        page = (ViewPager) findViewById(R.id.pager);
        user = (View) findViewById(R.id.v1);
        professional = (View) findViewById(R.id.v2);
        l3=(LinearLayout) findViewById(R.id.toolbarSingn);
      l4=(LinearLayout) findViewById(R.id.main2);
        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        imPro = (ImageView) findViewById(R.id.imageSignin);
        users = (TextView) findViewById(R.id.t1);
        professionals = (TextView) findViewById(R.id.t2);
        users.setOnClickListener(this);
        professionals.setOnClickListener(this);
        backArrow.setOnClickListener(this);

        check=getSharedPreferences("Default_Language",MODE_PRIVATE);
        value=check.getString("Value","");

        //getPage=getIntent().getExtras().getString("unique_id");

      /*  if(value.equals("CHENNAI"))
        {
            imPro.setVisibility(View.INVISIBLE);
            l3.setBackgroundResource(R.color.app_heading2);
            l4.setBackgroundResource(R.color.app_heading2);
        }*/


        Signin_adapter fa = new Signin_adapter(getSupportFragmentManager());
        page.setAdapter(fa);

        page.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (page.getCurrentItem() == 0) {
                    user.setVisibility(View.VISIBLE);
                    professional.setVisibility(View.INVISIBLE);
                }
                if (page.getCurrentItem() == 1) {
                    user.setVisibility(View.INVISIBLE);
                    professional.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }


        });

        if (isProfUser)
            page.setCurrentItem(1, true);
        else
            page.setCurrentItem(0,true);


        imPro.setOnClickListener(new Language_change(Sign_activity.this,imPro));


    }




    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.t1: {
                page.setCurrentItem(0);
                break;
            }

            case R.id.t2: {
                page.setCurrentItem(1);
                break;
            }
            case R.id.imageArrow: {
                onBackPressed();

                break;
            }


        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


    // TARA
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        try {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
                Log.d("Activity", "ON RESULT CALLED");
            }
        } catch (Exception e) {
            Log.d("ERROR", e.toString());
        }
    }


}
