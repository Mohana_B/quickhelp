package com.trifft.admin.quichelp.model;

public class CountryMasterModel {
    private boolean status;
    private City message;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public City getMessage() {
        return message;
    }

    public void setMessage(City message) {
        this.message = message;
    }

    public class City {
        private String city;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

}
