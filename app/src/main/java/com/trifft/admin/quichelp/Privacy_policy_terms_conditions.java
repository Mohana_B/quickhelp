package com.trifft.admin.quichelp;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by aduser on 2/10/2018.
 */

public class Privacy_policy_terms_conditions extends AppCompatActivity implements View.OnClickListener {

    LinearLayout backArrow;

    WebView content;

    TextView header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_privacy_policy);

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);

        content = (WebView) findViewById(R.id.content);

        header=(TextView) findViewById(R.id.header);

        header.setText("Terms & Conditions");

        String text = "<html>" +
                "<head><body>"
                + "<center><h1 style=\"color:#005a8c\">Terms & Conditions</h1></center>\n" +
                "<p style=\"text-align:justify\">The terms and conditions of use (refer to as Terms) defined below between Quichelp Solutions (refer to as \"Quichelp\", \"We\", \"Us\", \"Company\" , \"website\" in the rest of the document) and the Users / business users / registered users  (termed as Members,  You, Yours, Yourself, User). If you are reading this document and browsing further pages on Quichelp.com and Quichelp App means you agree by the Terms & Conditions. Any use of the above terminology or other words in singular, plural, capitalization, and/ or he/she or they, are taken interchangeable and therefore are referring to the same.</p>\n" +
                "<p style=\"text-align:justify\">Please read carefully the terms before using, registering or accessing any material in the website. Your continuous viewing and registering on the website indicates complete acceptance of the terms and shall be legally bounded.</p>\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">General Usage Conditions</h2>\n" +
                "\n" +
                "\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">1.\tDo not enter false information in any form or give incorrect information or information about others such as email id, phone numbers in our platform.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">2.\tMembers shall not provide any information that can cause harmful or impede the security of other members.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">3.\tMembers shall not copy, scan, run robots, stall, probe, test any of our website contents on any other servers or connections that shall breach our security.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">4.\tUnlawful activities by any means are not allowed on our website and App.</li>\n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">Payment Policy</h2>\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">1.\tThe payment option available to members is through \"Instamojo\" which you will see as  \"Pay with Instamojo\" or  \"Pay now\" button. As you are all aware that Instamojo is the widely used and accepted Payment Gateway. The payments are done in a highly secure and encrypted form.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">2.\tQuichelp values your personal details with regards to Payments as you may need to provide the details of your credit card, bank details.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">3.\tNotifications will be sent to members to the email id once payment is successful and in most cases a SMS alert is also sent to the phone number entered in the service request form.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">4.\tAs the Government is pushing for clean money /cashless economy we full support  online payment model, but for our valuable customers we also give the option to pay by cash or by bank transfer upon request.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">5.\tOnce you are in Payment gateway, we have no view of the details you enter and thus we don't take any liability of data exchanging happening on the payment servers.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">6.\tIf you have questions or issues with regards to payment policy please writes to us at <a style=\"color:#005a8c\" href=\"mailto:contact@quichelp.com\">contact@Quichelp.com</a>.</li>\n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">Job Order and Cancellation Policies</h2>\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">1.\tAt Quichelp, we strive to provide the best service possible to the customers.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">2.\tJobs placed on our website or App cannot be cancelled within 24 hours of the start of the job. You may postpone it another day either online or by calling our customer care service or drop us an email at <a style=\"color:#005a8c\" href=\"mailto:contact@quichelp.com\">contact@Quichelp.com</a>. Some services may have different cancellation time, get in contact with our customer care if you have any questions.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">3.\tOur services start from 7AM until 9PM, on all days, please contact us if you need our services in other times.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">4.\tYou will receive a confirmation of our service through email, please keep your email updated on our site.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">5.\tIf you cancel the job after Payment is processed, you will get the refund after deducting the necessary transfer charges.</li>\n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">Indemnification</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "You  agree to fully indemnify, hold harmless  and defend Quichelp and its directors, officers, employees, agents, stockholders,  subsidiaries, affiliates, successors, assigns, service providers, and suppliers (collectively, \"Indemnified Parties\") from and against all claims, actions, suits, demands, damages, liabilities, obligations, losses, settlements, judgments, costs and expenses (including without limitation reasonable legal fees and costs), whether or not involving a third party claim which arises out of or result from but not limited to (1) any breach of any representation of warranty of terms contained in this Agreement, (2) any breach of any covenant or other obligation or duty of Quichelp under this Agreement or under applicable law, (3) claims arising out of the negligence or willful malfeasance of  \"Indemnified Parties\", in each case whether or not caused by the negligence of Quichelp or any other Indemnified Parties and whether or not the relevant Claim has merit.\n" +
                "</p> \n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">Limitation of Liability</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "In no event shall Quichelp or its directors, officers, employees, agents, stockholders,  subsidiaries, affiliates, successors, assigns, service providers, and suppliers be liable to you for any special, indirect, incidental, consequential, punitive, reliance, or exemplary damages (including without limitation lost business opportunities, lost revenues, or loss of anticipated profits or any other pecuniary or non-pecuniary loss or damage of any nature whatsoever) arising out of or relating to (i) this agreement, (ii) the services, the site or any reference site, or (iii) your use or inability to use the services, the site (including any and all materials) or any reference sites, even if Quichelp or a Quichelp authorized representative has been advised of the possibility of such damages (iv) loss of use of data, or profits, whether or not foreseeable or whether or not Quichelp has been advised of the possibility of such damages, or based on any theory of liability, including breach of warranty, negligence or any other claim arising out of or in connection with Your use of or access to the Website, its Services or Materials.\n" +
                "</p> \n" +
                "<p style=\"text-align:justify\">Quichelp shall not assume any liability for the non-availability of any service at any point of time.</p>\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">Termination</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                " If any of the terms & conditions were abused, violated in part or full or complaints received due to violations of rules of the land governed , We may terminate your access to our Site, without cause or notice, which may result in the forfeiture and destruction in part or all of the information associated with your account. All provision of this Terms & Conditions document shall survive termination, including, without limitation, ownership provisions, disclaimers, indemnity and limitations of Liability.\n" +
                " </p> \n" +
                " \n" +
                " \n" +
                "<h2 style=\"color:#005a8c\">Notifications</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "Quichelp reserves the right to change these conditions from time to time as it sees fits and your continued use of the site will signify your acceptance of any changes to these terms.   We encourage you to read our Policies from time to time. \n" +
                "</p> \n" +
                " \n" +
                " \n" +
                " <h2 style=\"color:#005a8c\">Disclaimer</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "The Site and its contents are offered for informational purposes only;  Quichelp and its related App shall not be responsible or liable for the accuracy, usefulness or availability of any information transmitted or made available through this site, and shall not be responsible or liable for any discrepancies arising thereof.  \n" +
                "</p> \n" +
                " \n" +
                "  <h2 style=\"color:#005a8c\">Governing Law</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "These terms and conditions are governed by the Local regulations. By accessing this website (using our services and products thereof) you consent to the exclusive jurisdiction of the courts in India  in all disputes arising out of such access.  Failure of the Company to enforce any of the above provisions set out in these Terms and Conditions and any Agreement, or failure to exercise any option to terminate, shall not be construed as wavier of such provisions and shall not affect the validity of these Terms and Conditions or any Agreement or part thereof.\n" +
                "</p>"
                +
                "</body></html>";

        content.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
        WebSettings settings = content.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        Resources res = getResources();
        settings.setDefaultFontSize((int) 17);
        backArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.imageArrow:
                onBackPressed();

                break;
        }

    }

}
