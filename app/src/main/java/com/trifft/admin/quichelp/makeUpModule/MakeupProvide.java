package com.trifft.admin.quichelp.makeUpModule;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.trifft.admin.quichelp.Myapplication;
import com.trifft.admin.quichelp.Pricing_adapter_class;
import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Select_country;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.model.PriceDetailsModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Fatima.t on 13/03/2020.
 */

public class MakeupProvide extends Fragment implements View.OnClickListener {

    Button ac, pricing_details;


    ViewPager pager = MakeupMainActivity.viewpage;
    SharedPreferences check;
    String value;
    ProgressDialog progressDialog;
    TextView t1, t2, t3, t4, t5;

    ImageView ac_image;
    View view;

    RelativeLayout bridalRL, partyRL, glamGlowRL, hairSpaRL, facialRL, bodySpaRL;
    LinearLayout bridalLL, partyLL, glamGlowLL, hairSpaLL, facialLL, bodySpaLL;
    ImageView bridalImg, partyImg, glamGlowImg, hairSpaImg, facialImg, bodySpaImg;

    String USER_ID= Serviceselectionpage.USER_ID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.makeup_provide, container, false);
        t1 = (TextView) view.findViewById(R.id.tei);
        t2 = (TextView) view.findViewById(R.id.tev);
        t3 = (TextView) view.findViewById(R.id.te);
        t4 = (TextView) view.findViewById(R.id.texg);
        t5 = (TextView) view.findViewById(R.id.tet);
        ac = (Button) view.findViewById(R.id.b2);
        ac_image = (ImageView) view.findViewById(R.id.ac);

        initializeViews(view);

        pricing_details = (Button) view.findViewById(R.id.pricing_details);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

      /*  if(value.equals("CHENNAI"))
        {
            t1.setTextColor(getResources().getColor(R.color.app_heading2));
            t2.setTextColor(getResources().getColor(R.color.app_heading2));
            t3.setTextColor(getResources().getColor(R.color.app_heading2));
            t4.setTextColor(getResources().getColor(R.color.app_heading2));
            t5.setTextColor(getResources().getColor(R.color.app_heading2));

            ac.setBackgroundResource(R.drawable.ed_button2);
        }*/


        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utils.isValidStr(USER_ID)) {
                    pager.setCurrentItem(1, true);
                } else {
                    DialogManager.showToast(getActivity(), getString(R.string.not_logged_in_msg));
                }

            }
        });


        pricing_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isOnline()) {
                    if (Myapplication.glogol_city.size() == 0) {

                        load_pricing_dialog();
                        new Load_city_background().execute();
                        //new Load_pricing_details().execute();
                    } else {
                        load_pricing_dialog();
                    }

                } else {
                    Toast.makeText(getActivity(), "No network connection to load cities..", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    private void initializeViews(View view) {
        bridalRL = view.findViewById(R.id.bridal_rl);
        partyRL = view.findViewById(R.id.party_rl);
        glamGlowRL = view.findViewById(R.id.glam_glow_rl);
        hairSpaRL = view.findViewById(R.id.hair_spa_rl);
        facialRL = view.findViewById(R.id.facial_rl);
        bodySpaRL = view.findViewById(R.id.body_spa_rl);

        bridalLL = view.findViewById(R.id.bridal_data_ll);
        partyLL = view.findViewById(R.id.party_data_ll);
        glamGlowLL = view.findViewById(R.id.glam_glow_data_ll);
        hairSpaLL = view.findViewById(R.id.hair_spa_data_ll);
        facialLL = view.findViewById(R.id.facial_data_ll);
        bodySpaLL = view.findViewById(R.id.body_spa_data_ll);

        bridalImg = view.findViewById(R.id.bridal_img);
        partyImg = view.findViewById(R.id.party_img);
        glamGlowImg = view.findViewById(R.id.glam_glow_img);
        hairSpaImg = view.findViewById(R.id.hair_spa_img);
        facialImg = view.findViewById(R.id.facial_img);
        bodySpaImg = view.findViewById(R.id.body_spa_img);

        bridalRL.setOnClickListener(this);
        partyRL.setOnClickListener(this);
        glamGlowRL.setOnClickListener(this);
        hairSpaRL.setOnClickListener(this);
        facialRL.setOnClickListener(this);
        bodySpaRL.setOnClickListener(this);
    }

    Pricing_adapter_class adapter_class;
    ArrayList<HashMap<String, String>> arrayList_globol;
    ListView list;
    EditText select_city;

    String responsePriceDetails = "";
    TextView priceDetailsTv;
    private void load_pricing_dialog() {

        LayoutInflater in = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vv = in.inflate(R.layout.pricing_popup, null);
        //final TextView cbe=(TextView)vv.findViewById(R.id.cbe);

       /* final PopupWindow pop=new PopupWindow(vv,ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,true);
        pop.setOutsideTouchable(true);
        pop.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transperenr)));
        pop.setFocusable(true);

        pop.showAtLocation(about, Gravity.CENTER,0,0);*/


        Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(vv);
        dialog.setCancelable(false);
        // dialog.getWindow().setBackgroundDrawable(null);
        //dialog.addContentView(new View(this), (new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT)));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        list = (ListView) vv.findViewById(R.id.list);
        priceDetailsTv = vv.findViewById(R.id.price_data_tv);

        ImageView close = (ImageView) vv.findViewById(R.id.close);

        select_city = (EditText) vv.findViewById(R.id.select_city);

        select_city.setOnClickListener(new Select_country(getActivity(), select_city));


        select_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (isOnline()) {
                    if (!select_city.getText().toString().equals("Select City")) {
                        //arrayList_globol.clear();
                        //adapter_class.notifyDataSetChanged();

                        new Load_pricing_details().execute(select_city.getText().toString());
                    }
                } else {
                    Toast.makeText(getActivity(), "No network connection to load pricing..", Toast.LENGTH_SHORT).show();
                }


            }
        });
        /*select_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/


        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                //pop.dismiss();
            }
        });

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bridal_rl:
                showHideViews(bridalImg, bridalLL);
                break;
            case R.id.party_rl:
                showHideViews(partyImg, partyLL);
                break;
            case R.id.glam_glow_rl:
                showHideViews(glamGlowImg, glamGlowLL);
                break;
            case R.id.hair_spa_rl:
                showHideViews(hairSpaImg, hairSpaLL);
                break;
            case R.id.facial_rl:
                showHideViews(facialImg, facialLL);
                break;
            case R.id.body_spa_rl:
                showHideViews(bodySpaImg, bodySpaLL);
                break;
        }

    }

    private void showHideViews(ImageView image, LinearLayout dataLL) {
        if (dataLL.getVisibility() == View.VISIBLE){
            image.setBackgroundResource(R.mipmap.dropdown_up);
            dataLL.setVisibility(View.GONE);
        } else {
            image.setBackgroundResource(R.mipmap.dropdown_down);
            dataLL.setVisibility(View.VISIBLE);
        }
    }


    private class Load_city_background extends AsyncTask<Void, Void, Void> {

        String status = "", message = "";

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Login_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();


            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "getcountry", null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject object = null;
            if (result != null) {
                try {
                    object = new JSONObject(result);

                    status = object.getString("status");
                    message = object.getString("message");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.e("status", status);


            ArrayList<String> arrayList = new ArrayList<String>();
            String str_array[];
            String city = "";
            if (status.equals("true")) {
                try {
                    JSONObject get_city = object.getJSONObject("message");

                    city = get_city.getString("city");

                    str_array = city.split(",");

                    for (int i = 0; i < str_array.length; i++) {
                        Myapplication.glogol_city.add(str_array[i]);
                    }

                    // globol_city=arrayList.;

                    Log.e("globol_city", Myapplication.glogol_city.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            return null;
        }
    }

    private class Load_pricing_details extends AsyncTask<String, Void, Void> {
        String status = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... voids) {

            String result = null;

//            String path = getResources().getString(R.string.url_service) + "Login_Json/data?";
            String path=getResources().getString(R.string.url_service)+"ServicePrice_Json/data?";
            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
//                uri = new URI("https", path + "service=" + "getcountrycityprice" + "&country=" + "india" + "&city=" + voids[0] + "&servicetype=beauty", null);
                uri=new URI("https",path+"service=service_types" +"&category="+"7"+"&city="+voids[0],null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
//                HttpPost post = new HttpPost(uri.toURL().toString());
                HttpGet post = new HttpGet(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);

                status = jsn_response.getString("status");


                arrayList_globol = new ArrayList<HashMap<String, String>>();

                if (status.equals("true")) {

                    JSONObject jsonObject = new JSONObject(jsn_response.getString("message"));
                    if (jsonObject != null){

                        Type listType = new TypeToken<List<PriceDetailsModel>>() {}.getType();
                        String makeupArray = jsonObject.getJSONArray("Makeup").toString();
                        ArrayList<PriceDetailsModel> makeupList = new Gson().fromJson(makeupArray, listType);

                        String spaArray = jsonObject.getJSONArray("Spa").toString();
                        ArrayList<PriceDetailsModel> spaList = new Gson().fromJson(spaArray, listType);

                        String makeupData = "<font color=" + "#005a8c" + ">" + "Makeup" + "</font>" +PriceDetailsModel.printFullList(makeupList );
                        String spaData = "<font color=" + "#005a8c" + ">" + "Spa" + "</font>" +PriceDetailsModel.printFullList(spaList);

                        responsePriceDetails = makeupData + "\n\n" + spaData;

                    }

                    /*JSONArray array = jsn_response.getJSONArray("message");

                    Log.e("Makeup provide", jsn_response.toString() );

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject get_obj = array.getJSONObject(i);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(Cleaningprovide.id, get_obj.getString("id"));
                        map.put(Cleaningprovide.country, get_obj.getString("country"));
                        map.put(Cleaningprovide.city, get_obj.getString("city"));
                        map.put(Cleaningprovide.code, get_obj.getString("code"));
                        map.put(Cleaningprovide.currency, get_obj.getString("currency"));
                        map.put(Cleaningprovide.paint, get_obj.getString("paint"));
                        map.put(Cleaningprovide.movers, get_obj.getString("movers"));
                        map.put(Cleaningprovide.cleaning, get_obj.getString("cleaning"));
                        map.put(Cleaningprovide.clean_living, get_obj.getString("clean_living"));
                        map.put(Cleaningprovide.clean_bed, get_obj.getString("clean_bed"));
                        map.put(Cleaningprovide.clean_kitchen, get_obj.getString("clean_kitchen"));
                        map.put(Cleaningprovide.clean_toilet, get_obj.getString("clean_toilet"));
                        map.put(Cleaningprovide.call_chef_3CS, get_obj.getString("call_chef_3CS"));
                        map.put(Cleaningprovide.call_chef_3CN, get_obj.getString("call_chef_3CN"));
                        map.put(Cleaningprovide.call_chef_5CS, get_obj.getString("call_chef_5CS"));
                        map.put(Cleaningprovide.call_chef_5CN, get_obj.getString("call_chef_5CN"));
                        map.put(Cleaningprovide.aircon, get_obj.getString("aircon"));
                        map.put(Cleaningprovide.driving, get_obj.getString("driving"));

                        arrayList_globol.add(map);

                        responsePriceDetails = "Realstate Standard : "+get_obj.getString("realestate_standard") +
                                "\n\n Realstate Executive : "+get_obj.getString("realestate_executive") +
                                "\n\n Realstate Airbnb : "+get_obj.getString("realestate_airbnb");
                    }*/

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if (status.equals("true")) {
                //load_pricing_dialog();

                adapter_class = new Pricing_adapter_class(getActivity(), arrayList_globol);
                list.setAdapter(adapter_class);

                if (Utils.isValidStr(responsePriceDetails)) {
                    responsePriceDetails = responsePriceDetails.replace("\n", "<br>");
                    priceDetailsTv.setText(Html.fromHtml(responsePriceDetails));
                    list.setVisibility(View.GONE);
                    priceDetailsTv.setVisibility(View.VISIBLE);
                } else {
                    list.setVisibility(View.VISIBLE);
                    priceDetailsTv.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}

