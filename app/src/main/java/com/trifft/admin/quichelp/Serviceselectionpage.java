package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.QuickHelpLog;
import com.trifft.admin.quichelp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fragment_adapter.Servicelandingadapter;

/**
 * Created by Admin on 11/10/2016.
 */
public class Serviceselectionpage extends AppCompatActivity implements View.OnClickListener, CommonInterface {

    public static ArrayList<String> globol_array_city;



    ViewPager viewpage;
    View user,professional;
    TextView users,professionals;
    ImageView Ndrawer,language;
    TextView signin,first_name;
    DrawerLayout dlayout;
    LinearLayout id;
    TextView drawer_profile,drawer_callhistory,drawer_change_password,drawer_message,drawer_logout;
    TextView drawer_professional_myrequest,drawer_choose_file;

    TextView drawer_user_privacy_policy,drawer_prof_privacy_policy,drawer_user_terms_condtns;

    LinearLayout l1,l2,drawr;
    String color;
    SharedPreferences myshar;

    TextView change_password_professional,job_history_prof,message_prof,profile_professional;

    SharedPreferences get_id;
    public static String USER_ID,FIRSTNAME,LASTNAME,EMAIL,PHONENUM;

    public static ArrayList<String> countryListGlobal = new ArrayList<>();


    View drawer_profile_view,drawer_callhistory_view,drawer_change_password_view,change_password_professional_view,
            profile_professional_view,job_history_prof_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.serviceselectionpage);


     //   getCountryMaster();

        /*String savedEmail = Utils.getEmail(Serviceselectionpage.this);
        if (Utils.isValidStr(savedEmail))
            QuickHelpLog.e("SavedEmail",Utils.getEmail(Serviceselectionpage.this));*/

        drawer_user_privacy_policy=(TextView)findViewById(R.id.user_privacy);
        drawer_user_terms_condtns=(TextView)findViewById(R.id.uesr_terms_cond);
        drawer_user_terms_condtns.setText("TERMS & CONDITIONS");
        drawer_prof_privacy_policy=(TextView)findViewById(R.id.professional_privacy) ;

        l1=(LinearLayout) findViewById(R.id.mainPage1);
        l2=(LinearLayout) findViewById(R.id.main2);
        dlayout=(DrawerLayout) findViewById(R.id.drawer);
        id=(LinearLayout) findViewById(R.id.listLay);
        viewpage=(ViewPager)findViewById(R.id.viewpage);
        Ndrawer=(ImageView)findViewById(R.id.image);
        language=(ImageView)findViewById(R.id.imageV);
        //image_sample=(ImageView)findViewById(R.id.imageHide);
        signin=(TextView)findViewById(R.id.textView1) ;
        first_name=(TextView)findViewById(R.id.firstname);
        user=(View)findViewById(R.id.v1);
        professional=(View)findViewById(R.id.v2);
        users=(TextView)findViewById(R.id.t1);
        professionals=(TextView)findViewById(R.id.t2);
        drawer_message=(TextView)findViewById(R.id.message);
        drawr=(LinearLayout) findViewById(R.id.change);

        drawer_profile=(TextView) findViewById(R.id.profile);
        drawer_callhistory=(TextView)findViewById(R.id.callhistory);
        drawer_change_password=(TextView) findViewById(R.id.change_password);

        change_password_professional=(TextView) findViewById(R.id.change_password_prof);
        profile_professional=(TextView) findViewById(R.id.profile_prof);
        job_history_prof=(TextView) findViewById(R.id.job_prof);

        drawer_profile_view=(View) findViewById(R.id.profile_view);
        drawer_callhistory_view=(View)findViewById(R.id.callhistory_view);
        drawer_change_password_view=(View) findViewById(R.id.change_password_view);

        change_password_professional_view=(View) findViewById(R.id.change_password_prof_view);
        profile_professional_view=(View) findViewById(R.id.profile_prof_view);
        job_history_prof_view=(View) findViewById(R.id.job_prof_view);

        message_prof=(TextView) findViewById(R.id.message_profess);



        LinearLayout professionla_linear=(LinearLayout) findViewById(R.id.professional_linear);
        LinearLayout user_linear=(LinearLayout) findViewById(R.id.user_linear);

        drawer_logout=(TextView) findViewById(R.id.logout);
        drawer_professional_myrequest=(TextView)findViewById(R.id.myrequest);
        drawer_choose_file=(TextView)findViewById(R.id.uploadfile);
//drawer_profile_view=(View) findViewById(R.id.profile_view);
        myshar=getSharedPreferences("Default_Language",MODE_PRIVATE);
        color=myshar.getString("Value",null);

        get_id=getSharedPreferences("My_id",MODE_PRIVATE);
        USER_ID=get_id.getString("my_id",null);
        EMAIL=get_id.getString("email","");
        FIRSTNAME=get_id.getString("Firstname","");
        LASTNAME=get_id.getString("Lastname","");
        PHONENUM=get_id.getString("phone_num","");


        QuickHelpLog.e("ServiceSelectionPage", "User details : USer Id "+USER_ID+" Email :"+EMAIL+" First Name :"+FIRSTNAME+"Last Name :"+LASTNAME+"Phone :"+PHONENUM);
        String utilsEmail = Utils.getEmail(this);
        if (!Utils.isValidStr(EMAIL) && Utils.isValidStr(utilsEmail))
            EMAIL = Utils.getEmail(this);

        globol_array_city=new ArrayList<String>();

        /*if(color.equals("CHENNAI"))
        {
            language.setVisibility(View.INVISIBLE);
            l1.setBackgroundResource(R.color.app_heading2);
            l2.setBackgroundResource(R.color.app_heading2);
            id.setBackgroundResource(R.color.app_heading2);
        }*/

        if(get_id.getString("USER","").equals("true"))
        {
            user_linear.setVisibility(View.VISIBLE);
            professionla_linear.setVisibility(View.GONE);
        }

        if(get_id.getString("PROF","").equals("true"))
        {
            user_linear.setVisibility(View.GONE);
            professionla_linear.setVisibility(View.VISIBLE);
        }

        if(USER_ID!=null)
        {
            signin.setVisibility(View.GONE);
            first_name.setVisibility(View.VISIBLE);
            first_name.setText(FIRSTNAME);
            //image_sample.setVisibility(View.INVISIBLE);
            //drawer_profile.setVisibility(View.VISIBLE);
            //drawer_profile_view.setVisibility(View.VISIBLE);
            drawer_logout.setVisibility(View.VISIBLE);

            if(get_id.getString("USER","").equals("true"))
            {
                drawer_profile.setVisibility(View.VISIBLE);
                drawer_callhistory.setVisibility(View.VISIBLE);
                drawer_change_password.setVisibility(View.VISIBLE);

                drawer_profile_view.setVisibility(View.VISIBLE);
                drawer_callhistory_view.setVisibility(View.VISIBLE);
                drawer_change_password_view.setVisibility(View.VISIBLE);
                if (!Utils.isValidStr(FIRSTNAME) || !Utils.isValidStr(LASTNAME) || !Utils.isValidStr(EMAIL) || !Utils.isValidStr(PHONENUM)){
                    DialogManager.showConfirmPopup(this, new DialogMangerCallback() {
                        @Override
                        public void onOkClick(View view) {
                            Intent intent=new Intent(Serviceselectionpage.this,Drawerlayout_profile.class);
                            startActivity(intent);
                        }

                        @Override
                        public void onCancelClick(View view) {

                        }
                    }, getString(R.string.alert), getString(R.string.complete_profile_msg), getString(R.string.ok), getString(R.string.cancel));
                }

            }
            if(get_id.getString("PROF","").equals("true"))
            {
                change_password_professional.setVisibility(View.VISIBLE);
                profile_professional.setVisibility(View.VISIBLE);
                job_history_prof.setVisibility(View.VISIBLE);

                change_password_professional_view.setVisibility(View.VISIBLE);
                profile_professional_view.setVisibility(View.VISIBLE);
                job_history_prof_view.setVisibility(View.VISIBLE);
            }

        }



        users.setOnClickListener(this);
        professionals.setOnClickListener(this);
        signin.setOnClickListener(this);
        Ndrawer.setOnClickListener(this);
        drawer_profile.setOnClickListener(this);
        drawer_callhistory.setOnClickListener(this);
        drawer_change_password.setOnClickListener(this);

        change_password_professional.setOnClickListener(this);

        message_prof.setOnClickListener(this);
        drawer_message.setOnClickListener(this);
        drawer_logout.setOnClickListener(this);
        drawer_professional_myrequest.setOnClickListener(this);

        drawer_user_privacy_policy.setOnClickListener(this);
        drawer_prof_privacy_policy.setOnClickListener(this);
        drawer_user_terms_condtns.setOnClickListener(this);

        profile_professional.setOnClickListener(this);

        job_history_prof.setOnClickListener(this);

        drawer_choose_file.setOnClickListener(this);

        language.setOnClickListener(new Language_change(Serviceselectionpage.this,language));


        Servicelandingadapter adapter=new Servicelandingadapter(getSupportFragmentManager());
        viewpage.setAdapter(adapter);

        viewpage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if(viewpage.getCurrentItem()==0)
                {
                    user.setVisibility(View.VISIBLE);
                    professional.setVisibility(View.INVISIBLE);
                }
                else
                {
                    user.setVisibility(View.INVISIBLE);
                    professional.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.t1:
                viewpage.setCurrentItem(0);
                break;

            case R.id.t2:
                viewpage.setCurrentItem(1);
                break;
            case R.id.image:
                dlayout.openDrawer(id);
                break;
            case R.id.textView1:
                Intent n=new Intent(Serviceselectionpage.this, Sign_activity.class);
                startActivity(n);
                break;
            case R.id.profile:
              Intent intent=new Intent(Serviceselectionpage.this,Drawerlayout_profile.class);
                //intent.putExtra("unique_id","landing_page");
                startActivity(intent);

                break;
            case R.id.callhistory:
                Intent i=new Intent(Serviceselectionpage.this,Drawerlayout_callhistory.class);
                i.putExtra("getPage","default");
                startActivity(i);

                break;
            case R.id.change_password:
                Intent password=new Intent(Serviceselectionpage.this,Change_password.class);
                password.setAction("user");
                startActivity(password);

                break;
            case R.id.change_password_prof:
                Intent password_pro=new Intent(Serviceselectionpage.this,Change_password.class);
                password_pro.setAction("prof");
                startActivity(password_pro);

                break;
            case R.id.message:
                Intent msg=new Intent(Serviceselectionpage.this,Drawer_message.class);
                startActivity(msg);
                break;
            case R.id.message_profess:
                Intent msg_prof=new Intent(Serviceselectionpage.this,Drawer_message.class);
                startActivity(msg_prof);
                break;
            case R.id.logout:
                SharedPreferences.Editor sign_out=get_id.edit();
                sign_out.putString("my_id",null).
                        putString("email",null).putString("Firstname",null)
                        .putString("Lastname",null).putString("phone_num",null)
                        .putString("USER","false").putString("PROF","false").commit();

                onRestart();
                Toast.makeText(Serviceselectionpage.this,"Successfully SignOut",Toast.LENGTH_SHORT).show();
                //finish();
                break;


            case R.id.uploadfile:
                Intent upload_file=new Intent(Serviceselectionpage.this,Drawer_uploadfile.class);
                startActivity(upload_file);
                break;
            case R.id.myrequest:
                Intent myrequest=new Intent(Serviceselectionpage.this,Drawer_myrequest.class);
                startActivity(myrequest);
                break;
            case R.id.job_prof:
                Intent job_pro=new Intent(Serviceselectionpage.this,Professional_job_history.class);
                job_pro.putExtra("getPage","default");
                startActivity(job_pro);
                break;

            case R.id.profile_prof:
                Intent profile_prof=new Intent(Serviceselectionpage.this,My_profile_professional.class);
                //intent.putExtra("unique_id","landing_page");
                startActivity(profile_prof);

                break;

            case R.id.user_privacy:
                Intent user_privacy=new Intent(getApplicationContext(),Privacy_policy_user.class);
                startActivity(user_privacy);

                break;
            case R.id.uesr_terms_cond:
                Intent uesr_terms_cond=new Intent(getApplicationContext(),Privacy_policy_terms_conditions.class);
                startActivity(uesr_terms_cond);

                break;
            case R.id.professional_privacy:
                Intent professional_privacy=new Intent(getApplicationContext(),Privacy_policy_professionals.class);
                startActivity(professional_privacy);
                break;

            default:
                break;
        }
    }

    boolean exit=false;
    @Override
    public void onBackPressed() {
        if(dlayout.isDrawerOpen(id))
        {
            dlayout.closeDrawer(id);
        }
        else
        {
            if(exit)
            {
                finish();
            }
            else
            {

                Toast.makeText(this,"Please click back again to exit",Toast.LENGTH_SHORT).show();
                exit=true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        exit=false;
                    }
                }, 3000);


            }}
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    private void getCountryMaster() {
        if (countryListGlobal == null || countryListGlobal.size() <= 0)
        APIRequestHandler.getInstance().countryMaster(Serviceselectionpage.this, this);
    }


    @Override
    public void onRequestSuccess(Object responseObj) {

        if (responseObj != null) {
            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryListGlobal.clear();
                        countryListGlobal.add(getString(R.string.select_city));
                        countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(Serviceselectionpage.this, "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {
        if (responseObj != null) {
            if (responseObj instanceof CountryMasterModel) {
                DialogManager.showToast(Serviceselectionpage.this, "Something wrong happend with Country master Api call");
            }
        }
    }
}
