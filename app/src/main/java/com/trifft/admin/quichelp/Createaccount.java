package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import fragment_adapter.Createaccount_adapter;

/**
 * Created by Admin on 11/12/2016.
 */

public class Createaccount extends AppCompatActivity implements View.OnClickListener {

    ViewPager viewpage;
    TextView users, professionals;
    View user, professional;
    ImageView profile;
    SharedPreferences check;
    LinearLayout l3,l4,backArrow;
    String value;
    public static final String EXTRA_PROF_USER = "extraProfessionalUser";
    private boolean isProfUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createaccount);

        Intent intent = getIntent();
        if (intent != null){
            if (intent.getExtras() != null && intent.getExtras().containsKey(EXTRA_PROF_USER))
                isProfUser = intent.getBooleanExtra(EXTRA_PROF_USER, false);
        }

        viewpage = (ViewPager) findViewById(R.id.viewpage);
        users = (TextView) findViewById(R.id.t1);
        professionals = (TextView) findViewById(R.id.t2);
        l3=(LinearLayout) findViewById(R.id.createAcc);
        l4=(LinearLayout) findViewById(R.id.main2);
        backArrow=(LinearLayout) findViewById(R.id.imageArrow);
        profile=(ImageView) findViewById(R.id.imageV);
        user = (View) findViewById(R.id.v1);
        professional = (View) findViewById(R.id.v2);

        check=getSharedPreferences("Default_Language",MODE_PRIVATE);
        value=check.getString("Value","");

       /* if(value.equals("CHENNAI"))
        {
            profile.setVisibility(View.INVISIBLE);
            l3.setBackgroundResource(R.color.app_heading2);
            l4.setBackgroundResource(R.color.app_heading2);
        }*/


        professionals.setOnClickListener(this);
        users.setOnClickListener(this);
        user.setOnClickListener(this);
        professional.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        profile.setOnClickListener(new Language_change(Createaccount.this,profile));

        Createaccount_adapter adapter = new Createaccount_adapter(getSupportFragmentManager());
        viewpage.setAdapter(adapter);

        viewpage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (viewpage.getCurrentItem() == 0) {
                    user.setVisibility(View.VISIBLE);
                    professional.setVisibility(View.INVISIBLE);
                }
                else {
                    user.setVisibility(View.INVISIBLE);
                    professional.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (isProfUser)
            viewpage.setCurrentItem(1, true);
        else
            viewpage.setCurrentItem(0,true);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.t1:
                viewpage.setCurrentItem(0);
                break;
            case R.id.t2:
                viewpage.setCurrentItem(1);
                break;
            case R.id.imageArrow:
                onBackPressed();
                break;

        }
    }

}