package com.trifft.admin.quichelp.utils;


import com.trifft.admin.quichelp.BuildConfig;

/**
 * Created by fatima on 19-10-2019.
 */

public class AppConstants {

//    public static final String APP_URL = BuildConfig.APP_URL;
    public static final String SHARED_PREFERENCE = "PioneerFarmer";

    public static final String SERVER_DATE_FORMAT = "MM/dd/yyyy";

    public static final String USER_ID = "User_Id";
    public static final String COUPON = "coupon";

    public static String AppURL = BuildConfig.BASE_URL;


}