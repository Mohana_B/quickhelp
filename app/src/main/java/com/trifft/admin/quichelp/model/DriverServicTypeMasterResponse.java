package com.trifft.admin.quichelp.model;

import java.util.ArrayList;

public class DriverServicTypeMasterResponse extends CommonResponse{
    private ArrayList<IdNameModel> Domestic;
    private ArrayList<IdNameModel> Outstation;

    public ArrayList<IdNameModel> getDomestic() {
        return Domestic;
    }

    public void setDomestic(ArrayList<IdNameModel> domestic) {
        Domestic = domestic;
    }

    public ArrayList<IdNameModel> getOutstation() {
        return Outstation;
    }

    public void setOutstation(ArrayList<IdNameModel> outstation) {
        Outstation = outstation;
    }
}
