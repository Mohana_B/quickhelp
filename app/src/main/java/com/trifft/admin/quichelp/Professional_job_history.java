package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import fragment_adapter.Professional_history_swipe_adapter;

/**
 * Created by aduser on 12/12/2017.
 */

public class Professional_job_history extends AppCompatActivity implements View.OnClickListener{

    Professional_history_swipe_adapter swipe;
    TextView upcoming,history;
    View upcomingview,historyview;
    ViewPager pager;

    TextView tSignin;
    ImageView language;
    LinearLayout backArrow;

    String getPage;
    // public static String USER_ID,EMAIL,FIRSTNAME,LASTNAME;
    //SharedPreferences get_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_call_history);

        upcomingview=(View)findViewById(R.id.v3);
        historyview=(View)findViewById(R.id.v4);
        upcoming=(TextView)findViewById(R.id.upcoming);
        history=(TextView)findViewById(R.id.history);
        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        language=(ImageView) findViewById(R.id.language);
        pager=(ViewPager)findViewById(R.id.pager);

        /*get_id=getSharedPreferences("My_id",MODE_PRIVATE);
        USER_ID=get_id.getString("my_id",null);
        EMAIL=get_id.getString("email",null);
        FIRSTNAME=get_id.getString("Firstname",null);
        LASTNAME=get_id.getString("Lastname",null);*/



        swipe=new Professional_history_swipe_adapter(getSupportFragmentManager());

        pager.setAdapter(swipe);

        getPage=getIntent().getExtras().getString("getPage");

        if(getPage.equals("upcoming"))
        {
            pager.setCurrentItem(0);
        }
        else if(getPage.equals("history"))
        {
            pager.setCurrentItem(1);
        }
        else
        {
            pager.setCurrentItem(0);
        }





        history.setOnClickListener(this);
        upcoming.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Professional_job_history.this,language));

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(pager.getCurrentItem()==0)
                {
                    upcomingview.setVisibility(View.VISIBLE);
                    historyview.setVisibility(View.INVISIBLE);
                }
                else
                {
                    upcomingview.setVisibility(View.INVISIBLE);
                    historyview.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.upcoming:
                pager.setCurrentItem(0);
                break;
            case R.id.history:
                pager.setCurrentItem(1);
                break;
            case R.id.imageArrow:

                onBackPressed();

                break;

        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


}
