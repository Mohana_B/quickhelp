package com.trifft.admin.quichelp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.trifft.admin.quichelp.Getprice_page;
import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CheckDiscountAvailablResponse;
import com.trifft.admin.quichelp.model.DiscountantResponse;
import com.trifft.admin.quichelp.utils.AppConstants;
import com.trifft.admin.quichelp.utils.Utils;

public class DiscountInfoActivity extends BaseActivity implements CommonInterface, View.OnClickListener {
    private DiscountInfoActivity thisActivity;

    public static final String EXTRA_JOB_ID = "extraJobId";
    public static final String EXTRA_PRICE = "extraPrice";
    private String extraJobId, extraPrice;
    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    private Dialog mDialog;
    private CheckDiscountAvailablResponse chkDisResponse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_info);
        thisActivity = this;

        /*progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();*/

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            if (intent.getExtras().containsKey(EXTRA_JOB_ID))
                extraJobId = intent.getStringExtra(EXTRA_JOB_ID);

            if (intent.getExtras().containsKey(EXTRA_PRICE))
                extraPrice = intent.getStringExtra(EXTRA_PRICE);

        }

        // Check discount Available
        APIRequestHandler.getInstance().checkDiscountAvailable("rebate_data", USER_ID, thisActivity, this);
    }

    @Override
    public void onRequestSuccess(Object responseObj) {

        if (responseObj != null) {
            if (responseObj instanceof CheckDiscountAvailablResponse) {
                CheckDiscountAvailablResponse response = (CheckDiscountAvailablResponse) responseObj;
                if (response.isStatus()) {
                    if (response.getRebate_data() != null) {
                        CheckDiscountAvailablResponse.ActualResponse actualResponse = response.getRebate_data();
                        if (Utils.isValidStr(actualResponse.getCode())) {
                            chkDisResponse = response;
                            // discount available, show discount popup, on apply code make api call.
                            showDiscAvailablePopup(response);
                        } else
                            gotoPriceScreen();
                    } else {
                        gotoPriceScreen();
                    }

                } else {
                    gotoPriceScreen();
                }
            } else if (responseObj instanceof DiscountantResponse) {
                DiscountantResponse response = (DiscountantResponse) responseObj;
                if (response.isStatus()) {
                    // discount applied successfully, take discount price and navigate to pirce screen
                    Intent intent = new Intent(thisActivity, Getprice_page.class);
                    intent.putExtra("job_id", extraJobId);
                    intent.putExtra("price", response.getAfter_discount());
                    startActivity(intent);
                } else {
                    gotoPriceScreen();
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {
        if (responseObj != null) {
            if (responseObj instanceof CheckDiscountAvailablResponse) {
                gotoPriceScreen();
            } else if (responseObj instanceof DiscountantResponse) {
                gotoPriceScreen();
            }
        }
    }

    private void gotoPriceScreen() {
        Intent intent = new Intent(thisActivity, Getprice_page.class);
        intent.putExtra("job_id", extraJobId);
        intent.putExtra("price", extraPrice);
        startActivity(intent);
    }

    private void showDiscAvailablePopup(CheckDiscountAvailablResponse response) {

        mDialog = new Dialog(thisActivity);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.popup_discount_available);

        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);

        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);

        TextView headerText, firstTv, secondTv, thirdTv;
        Button paymentBtn, discountBtn;

        headerText = mDialog.findViewById(R.id.popup_headerTv);
        firstTv = mDialog.findViewById(R.id.popup_firstTv);
        secondTv = mDialog.findViewById(R.id.popup_secondTv);
        thirdTv = mDialog.findViewById(R.id.popup_thirdTv);
        paymentBtn = mDialog.findViewById(R.id.popup_leftBtn);
        discountBtn = mDialog.findViewById(R.id.popup_rightBtn);

        headerText.setText(response.getMessage());
        String st_first = "Job Id : " + extraJobId + "  " + "Total Amount : " + extraPrice;
        firstTv.setText(st_first);
        String st_second = "You are eligible for " + response.getRebate_data().getDiscount() + " discount";

        SpannableString spannableString = new SpannableString(st_second);
        spannableString.setSpan(new ForegroundColorSpan(Color.GREEN), 0, st_second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, st_second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(2f), 0, st_second.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        secondTv.setText(spannableString);

        paymentBtn.setText(R.string.paynow);
        discountBtn.setText(R.string.click_for_discount);
        paymentBtn.setOnClickListener(this);
        discountBtn.setOnClickListener(this);

        mDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //Payment btn
            case R.id.popup_leftBtn:
                if (mDialog != null)
                    mDialog.dismiss();
                gotoPriceScreen();
                break;
            // discount Btn
            case R.id.popup_rightBtn:
                if (mDialog != null)
                    mDialog.dismiss();
                // make api call to get discounted price
                if (chkDisResponse != null)
                    APIRequestHandler.getInstance().getDiscountInfo("get_discount", USER_ID, extraJobId, extraPrice, chkDisResponse.getRebate_data().getCode(), AppConstants.COUPON, thisActivity, this);
                break;
        }
    }
}
