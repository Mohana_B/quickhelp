package com.trifft.admin.quichelp;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by PSDeveloper on 12/30/2016.
 */

public class Popup_cleaning_hours implements View.OnClickListener {




    TextView select_hours,close;
    Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b10;
    Context context;
    EditText hours;
    LinearLayout j;

    public Popup_cleaning_hours(Context con, EditText cleaning_hourd) {
        context=con;
        hours=cleaning_hourd;


    }

    String val;
    @Override
    public void onClick(View view) {




        LayoutInflater in=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_10_rooms,null);

        final Dialog dialog=new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);



        dialog.setContentView(vv);
        dialog.show();
        select_hours=(TextView) vv.findViewById(R.id.hd1);
        close=(TextView) vv.findViewById(R.id.btn_close);
        b1 = (Button)vv.findViewById(R.id.button1);
        b2 = (Button)vv.findViewById(R.id.button2);
        b3 = (Button)vv.findViewById(R.id.button3);
        b4 = (Button)vv.findViewById(R.id.button4);
        b5 = (Button)vv.findViewById(R.id.button5);
        b6 = (Button)vv.findViewById(R.id.button6);
        b7 = (Button)vv.findViewById(R.id.button7);
        b8 = (Button)vv.findViewById(R.id.button8);
        b9 = (Button)vv.findViewById(R.id.button9);
        b10 = (Button)vv.findViewById(R.id.button10);

        select_hours.setText("SELECT HOURS");

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b1.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b2.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b3.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b4.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b5.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b6.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b7.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b8.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b9.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val=b10.getText().toString();
                hours.setText(val+" hours");
                dialog.dismiss();
            }
        });






    }
}
