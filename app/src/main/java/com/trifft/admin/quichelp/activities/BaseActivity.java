package com.trifft.admin.quichelp.activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.utils.DialogManager;

import java.net.SocketTimeoutException;

public class BaseActivity extends AppCompatActivity implements CommonInterface {

    public static void showHomePage(FragmentActivity activity) {
        activity.finish();
        Intent home = new Intent(activity, Serviceselectionpage.class);
        activity.startActivity(home);
    }

    @Override
    public void onRequestSuccess(Object responseObj) {

    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {
        try {
            if (errorCode.getCause() instanceof SocketTimeoutException) {
                DialogManager.showToast(this, getString(R.string.connection_timeout));
            } else {
                DialogManager.showToast(this, getString(R.string.no_internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
