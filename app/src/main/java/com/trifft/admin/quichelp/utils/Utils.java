package com.trifft.admin.quichelp.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.trifft.admin.quichelp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by fatima on 19-10-2019.
 */
public class Utils {
    private static final String TAG = Utils.class.getName();
    public static Dialog progress;
    public static Dialog mDialog;

    public static void setDeviceId(String deviceId, Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        SharedPreferences.Editor editor = example.edit();
        editor.putString("deviceId", deviceId);
        editor.apply();
    }

    public static String getDeviceId(Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        String deviceId = example.getString("deviceId", null);
        if (deviceId == null) {
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            setDeviceId(deviceId, context);
        }
        return deviceId;
    }


    public static boolean isNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private static Dialog getLoadingprogress(Context mContext) {
        mDialog = getDialog(mContext, R.layout.progress);
        // ProgressBar mProgressBar = (ProgressBar)
        // mDialog.findViewById(R.id.progressBar);
        mDialog.setCancelable(false);

        return mDialog;
    }

    public static Dialog getDialog(Context mContext, int mLayout) {
        Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(mLayout);
        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);

        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(false);

        return mDialog;
    }

    public static boolean isNumeric(String value) {
        try {
            Double.valueOf(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidStr(String string) {
        return string != null && !string.trim().isEmpty() && !string.equalsIgnoreCase("null");
    }

    public static void setUserFirstName(String firstName, Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        SharedPreferences.Editor editor = example.edit();
        editor.putString("firstName", firstName);
        editor.apply();
    }

    public static String getUserFirstName(Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        return example.getString("firstName", null);
    }

    public static void setUserLastName(String lirstName, Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        SharedPreferences.Editor editor = example.edit();
        editor.putString("lirstName", lirstName);
        editor.apply();
    }

    public static String getUserLastName(Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        return example.getString("lirstName", null);
    }

    public static String getEmail(Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        return example.getString("email", null);
    }

    public static void setEmail(String email, Context context) {
        SharedPreferences example = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, 0);
        SharedPreferences.Editor editor = example.edit();
        editor.putString("email", email);
        editor.apply();
    }
    public static String getVersionName(Context context) {
        PackageInfo pInfo;
        String code = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            code = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return code;

    }

    public static int getVersionCode(Context context) {
        PackageInfo pInfo;
        int code = 0;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            code = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return code;

    }

    public static String getCurrentTimeAndDate(Context context) {
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date;
        date = simpledateformat.format(calander.getTime());
        return date;
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
        String date;
        date = simpledateformat.format(calendar.getTime());
        return date;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    public static String formatDateToServer(String str) {
        String date = "";
        SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date1 = myDateFormat.parse(str);
            SimpleDateFormat myDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
            date = myDateFormat2.format(date1);
        } catch (ParseException e) {

        }
        return date;

    }

    public static String formatTime24To12(String inTime) {
        String outTime = null;
        SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
        try {
            outTime = inFormat.format(outFormat.parse(inTime));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return outTime;
    }
}