package com.trifft.admin.quichelp;


import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by aduser on 2/10/2018.
 */

public class Privacy_policy_professionals extends AppCompatActivity implements View.OnClickListener {

    LinearLayout backArrow;

    WebView content;

    TextView header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_privacy_policy);

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);

        content = (WebView) findViewById(R.id.content);

        header=(TextView) findViewById(R.id.header);

        header.setText("Policies");

        String text = "<html>" +
                "<head><body>"
                + "<center><h1 style=\"color:#005a8c\">Privacy Policy</h1></center>\n" +
                "<p style=\"text-align:justify\">Your privacy is important to us. This Policy outlines how your Personal Data will be managed in accordance with the concerned Data Protection Act which strives to protect personal data of individuals. Please take a moment to read this Policy so that you know and understand the purposes for which we may collect, use and/or disclose your Personal Data. You agree and consent to Quichelp,    or any of our group of companies (referred to herein as \"Quichelp\" , \"our website\",  \"App\", \"us\", \"we\", \"our\" or \"ours\"). This Privacy Policy supplements but does not supersede nor replace any other consent which you may have previously provided to us nor does it affect any rights that we may have at law in connection with the collection, use and/or disclosure of your Personal Data. We may from time to time update this Privacy Policy to ensure that the Data Protection is consistent with our future developments, industry trends and/or any changes in legal or regulatory requirements. For the avoidance of doubt, this Privacy Policy forms a part of the terms and conditions governing your relationship with us and should be read in conjunction with such terms and conditions.</p>\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">1  Your Personal Data</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "In this Privacy  Policy, 'Personal Data' refers to any data or information about you from which you can be identifi ed either (a) from that data; or (b) from that data and other information to which we have or are likely to have access. Examples of such Personal Data include: (a) your name, PAN number or other Aadhar Card number, telephone number(s), mailing address, email address and any other information relating to you which you have provided us in any forms you may have submitted to us, or in other forms of interaction with you; (b) information about your use of our website and services, including cookies, IP address, policy and claims history information; (c) your employment history, education background, and income levels; (d) your transaction related information, such as your bank account or credit & debit card information, and your credit history; and (e) information about your usage of and interaction with our website and/or services including computer and connection information, device capability, bandwidth, statistics on page views, and traffic to and from our website.\n" +
                "</p>\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">2  Collection of Personal Data</h2>\n" +
                "<p style=\"text-align:justify\">2.1 Generally,  Quichelp may collect your Personal Data in the following ways:</p>\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(a)\t when you register with us for an account;</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(b)\t when you access our website or creates a profile; </li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(c)\t when you interact with any of our representatives ;</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(d)\t when you respond to our request for additional Personal Data; </li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(e)\t when you ask to be included in an email or other mailing list;</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(f)\t when you request that we contact you;</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(g)\t when you respond to our initiatives or promotions</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(h)\t when you submit your Personal Data to us for any other reason. </li> \n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">3 Purposes for the Collection, Use and Disclosure of Your Personal Data</h2>\n" +
                "<p style=\"text-align:justify\">In this subsection you will find the details of why we collect your Personal Information, What will we do with the information, Terms pertaining to Disclosure of your personal data with us.</p>\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(i)\tevaluating and providing advice and/or recommendations to you regarding the type of  services suited to your needs</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(j)\t Assessing and processing any applications or requests made by you for services offered by Quichelp.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(k)\tcommunicating with you to inform you of changes and updates to our policies, terms and conditions and other administrative information, including without limitation for the purposes of serving you in relation services offered to you</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(l)\t processing and administering benefits or entitlements in connection with our services which you have applied for, including the administration of loyalty and rewards programs  if any.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(m)\t Verification of your identity for the purpose of providing you with our services.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(n)\tresponding to your queries and requests and handling complaints</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(o)\t Providing you with personalized service.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(p)\tConducting market research for statistical profiling and other purposes to understand and determine customer preferences and demographics in order for us to review, develop and improve the products and services which we are providing to you (including without limitation to ensure that the products and services offered are relevant to you).</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(q)\tmanaging our infrastructure and business operations and complying with internal policies and procedures</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(r)\t archiving of documents and records in both electronic and physical form for record keeping purposes</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(s)\t maintaining records of customer instructions, whether through phone recordings, hard copy documents, soft copy documents or instructions given via electronic or other means</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(t)\tFacilitating the verification and checks of your Personal Data in order to provide you with our products and services which you have requested</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(u)\t preventing, detecting, assisting and investigating crime, including fraud of any form </li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(v)\t compliance with any applicable local or foreign statute, rule, law, regulation, judgment, decree, directive, code of practice, guideline, administrative requirement, sanctions regime, court order, agreement between Quichelp  and an Competent Authority, agreement or treaty between Authorities, international guidance and internal policies or procedures, which may apply to Quichelp or which any such company is subject to, or to assist in or with law enforcement and investigations by any Authority or to comply with any request from an Authority</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(w)\t subject to applicable law, any other purpose set out in the Terms and Conditions. You should ensure that all Personal Data submitted to us is complete, accurate, true and correct. Failure on your part to do so may result in our inability to provide you with products and services you have requested. Where personal data is submitted by you on behalf of another individual or concerns another individual other than yourself (or, in the case of situations where you, as a representative of your company or organization, are submitting the personal data of individuals as part of the disclosures by the company or organization to us), you represent and warrant to us that all the necessary consents (procured in accordance with all applicable data protection legislation, including without limitation the PDPA, for such purposes stated in the relevant sections of this Data Protection Policy) have been obtained from the relevant individuals and that you have retained proof of these consents.</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(x)\tQuichelp may collect, use and/or disclose your Personal Data for the following purposes, depending on the nature of our relationship with you as well as the type of business you hold with us:</li>\n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">4  Security and confidentiality</h2>\n" +
                "<p style=\"text-align:justify\">Quichelp safeguards the security of the personal information you provide to Us with physical, electronic and managerial procedures. We use industry-standard Secure Sockets Layer (SSL) encryption on all pages where we collect personal information.</p> \n" +
                "<p style=\"text-align:justify\">Any personal information you provide will be held securely and your personal information will not be sold or traded to third parties. In some circumstances we may need to disclose your personal information </p>\n" +
                "<ul style=\"list-style: none;\">\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(i)\tto a third party to provide a service you have requested</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(ii)\tto fulfil a request for information</li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(iii)\tto comply with a legal requirement or request from a competent court, regulator or other authority, or </li>\n" +
                "<li style=\"text-align:justify;padding-bottom:5px\">(iv)\tif We believe that there has been a violation of the Terms, of Our rights or the rights of any third party. Any information about you that We pass to a third party service provider will be held securely by that party and used only to provide the services or information you have requested.</li>\n" +
                "</ul>\n" +
                "\n" +
                "\n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">5  Internet based data transfers</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "Given that the Internet is a global environment, using the Internet to collect and process personal information necessarily involves the transmission of data on an international basis. All data collected are processed and stored in Secured Servers.   However, if we process your personal information on servers or use third party service providers, we will endeavor to ensure that your personal information is afforded the highest level of protection. By visiting the Website and communicating electronically with us, you acknowledge our processing of personal information in this way.</p> \n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">6  Data Retention</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "We reserves the right to store your Personal data such Username, email id, messages, phone numbers,  Payment details, IP addresses, or any such data that deemed to be needful. We always store Personal data in a secure encrypted format.</p> \n" +
                "\n" +
                "<h2 style=\"color:#005a8c\">8  Changes to this Privacy Policy</h2>\n" +
                "<p style=\"text-align:justify\">\n" +
                "If this Privacy Policy changes in any way, we will place an updated version on this webpage. If you do not agree with the changes you may please discontinue using the Website. Regularly viewing the website ensures you are always aware of our Privacy Policy and you agree to it.</p> "
                +
                "</body></html>";

        /*content.loadData(text, "text/html; charset=UTF-8", null);

        final WebSettings webSettings = content.getSettings();


        */
        content.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);
        WebSettings settings = content.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        Resources res = getResources();
        settings.setDefaultFontSize((int) 17);
        backArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.imageArrow:
                onBackPressed();

                break;
        }

    }
}
