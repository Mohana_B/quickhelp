package com.trifft.admin.quichelp.apiInterfaces;

/**
 * Created by Fatima.t on 29/4/2020.
 */
public interface CommonInterface {
    public void onRequestSuccess(Object responseObj);
    public void onRequestFailure(Object responseObj, Throwable errorCode);
}
