package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by PSDeveloper on 11/22/2016.
 */

public class Facebook_sample extends AppCompatActivity {


    TextView t1, t2, t3, t4, t5;
    Sign_user user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook_sample);

        t1 = (TextView) findViewById(R.id.textView1);
        t2 = (TextView) findViewById(R.id.textView2);
        t3 = (TextView) findViewById(R.id.textView3);
        t4 = (TextView) findViewById(R.id.textView4);
        t5 = (TextView) findViewById(R.id.textView5);

        String name = getIntent().getExtras().getString("name");
        String id = getIntent().getExtras().getString("id");
        String gender = getIntent().getExtras().getString("gender");
        String email = getIntent().getExtras().getString("email");

        user = new Sign_user();

        t2.setText(name);
        t3.setText(email);
        t4.setText(gender);
        t5.setText(id);

    }

    boolean exit = false;

    @Override
    public void onBackPressed() {
    super.onBackPressed();

        user.logout();

    }

}

