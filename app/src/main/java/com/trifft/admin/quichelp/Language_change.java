package com.trifft.admin.quichelp;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.Locale;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Language_change implements View.OnClickListener {

    TextView english,german;
   Context context;
    ImageView dropdown;
    String local;

    public Language_change(Context c, ImageView drop) {
        this.context=c;
        dropdown=drop;

    }
int y;

    @Override
    public void onClick(View view) {

        LayoutInflater in=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_profile,null);

        int width= context.getResources().getDimensionPixelSize(R.dimen.popup);
        int gravity= context.getResources().getDimensionPixelSize(R.dimen.popup_location);
        final PopupWindow pop=new PopupWindow(vv,width,ViewGroup.LayoutParams.WRAP_CONTENT,true);

       english=(TextView)vv.findViewById(R.id.english);
        german=(TextView)vv.findViewById(R.id.german);
        pop.setOutsideTouchable(true);
        pop.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edittext));
        pop.setFocusable(true);
        pop.showAsDropDown(dropdown,gravity,5);



        local=Locale.getDefault().getDisplayLanguage();

        Log.e("language",local);
        if(local.equals("English")||local.equals("en-us"))
        {
            english.setTextColor(Color.WHITE);
            english.setBackgroundResource(R.color.app_heading);
        }
        else if(local.equals("Deutsch"))
        {
            german.setTextColor(Color.WHITE);
            german.setBackgroundResource(R.color.app_heading);
        }


        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(local.equals("English")||local.equals("en-us"))
             {
                 pop.dismiss();
             }
                else
                {
                    pop.dismiss();
                    String language="en-US";
                    Locale loca=new Locale(language);
                    Locale.setDefault(loca);
                    Configuration confi=new Configuration();
                    confi.locale=loca;
                    context.getResources().updateConfiguration(confi,context.getResources().getDisplayMetrics());

                    //((Activity)context).recreate();
                    ((Activity)context).finish();
                    ((Activity)context).startActivity(((Activity) context).getIntent());
                }


            }});
        german.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pop.dismiss();

             /*   german.setBackgroundResource(R.color.app_heading);
                german.setTextColor(Color.WHITE);*/

                if(local.equals("Deutsch"))
                {
                    pop.dismiss();
                }
                else
                {
                    pop.dismiss();
                    String language="de";
                    Locale local=new Locale(language);
                    Locale.setDefault(local);
                    Configuration config=new Configuration();
                    config.locale=local;
                    context.getResources().updateConfiguration(config,context.getResources().getDisplayMetrics());

                    ((Activity)context).finish();
                    // ((Activity)context).recreate();
                    ((Activity)context).startActivity(((Activity) context).getIntent());
                }



    }

    });



}



}