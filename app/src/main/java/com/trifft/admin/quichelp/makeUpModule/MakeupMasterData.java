package com.trifft.admin.quichelp.makeUpModule;

import com.trifft.admin.quichelp.model.IdNameModel;

import java.util.ArrayList;

public class MakeupMasterData {
    private ArrayList<IdNameModel> Makeup;
    private ArrayList<IdNameModel> Spa;

    public ArrayList<IdNameModel> getMakeup() {
        return Makeup;
    }

    public void setMakeup(ArrayList<IdNameModel> makeup) {
        Makeup = makeup;
    }

    public ArrayList<IdNameModel> getSpa() {
        return Spa;
    }

    public void setSpa(ArrayList<IdNameModel> spa) {
        Spa = spa;
    }
}
