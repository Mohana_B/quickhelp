package com.trifft.admin.quichelp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.model.IdNameModel;

import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Created by Fatima.t on 03/25/2020.
 */

public class DropDownSelectionIdNameModel implements View.OnClickListener {

    Context cn;
    EditText edit;
    SharedPreferences checking_language;
    ProgressDialog progressDialog;
    SharedPreferences country_change;

    ListView list;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<IdNameModel> arrayList;
    ArrayList<String> arrayListString = new ArrayList<>();

    public DropDownSelectionIdNameModel(Context activity, EditText select_country, ArrayList<IdNameModel> dataList) {
        cn=activity;
        edit=select_country;
        arrayList = dataList;
    }

    public DropDownSelectionIdNameModel(ArrayList<IdNameModel> updatedList){
        arrayList = updatedList;
        arrayListString.clear();
        for (IdNameModel model : arrayList) {
            if (Utils.isValidStr(model.getName()))
                arrayListString.add(model.getName());
            else
                arrayListString.add(model.getCategory_name());
        }

        if (arrayAdapter != null)
            arrayAdapter.notifyDataSetChanged();
    }
    String local;
    @Override
    public void onClick(View view) {

        LayoutInflater in=(LayoutInflater)cn.getSystemService(cn.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_selectcountry,null);
        list=(ListView)vv.findViewById(R.id.list);

        final PopupWindow pop=new PopupWindow(vv,edit.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT,true);
        pop.setOutsideTouchable(true);
        pop.setBackgroundDrawable(new ColorDrawable(cn.getResources().getColor(R.color.white)));
        pop.setFocusable(true);
        pop.showAsDropDown(edit);

        checking_language=cn.getSharedPreferences("Default_Language",cn.MODE_PRIVATE);

        local=checking_language.getString("Value",null);

        country_change=cn.getSharedPreferences("Default_Language",cn.MODE_PRIVATE);
        final SharedPreferences.Editor edi=country_change.edit();

        if(arrayList.size() > 0)
        {
            // check Myapplication.glogol_city has duplicates or not
            /*
             * convert array to list and then add all
             * elements to LinkedHashSet. LinkedHashSet
             * will automatically remove all duplicate elements.
             */
            LinkedHashSet<IdNameModel> lhSetColors = new LinkedHashSet<IdNameModel>(arrayList);

            arrayList.clear();
            arrayList.addAll(lhSetColors);

            arrayListString.clear();
            for (IdNameModel model : arrayList) {
                if (Utils.isValidStr(model.getName()))
                    arrayListString.add(model.getName());
                else
                    arrayListString.add(model.getCategory_name());
            }

            arrayAdapter=new ArrayAdapter<String>(cn,android.R.layout.simple_spinner_dropdown_item, /*arrayList*/ arrayListString);

            list.setAdapter(arrayAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    edit.setTag(arrayList.get(position));

                    if (Utils.isValidStr(arrayList.get(position).getName()))
                        edit.setText(arrayList.get(position).getName());
                    else
                        edit.setText(arrayList.get(position).getCategory_name());

                    pop.dismiss();
                }
            });
        }
    }
}
