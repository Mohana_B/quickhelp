package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/10/2016.
 */

public class Paintingbooknow extends Fragment implements AdapterView.OnItemSelectedListener, CommonInterface {


    SharedPreferences check;
    String value;
    Button getaprice;
    ProgressDialog progressDialog;
    String first_name, mobileNumber, last_name, app_area, selectRooms, email, date_data, time_data, address_1, address_2, special_comments, typeofService, selectedCity, Zipcode;

    EditText firstname, lastname, select_rooms, approx_area, date, time, /*select_city,*/
            specialComments, address1, address2,
            zip_code, mobile_number;
    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    RadioButton personal, commercial;
    SharedPreferences my_id;
    RelativeLayout kk;


    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    private EditText email_id;

    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.paintingbooknow, container, false);
        getaprice = (Button) v.findViewById(R.id.getaprice);
        select_rooms = (EditText) v.findViewById(R.id.selectrooms);
        approx_area = (EditText) v.findViewById(R.id.approxarea);
        kk = (RelativeLayout) v.findViewById(R.id.head);
        mobile_number = (EditText) v.findViewById(R.id.phone_num);
        date = (EditText) v.findViewById(R.id.date);
//        select_city = (EditText) v.findViewById(R.id.selectcity);
        spnCountry = v.findViewById(R.id.pnt_spn_country);
        time = (EditText) v.findViewById(R.id.time);
        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        email_id = (EditText) v.findViewById(R.id.email);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);

        personal = (RadioButton) v.findViewById(R.id.personal);
        commercial = (RadioButton) v.findViewById(R.id.commercial);


        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        my_id = getActivity().getSharedPreferences("My_id", getActivity().MODE_PRIVATE);
        USER_ID = my_id.getString("my_id", null);

        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            email_id.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);

   /*     if(value.equals("CHENNAI"))
        {
            getaprice.setBackgroundResource(R.drawable.ed_button2);
            select_city.setText("CHENNAI");

        }
        else{select_city.setText("MUMBAI");}*/
        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);


        select_rooms.setOnClickListener(new Popup_Ten_room(getActivity(), select_rooms, "rooms"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));
        //select_city.setOnClickListener(new Select_country(getActivity(),select_city));

        livingroom_textchange_listner();


        getaprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email = email_id.getText().toString();
                date_data = date.getText().toString();
                selectRooms = select_rooms.getText().toString();
                time_data = time.getText().toString();
                mobileNumber = mobile_number.getText().toString();
                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();
                app_area = approx_area.getText().toString();
//                selectCity = select_city.getText().toString();
                selectedCity = spnCountry.getSelectedItem().toString();
                Zipcode = zip_code.getText().toString();
                special_comments = specialComments.getText().toString();


                if (first_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (last_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (email.length() == 0 || email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (!personal.isChecked() && !commercial.isChecked()) {
                    Toast.makeText(getActivity(), "Select the service type", Toast.LENGTH_SHORT).show();
                } else if (selectRooms.equals("")) {
                    Toast.makeText(getActivity(), "Select the Rooms", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(mobileNumber)) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (date_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                } else if (time_data.equals("")) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (Zipcode.length() == 0) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                } else if (Zipcode.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                } else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (personal.isChecked()) {
                        typeofService = "personal";
                    }

                    if (commercial.isChecked()) {
                        typeofService = "commercial";
                    }
                    new Mytask().execute();
                }


            }
        });

        return v;
    }


    class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", jod_id = "", message = "", price = "";
        boolean status1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Paint_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();


            /*https://quichelp.com/json/Paint_Json/data/?service=insert_paint_job&
            userid=191&
            firstname=Shrikar&
            lastname=kodamgulawar&
            email=k.shrikar@gmail.com&
            phone=1234567899&
            frm_city=Chennai&
            frm_zip=1212111&
            service_type=personal&
            no_rooms=2&
            area=1000&
            pref_time=18:29&
            frm_address1=Hyderabad&
            sp_comments=wewewe&
            frm_address2=dsdsd&
            pref_date=16-03-2020*/
            URI uri = null;

            String email_st = "";
            if (EMAIL_ID != null && !EMAIL_ID.equals(""))
                email_st = EMAIL_ID;
            else if (email != null && !email.equals(""))
                email_st = email;

            try {
                uri = new URI("https", path + "service=" + "insert_paint_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&email=" + email_st +
                        "&frm_city=" + selectedCity +
                        "&frm_zip=" + Zipcode +
                        "&phone=" + mobileNumber +
                        "&service_type=" + typeofService +
                        "&no_rooms=" + selectRooms +
                        "&area=" + app_area +
                        "&pref_time=" + time_data +
                        "&frm_address1=" + address_1 +
                        "&sp_comments=" + special_comments +
                        "&lastname=" + last_name +
                        "&frm_address2=" + address_2 +
                        "&pref_date=" + date_data, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status1 = object.getBoolean("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("status", status);

            if (status1 == true) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status1 == true) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void livingroom_textchange_listner() {

   /*     select_rooms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                combo.setVisibility(View.INVISIBLE);
                close.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }
}
