package com.trifft.admin.quichelp;


import android.app.TimePickerDialog;
import android.content.Context;


import android.view.View;
import android.view.Window;
import android.widget.EditText;

import java.util.Calendar;

/**
 * Created by PSDeveloper on 11/16/2016.
 */

public class TimePicker implements View.OnClickListener {

    Context cn;
    EditText edit;

    public TimePicker(Context activity, EditText time) {

        cn = activity;
        edit = time;

    }

    String am_pm;

    @Override
    public void onClick(View view) {


        Calendar myTime = Calendar.getInstance();
        final int hour = myTime.get(Calendar.HOUR_OF_DAY);
        final int time = myTime.get(Calendar.MINUTE);
        // final int am=myTime.get(Calendar.AM_PM);

        TimePickerDialog mTimePicker = new TimePickerDialog(cn, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(android.widget.TimePicker timePicker, int i, int i1) {
               /* if (i > 12) {
                    //i -= 12;
                    am_pm = "PM";
                } else {
                    am_pm = "AM";
                }*/

                edit.setText(i + ":" + i1);
                //edit.setText(i + ":" + i1 + " " + am_pm);

            }

        }, hour, time, true);//Yes 24 hour time
        mTimePicker.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mTimePicker.show();


    }
}
