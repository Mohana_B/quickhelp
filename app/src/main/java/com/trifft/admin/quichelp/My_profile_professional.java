package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by aduser on 12/13/2017.
 */

public class My_profile_professional extends AppCompatActivity implements View.OnClickListener {

    ImageView imPro;
    LinearLayout heading;
    SharedPreferences check;
    String value;
    Button update;
    LinearLayout backArrow;

    String USER_ID= Serviceselectionpage.USER_ID;

    String str_first_name,str_last_name;
    String str_address_1,str_address_2,str_selectCity,str_state,select_Country,str_zip_code,str_mobile_num;

    EditText firstname,lastname,email;
    EditText address1,address2,selectCountry,selectCity,selectState,zipcode,mobileNumber;
    ProgressDialog progressDialog;
    private String str_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile_professional);

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        imPro = (ImageView) findViewById(R.id.imageSignin);
        heading=(LinearLayout) findViewById(R.id.toolbarSingn);
        update=(Button) findViewById(R.id.update);

        firstname=(EditText) findViewById(R.id.firstname);
        lastname=(EditText) findViewById(R.id.lastname);
        email=(EditText) findViewById(R.id.email);
        address1=(EditText) findViewById(R.id.address1);
        address2=(EditText) findViewById(R.id.address2);
        selectCity=(EditText) findViewById(R.id.selectcity);
        selectState=(EditText) findViewById(R.id.state);
        zipcode=(EditText) findViewById(R.id.zipcode);
        mobileNumber=(EditText) findViewById(R.id.mobilenumber);
    

        backArrow.setOnClickListener(this);

        check=getSharedPreferences("Default_Language",MODE_PRIVATE);
        value=check.getString("Value","");

        if(USER_ID!=null)
        {


            if(isOnline())
            {
                new Get_user_contact_info().execute();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No network connection to load contact details",Toast.LENGTH_SHORT).show();
            }

            firstname.setText(Serviceselectionpage.FIRSTNAME);
            lastname.setText(Serviceselectionpage.LASTNAME);
            email.setText(Serviceselectionpage.EMAIL);
            mobileNumber.setText(Serviceselectionpage.PHONENUM);
        }
       

       /* if(value.equals("CHENNAI"))
        {
            imPro.setVisibility(View.INVISIBLE);
            heading.setBackgroundResource(R.color.app_heading2);
            update.setBackgroundResource(R.drawable.ed_button2);

        }*/

        selectCity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hide_kyboard();
                return false;
            }
        });
        selectCity.setOnClickListener(new Select_country(My_profile_professional.this,selectCity));

        imPro.setOnClickListener(new Language_change(My_profile_professional.this,imPro));
        
        
        
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                str_email=email.getText().toString();
                str_address_1=address1.getText().toString();
                str_address_2=address2.getText().toString();
                str_selectCity=selectCity.getText().toString();
                str_state=selectState.getText().toString();
                str_zip_code=zipcode.getText().toString();
                str_mobile_num=mobileNumber.getText().toString();



                if(!isOnline())
                {
                    Toast.makeText(getApplicationContext(), "No network connection" ,Toast.LENGTH_SHORT).show();
                }
                else if(USER_ID==null)
                {
                    Toast.makeText(getApplicationContext(), "Please Sigin in your application" ,Toast.LENGTH_SHORT).show();
                }
              /*  else if(str_email.length()>0&&!validEmail(str_email))
                {
                    *//*if(!validEmail(str_alt_email))
                    {*//*
                        Toast.makeText(getApplicationContext(), "Enter the valid email id" ,Toast.LENGTH_SHORT).show();
                    //}
                    //Toast.makeText(getApplicationContext(),"Enter the alternate email id" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(!validEmail(str_alt_email))
                {
                    Toast.makeText(getApplicationContext(), "Enter the valid email id" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_mobile_num.length()==0 || str_mobile_num.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the Phone number" ,Toast.LENGTH_SHORT).show();
                }*/
               /* else if(str_mobile_num.length()==0 || str_mobile_num.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the Phone number" ,Toast.LENGTH_SHORT).show();
                }
                else if(str_address_1.length()==0 || str_address_1.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }
                else if(str_address_2.length()==0 || str_address_2.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the address" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_state.length()==0 || str_state.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the State" ,Toast.LENGTH_SHORT).show();
                }*/
                /*else if(str_selectCity.length()==0 || str_selectCity.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Select the City" ,Toast.LENGTH_SHORT).show();
                }*/

               /* else if(str_zip_code.length()==0 || str_zip_code.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the zipcode" ,Toast.LENGTH_SHORT).show();
                }*/



                else {
                    //Toast.makeText(getApplicationContext(, "Database error..contact admin" ,Toast.LENGTH_SHORT).show();
                    new Mytask().execute();
                }
                
                
                
            }
        });
    }

    private void hide_kyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private class Get_user_contact_info extends AsyncTask<Void,Void,Void>
    {

        String status="",message="";



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(My_profile_professional.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path=getResources().getString(R.string.url_service)+"prof/Prof_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"prof_getcontact_info"
                        +"&prof_id="+USER_ID,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(status.equals("true"))
            {

                try {
                    JSONArray array=jsn_response.getJSONArray("message");

                    JSONObject get_array=array.getJSONObject(0);

                    str_address_1=get_array.getString("address1");
                    str_address_2=get_array.getString("address2");
                    str_state=get_array.getString("state");
                    str_selectCity=get_array.getString("city");
                    str_mobile_num=get_array.getString("phone_number");
                    str_zip_code=get_array.getString("zip_code");
                    str_mobile_num=get_array.getString("phone_number");





                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            // Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if(status.equals("true")) {

                address1.setText(str_address_1);
                address2.setText(str_address_1);
                selectState.setText(str_address_1);
                selectCity.setText(str_selectCity);
                mobileNumber.setText(str_mobile_num);
                zipcode.setText(str_zip_code);


            }
            else {

                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();



            }
        }
    }


    String status="",message="";
    private class Mytask extends AsyncTask<Void,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(My_profile_professional.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path=getResources().getString(R.string.url_service)+"prof/Prof_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"prof_updateprofile"
                        +"&address1="+str_address_1+"&address2="+str_address_2
                        +"&country="+"India"+"&state="+str_state
                        +"&city="+str_selectCity+"&zip_code="+str_zip_code
                        +"&phone_number="+str_mobile_num+"&email="+str_email
                        +"&userid="+USER_ID,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpPost post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jsn_response = null;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(status.equals("true"))
            {


            }

            Log.e("status",status);Log.e("message",message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();


            if(status.equals("true")) {


                onBackPressed();

                Toast.makeText(getApplicationContext(), "Profile has been updated..", Toast.LENGTH_SHORT).show();



            }
            else {

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();



            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.imageArrow:
                onBackPressed();

                break;
        }

    }

    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
