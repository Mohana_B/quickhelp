package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Admin on 1/9/2017.
 */

public class Drawer_myrequest_history_list_details extends AppCompatActivity {
    ImageView language;
    LinearLayout backArrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_myrequest_history_list_details);

        backArrow=(LinearLayout)findViewById(R.id.imageArrow);
        language=(ImageView) findViewById(R.id.language);

        language.setOnClickListener(new Language_change(Drawer_myrequest_history_list_details.this,language));

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
