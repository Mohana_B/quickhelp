package com.trifft.admin.quichelp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by PSDeveloper on 1/11/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    public static final String DB_NAME="servfy.db";

    public static final String TABLE_NAME="username";

    String path="/data/data/com.example.admin.newservfy/databases/"+DB_NAME;

    Context context;

    InputStream in;

FileOutputStream out;

    SQLiteDatabase db;
    public Database(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);

        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }



    public void database()  {

        boolean exist=checkPath();


        if(!exist)
        {

            db=this.getWritableDatabase();

            try {
                copyDatabase();


                db.close();
            } catch (IOException e) {
                e.printStackTrace();

                Log.e("error",e.toString()+" "+"error message");
            }


        }
        Log.e("database", "started");
    }



    public boolean checkPath()
    {
       boolean exist=false;

        File f=context.getDatabasePath(DB_NAME);

        if(f.exists())
        {

            exist=true;
        }
        Log.e("path",f.toString());
        return exist;
    }

    public void copyDatabase() throws IOException {

        in=context.getAssets().open(DB_NAME);


        out=new FileOutputStream(path);

        byte[] b=new byte[1024];

        while(in.read()>0)
        {
            out.write(b);
        }
        out.flush();
        out.close();
        in.close();









    }

    public void insertData(String name)
    {
        db=this.getWritableDatabase();

        ContentValues value=new ContentValues();

        value.put("email",name);

        db.insert(TABLE_NAME,null,value);


        db.close();

    }


    public String getData()
    {
        db=this.getReadableDatabase();

        String data=null;

        Cursor cursor=db.rawQuery("select email from "+TABLE_NAME,null);

        while(cursor.moveToNext())
        {
            data=cursor.getString(0);
        }


        db.close();

        return data;
    }




}
