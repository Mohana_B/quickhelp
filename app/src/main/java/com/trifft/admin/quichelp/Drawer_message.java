package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import fragment_adapter.Drawer_message_adapter;

/**
 * Created by Admin on 12/30/2016.
 */
public class Drawer_message extends AppCompatActivity implements View.OnClickListener {

    Drawer_message_adapter swipe;
    TextView inbox,sent;
    View inboxview,sentview;
    ViewPager pager;
    public static ImageView imPro;
    LinearLayout backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_messages);

        swipe=new  Drawer_message_adapter(getSupportFragmentManager());
        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(swipe);

        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        imPro = (ImageView) findViewById(R.id.imageSignin);

        inboxview=(View)findViewById(R.id.v3);
        sentview=(View)findViewById(R.id.v4);
        inbox=(TextView)findViewById(R.id.inbox);
        sent=(TextView)findViewById(R.id.sent);

        inbox.setOnClickListener(this);
        sent.setOnClickListener(this);
        backArrow.setOnClickListener(this);


      /*  if(value.equals("SINGAPORE"))
        {
            imPro.setVisibility(View.INVISIBLE);
            l3.setBackgroundResource(R.color.app_heading2);
            l4.setBackgroundResource(R.color.app_heading2);
        }*/
       pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
       public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(pager.getCurrentItem()==0)
        {
            inboxview.setVisibility(View.VISIBLE);
            sentview.setVisibility(View.INVISIBLE);
        }
        if(pager.getCurrentItem()==1)
        {
            inboxview.setVisibility(View.INVISIBLE);
            sentview.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    });

        imPro.setOnClickListener(new Language_change(Drawer_message.this,imPro));
    }

    @Override
    public void onClick(View v) {
       switch (v.getId())
       {
           case R.id.inbox:
               pager.setCurrentItem(0);
               inboxview.setVisibility(View.VISIBLE);
               sentview.setVisibility(View.INVISIBLE);
               break;
           case R.id.sent:
               pager.setCurrentItem(1);
               inboxview.setVisibility(View.INVISIBLE);
               sentview.setVisibility(View.VISIBLE);
               break;
           case R.id.imageArrow:
               onBackPressed();
               break;
       }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}
