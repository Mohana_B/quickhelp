package com.trifft.admin.quichelp.model;

public class CheckDiscountAvailablResponse extends CommonResponse {
    private ActualResponse rebate_data;

    public ActualResponse getRebate_data() {
        return rebate_data;
    }

    public void setRebate_data(ActualResponse rebate_data) {
        this.rebate_data = rebate_data;
    }

    public class ActualResponse {
        private String code;
        private String discount;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
}
