package com.trifft.admin.quichelp;


import android.app.DatePickerDialog;
import android.content.Context;


import android.view.View;

import android.widget.DatePicker;
import android.widget.EditText;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by PSDeveloper on 11/16/2016.
 */

public class Date_picker implements View.OnClickListener {
    Context c;
    EditText edit;
    public Date_picker(Context activity, EditText date) {
        this.c=activity;
        edit=date;
    }


    @Override
    public void onClick(View view) {
        final Calendar calender = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener dialog = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                calender.set(Calendar.YEAR, year);
                calender.set(Calendar.MONTH, month);
                calender.set(Calendar.DAY_OF_MONTH, day);
                updateDate();
            }
            private void updateDate(){

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                edit.setText(sdf.format(calender.getTime()));


            }
        };
        new DatePickerDialog(c, dialog, calender
                .get(java.util.Calendar.YEAR), calender.get(java.util.Calendar.MONTH),
                calender.get(java.util.Calendar.DAY_OF_MONTH)).show();

    }

}

