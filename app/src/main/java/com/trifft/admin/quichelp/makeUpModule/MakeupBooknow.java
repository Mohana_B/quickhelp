package com.trifft.admin.quichelp.makeUpModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.trifft.admin.quichelp.Date_picker;
import com.trifft.admin.quichelp.R;
import com.trifft.admin.quichelp.Serviceselectionpage;
import com.trifft.admin.quichelp.TimePicker;
import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.model.IdNameModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.DropDownSelectionIdNameModel;
import com.trifft.admin.quichelp.utils.QuickHelpLog;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Fatima.t on 13/03/2020.
 */

public class MakeupBooknow extends Fragment implements AdapterView.OnItemSelectedListener, CommonInterface {
    SharedPreferences check;
    String value;
    Button makeUpGetPrice;
    ProgressDialog progressDialog;
    String first_name, last_name, email_st, mobileNumber, beautypack_st, makeupPack_st, date_data, time_data, address_1, address_2, special_comments, Zipcode, selectedCity;

    EditText firstname, lastname, etEmail, mobile_number, /*selectCountry,*/ zip_code, beautyPack, makeupPack, date, time, address1, address2, specialComments;

    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;
    private ArrayList<IdNameModel> makeupMasterList = new ArrayList<>();
    private ArrayList<IdNameModel> spaMasterList = new ArrayList<>();
    Spinner spnCountry;
    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.makeup_booknow, container, false);
        beautyPack = (EditText) v.findViewById(R.id.beauty_spa_pack);
        makeupPack = (EditText) v.findViewById(R.id.makeup_pack);
        date = (EditText) v.findViewById(R.id.date);
        time = (EditText) v.findViewById(R.id.time);
        makeUpGetPrice = (Button) v.findViewById(R.id.getaprice);
        spnCountry = v.findViewById(R.id.spn_country);
//        selectCountry = (EditText) v.findViewById(R.id.selectcountry);
        mobile_number = (EditText) v.findViewById(R.id.phonenumber);
        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);

        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        etEmail = v.findViewById(R.id.ac_et_email);

        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            etEmail.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);

        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);


        // Here call Master Data of Beauty and Makeup Packages
        new MakeupMasterDataTask().execute();

        /*if(value.equals("CHENNAI"))
        {
            //ac.setBackgroundResource(R.drawable.ed_button2);
            selectCountry.setText("CHENNAI");
            selectCountry.setEnabled(false);
        }
        else{selectCountry.setText("MUMBAI");}*/


        beautyPack.setOnClickListener(new DropDownSelectionIdNameModel(getActivity(), beautyPack, spaMasterList));
        makeupPack.setOnClickListener(new DropDownSelectionIdNameModel(getActivity(), makeupPack, makeupMasterList));

        /*// write here code for Beauty Package and makeUp Package Spinners
        beautyPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // here call beauty Api Call to get Data
                new MakeupMasterDataTask().execute();
            }
        });*/

       /* makeupPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // here call makeup Api Call to get Data
            }
        });*/

        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));

//        selectCountry.setOnClickListener(new Select_country(getActivity(), selectCountry));

        makeUpGetPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name = firstname.getText().toString();
                last_name = lastname.getText().toString();
                email_st = etEmail.getText().toString();
                date_data = date.getText().toString();
//                air_con = aircon.getText().toString();
                beautypack_st = beautyPack.getText().toString();
                makeupPack_st = makeupPack.getText().toString();

                time_data = time.getText().toString();
                mobileNumber = mobile_number.getText().toString();
                address_1 = address1.getText().toString();
                address_2 = address2.getText().toString();
                Zipcode = zip_code.getText().toString();
//                select_Country = selectCountry.getText().toString();
                selectedCity = spnCountry.getSelectedItem().toString();
                special_comments = specialComments.getText().toString();


                if (!Utils.isValidStr(first_name)) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(last_name)) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(mobileNumber)) {
                    Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() < 10) {
                    Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(email_st)) {
                    Toast.makeText(getActivity(), "Enter the Email Address", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email_st)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(beautypack_st)) {
                    Toast.makeText(getActivity(), "Select Beauty & Spa Package", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(makeupPack_st)) {
                    Toast.makeText(getActivity(), "Select Makeup Package", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(date_data)) {
                    Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(time_data)) {
                    Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                } else if (!Utils.isValidStr(selectedCity)) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                    DialogManager.showToast(getActivity(), "Please select country");
                } else if (Zipcode.length() == 0) {
                    Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                } else if (Zipcode.length() < 6) {
                    Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                } else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {
                    new Mytask().execute();
                }
            }
        });

        return v;
    }

    private class MakeupMasterDataTask extends AsyncTask<Void, Void, Void> {


        String status = "", message = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Beauty_Json/data?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*
            https://quichelp.com/json/Beauty_Json/data?service=beauty_types&category_name=Makeup Spa Service*/
            /*https://quichelp.com/json/Beauty_Json/data?service=beauty_types&category_name=Makeup Spa Service&city=Hyderabad*/

            URI uri = null;

            try {
                uri = new URI("https", path + "service=" + "beauty_types&category_name=Makeup Spa Service"/*+URLEncoder.encode("Makeup Spa Service", "UTF-8").replace(" ","%20")*/, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } /*catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            HttpPost post;
            HttpGet get;
            try {
//                post=new HttpPost(uri.toURL().toString());
                get = new HttpGet(uri.toURL().toString());

//                HttpResponse response=client.execute(post);
                HttpResponse response = client.execute(get);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*{
  "Makeup": {
    "26": "Airbrush Package",
    "27": "HD Package",
    "28": "Beauty",
    "29": "Beauty & Glow",
    "36": "Photoshoot"
  },
  "Spa": {
    "30": "Hair Spa Regular",
    "31": "Basic Facials",
    "32": "Gold Facials",
    "33": "Diamond Facials",
    "34": "Standard Body Spa package",
    "35": "Fully Body Spa Package",
    "37": "Hair Spa Jazzy Style"
  }
}*/


            JSONObject object = null;
            if (result != null) {

                QuickHelpLog.e("MakeupMasterData", result);
                try {
                    object = new JSONObject(result);

                    status = object.getString("status");
                    message = object.getString("message");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {

                MakeupMasterData resModel = new Gson().fromJson(result, MakeupMasterData.class);

                spaMasterList.clear();
                spaMasterList.addAll(resModel.getSpa());
                new DropDownSelectionIdNameModel(spaMasterList);

                makeupMasterList.clear();
                makeupMasterList.addAll(resModel.getMakeup());
                new DropDownSelectionIdNameModel(makeupMasterList);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            Toast.makeText(getActivity(), "Please select the city again", Toast.LENGTH_SHORT).show();

        }
    }

    private class Mytask extends AsyncTask<Void, Void, Void> {

        String status = "", jod_id = "", price = "", message = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Beauty_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*localhost/json/Beauty_Json/data/?service=insert_beauty_job&
            userid=45&
            firstname=Shrikar&
            lastname=kodamgulawar
            emailid=k.shrikar@gmail.com&
            contact=1234567899&
            select_city=Chennai&
            pincode=1212111&
            spa_package=Hair Spa Regular&
            makeup_package=HD Package&
            pref_date=2020-12-26&
            pref_time=3:00PM&
            frm_address=Hyderabad&
            comments=wewewe&*/

            URI uri = null;

            String email = "";
            if (EMAIL_ID != null && !EMAIL_ID.equals(""))
                email = EMAIL_ID;
            else if (email_st != null && !email_st.equals(""))
                email = email_st;

            try {
                uri = new URI("https", path + "service=" + "insert_beauty_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&lastname=" + last_name +
                        "&emailid=" + email +
                        "&contact=" + mobileNumber +
                        "&select_city=" + selectedCity +
                        "&pincode=" + Zipcode +
                        "&spa_package=" + beautypack_st +
                        "&makeup_package=" + makeupPack_st +
                        "&pref_date=" + date_data +
                        "&pref_time=" + time_data +
                        "&frm_address=" + address_1 +
//                        "&frm_address1=" + address_1 +
//                        "&frm_address2=" + address_2 +
                        "&comments=" + special_comments, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status = object.getString("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
//            Log.e("status",status);

            if (status.equals("true")) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*Intent intent = new Intent(getActivity(), Getprice_page.class);
                intent.putExtra("job_id", jod_id);
                intent.putExtra("price", price);
                startActivity(intent);*/
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        if (responseObj != null) {

            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {

    }
}
