package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/12/2016.
 */

public class Createaccount_users extends Fragment {

    ProgressDialog progressDialog;
    SharedPreferences check;
    String value;
    EditText firstname, lastname, password, confirmPssd, email_id, phonenumber;
    String f_name, l_name, pssd, cnfm_pssd, email, phone_num;
    String status, message;
    String email_pattern1, email_pattern2;
    Button create_user;
    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.createaccount_users, container, false);

        firstname = (EditText) v.findViewById(R.id.editText);
        lastname = (EditText) v.findViewById(R.id.editText1);
        email_id = (EditText) v.findViewById(R.id.editText2);
        password = (EditText) v.findViewById(R.id.editText3);
        confirmPssd = (EditText) v.findViewById(R.id.editText4);
        phonenumber = (EditText) v.findViewById(R.id.editText5);
        create_user = (Button) v.findViewById(R.id.button);
        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

       /* if(value.equals("CHENNAI"))
        {
            create_user.setBackgroundResource(R.drawable.ed_button2);
        }*/

        email_pattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        email_pattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
        create_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                f_name = firstname.getText().toString();
                l_name = lastname.getText().toString();
                email = email_id.getText().toString();
                pssd = password.getText().toString();
                cnfm_pssd = confirmPssd.getText().toString();
                phone_num = phonenumber.getText().toString();

                if (f_name.length() == 0 || f_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                } else if (l_name.length() == 0 || l_name.equals("")) {
                    Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                } else if (email.length() == 0 || email.equals("")) {
                    Toast.makeText(getActivity(), "Enter the email id", Toast.LENGTH_SHORT).show();
                } else if (!validEmail(email)) {
                    Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                } else if (pssd.length() == 0 || pssd.equals("")) {
                    Toast.makeText(getActivity(), "Enter the password", Toast.LENGTH_SHORT).show();
                } else if (cnfm_pssd.length() == 0 || cnfm_pssd.equals("")) {
                    Toast.makeText(getActivity(), "Enter the confirm password", Toast.LENGTH_SHORT).show();
                } else if (!pssd.equals(cnfm_pssd)) {
                    Toast.makeText(getActivity(), "Password mismatch", Toast.LENGTH_SHORT).show();
                } else if (phone_num.length() == 0 || phone_num.equals("")) {
                    Toast.makeText(getActivity(), "Enter the phone number", Toast.LENGTH_SHORT).show();
                } else if (phone_num.length() < 10 || phone_num.length() > 10  ) {
                    Toast.makeText(getActivity(), "Enter valid phone number", Toast.LENGTH_SHORT).show();
                } else if (!isOnline()) {
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                } else {
                    new MyTask_create_user().execute();
                }


            }
        });


        return v;
    }


    private class MyTask_create_user extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Creating account...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Register_Json/data?";

            String path = getActivity().getResources().getString(R.string.url_service) + "Register_Json/data?";
            DefaultHttpClient client = new DefaultHttpClient();

            URI uri = null;
            try {
                uri = new URI("https", path + "service=" + "registeruser" + "&firstname=" + f_name + "&lastname=" + l_name + "&email=" + email
                        + "&password=" + pssd + "&phonenumber=" + phone_num, null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response;
            try {
                jsn_response = new JSONObject(result);
                status = jsn_response.getString("status");
                message = jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("status", status);
            Log.e("message", message);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if (status.equals("true")) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Account created successfully..");
                alert.setMessage("Verification mail has been sent to your mail id..");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {


                        dialogInterface.dismiss();

                        Intent intent = new Intent(getActivity(), Sign_activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        //getActivity().finish();
                        startActivity(intent);


                    }
                }).show();

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                //firstname.setText("");lastname.setText("");password.setText("");confirmPssd.setText("");email_id.setText("");
                //phonenumber.setText("");
                //firstname.requestFocus();
            }
        }
    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


}
