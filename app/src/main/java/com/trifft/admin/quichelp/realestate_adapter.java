package com.trifft.admin.quichelp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class realestate_adapter extends FragmentPagerAdapter {

    ViewPager page;
    public realestate_adapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                return  new com.trifft.admin.quichelp.Realestateprovide();
            case 1:
                return new Realestatebooknow();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
