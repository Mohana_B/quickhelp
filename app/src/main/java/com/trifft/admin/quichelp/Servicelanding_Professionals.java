package com.trifft.admin.quichelp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

/**
 * Created by PSDeveloper on 11/11/2016.
 */

public class Servicelanding_Professionals extends Fragment {


    Button joinus, signin;

    SharedPreferences shared;
    String value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.servicelandingprofessionals, container, false);

        joinus = (Button) v.findViewById(R.id.join_us);
        signin = (Button) v.findViewById(R.id.sign_in);

        shared = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);

        value = shared.getString("Value", "");

          /*  if(value.equals("CHENNAI"))
            {
                joinus.setBackgroundResource(R.drawable.ed_button2);
                signin.setBackgroundResource(R.drawable.ed_button2);
            }*/


        joinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(getActivity(), Createaccount.class);
                n.putExtra(Createaccount.EXTRA_PROF_USER, true);
                startActivity(n);

            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), Sign_activity.class);
                in.putExtra(Createaccount.EXTRA_PROF_USER, true);
                startActivity(in);
            }
        });

        return v;
    }
}


