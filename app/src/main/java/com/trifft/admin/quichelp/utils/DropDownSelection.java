package com.trifft.admin.quichelp.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.trifft.admin.quichelp.Myapplication;
import com.trifft.admin.quichelp.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Created by Fatima.t on 03/23/2020.
 */

public class DropDownSelection implements View.OnClickListener {

    public static ArrayList<String> globol_city;

    Context cn;
    EditText edit;
//    TextView german,singapore;
    SharedPreferences checking_language;
    ProgressDialog progressDialog;
    SharedPreferences country_change;

    ListView list;

    ArrayAdapter<String> arrayAdapter;

    ArrayList<String> arrayList;

    public DropDownSelection(Context activity, EditText select_country, ArrayList<String> dataList) {
        cn=activity;
        edit=select_country;
        arrayList = dataList;
    }
    String local;
    @Override
    public void onClick(View view) {

        LayoutInflater in=(LayoutInflater)cn.getSystemService(cn.LAYOUT_INFLATER_SERVICE);
        View vv=in.inflate(R.layout.popup_selectcountry,null);
        /*german=(TextView)vv.findViewById(R.id.german_cntry);
        singapore=(TextView)vv.findViewById(R.id.singapore_cntry);*/

        list=(ListView)vv.findViewById(R.id.list);


        final PopupWindow pop=new PopupWindow(vv,edit.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT,true);
        pop.setOutsideTouchable(true);
        pop.setBackgroundDrawable(new ColorDrawable(cn.getResources().getColor(R.color.view)));
        pop.setFocusable(true);
        pop.showAsDropDown(edit);

        checking_language=cn.getSharedPreferences("Default_Language",cn.MODE_PRIVATE);

        local=checking_language.getString("Value",null);

//        Log.e("language",local);

        country_change=cn.getSharedPreferences("Default_Language",cn.MODE_PRIVATE);
        final SharedPreferences.Editor edi=country_change.edit();

//        globol_city=new ArrayList<String>();

//        arrayList=new ArrayList<String>();


        if(/*Myapplication.glogol_city.size()*/ arrayList.size() > 0)
        {

//            Log.e("Globol city",globol_city.toString());

            // check Myapplication.glogol_city has duplicates or not
            /*
             * convert array to list and then add all
             * elements to LinkedHashSet. LinkedHashSet
             * will automatically remove all duplicate elements.
             */
            LinkedHashSet<String> lhSetColors = new LinkedHashSet<String>(arrayList);

            arrayList.clear();
            arrayList.addAll(lhSetColors);


            arrayAdapter=new ArrayAdapter<String>(cn,android.R.layout.simple_spinner_dropdown_item, arrayList);

            list.setAdapter(arrayAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    edit.setText(arrayList.get(position));
                    pop.dismiss();
                }
            });
        }
       /* else {

            if(!isOnline())
            {
                Toast.makeText(cn, "No network connection" ,Toast.LENGTH_SHORT).show();
            }
            else
            {
               new Load_city().execute();
            }
        }*/



      /*  if(local.equals("CHENNAI"))
        {
        german.setVisibility(View.GONE);
        }
        else if(local.equals("MUMBAI"))
        {
            singapore.setVisibility(View.GONE);

        }*/


       /* german.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pop.dismiss();
                String eng=german.getText().toString();
                edit.setText(eng);
                edi.putString("Value","CHENNAI").commit();


          *//*     // ((Activity)cn).recreate();
                ((Activity)cn).finish();
                ((Activity)cn).startActivity(((Activity) cn).getIntent());*//*


            }
        });*/
       /* singapore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pop.dismiss();
                String ger=singapore.getText().toString();
                edit.setText(ger);
                edi.putString("Value","MUMBAI").commit();

               *//* String language="en-US";
                Locale loca=new Locale(language);
                Locale.setDefault(loca);
                Configuration confi=new Configuration();
                confi.locale=loca;
                cn.getResources().updateConfiguration(confi,cn.getResources().getDisplayMetrics());

                ((Activity)cn).finish();
                ((Activity)cn).startActivity(((Activity) cn).getIntent());*//*
                //((Activity)cn).recreate();

            }
        });*/

    }

    public class Load_city extends AsyncTask<Void,Void,Void>
    {

        String status="",message="";



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog=new ProgressDialog(cn);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path=cn.getResources().getString(R.string.url_service)+"Login_Json/data?";

            DefaultHttpClient client =new DefaultHttpClient();



            URI uri = null;

            try {
                uri=new URI("https",path+"service="+"getcountry",null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            HttpPost post;
            try {
                post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            if(result!=null) {
                try {
                    object = new JSONObject(result);

                    status = object.getString("status");
                     message=object.getString("message");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
          //  Log.e("status",status);

            ArrayList<String> arrayList=new ArrayList<String>();
            String str_array[];
            String city="";
            if(status.equals("true"))
            {
                try {
                    JSONObject get_city=object.getJSONObject("message");

                    city=get_city.getString("city");

                    str_array=city.split(",");

                    for(int i=0;i<str_array.length;i++)
                    {
                        Myapplication.glogol_city.add(str_array[i]);
                    }

                   // globol_city=arrayList.;

                    Log.e("globol_city",Myapplication.glogol_city.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            Toast.makeText(cn, "Please select the city again" ,Toast.LENGTH_SHORT).show();

        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) cn.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
