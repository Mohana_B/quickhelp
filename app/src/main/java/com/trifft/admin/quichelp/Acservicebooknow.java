package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.trifft.admin.quichelp.activities.BaseActivity;
import com.trifft.admin.quichelp.activities.DiscountInfoActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.listeners.onBackPressInteface;
import com.trifft.admin.quichelp.model.CountryMasterModel;
import com.trifft.admin.quichelp.utils.DialogManager;
import com.trifft.admin.quichelp.utils.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11/11/2016.
 */

public class Acservicebooknow extends Fragment implements onBackPressInteface, AdapterView.OnItemSelectedListener, CommonInterface {

    SharedPreferences check;
    String value;
    Button ac;
    ProgressDialog progressDialog;
    String first_name, last_name, email_st, mobileNumber, air_con, typeofService, date_data, time_data, address_1, address_2, special_comments, Zipcode, selectedCity;

    EditText firstname, lastname, etEmail, mobile_number, /*selectCountry,*/
            zip_code, aircon, date, time, address1, address2, specialComments;
    Spinner spnCountry;
    String USER_ID = Serviceselectionpage.USER_ID, EMAIL_ID = Serviceselectionpage.EMAIL;

    private RadioButton repair_servicing;
    private RadioButton gas_servicing;
    private RadioButton normal_servicing;
    private RadioButton wet_cleaning;
    private RadioButton mount_servicing;
    private RadioButton filter_servicing;
    private RadioGroup select_opt;
    private String radioButton;

    String status = "", jod_id = "", price = "", message = "";
    boolean status1;

    private ArrayList<String> countryList = new ArrayList<>();
    private ArrayAdapter<String> countryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.acservicebooknow, container, false);
        aircon = (EditText) v.findViewById(R.id.aircon);
        date = (EditText) v.findViewById(R.id.date);
        time = (EditText) v.findViewById(R.id.time);
        ac = (Button) v.findViewById(R.id.getaprice);
        spnCountry = v.findViewById(R.id.ac_spn_country);
//        selectCountry = (EditText) v.findViewById(R.id.selectcountry);
        mobile_number = (EditText) v.findViewById(R.id.phonenumber);
        firstname = (EditText) v.findViewById(R.id.firstname);
        lastname = (EditText) v.findViewById(R.id.lastname);
        address1 = (EditText) v.findViewById(R.id.address1);
        address2 = (EditText) v.findViewById(R.id.address2);
        zip_code = (EditText) v.findViewById(R.id.zipcode);
        specialComments = (EditText) v.findViewById(R.id.specialcomments);
        //select_city=(EditText) v.findViewById(R.id.selectcity);
        select_opt = (RadioGroup) v.findViewById(R.id.selcetion);
        wet_cleaning = (RadioButton) v.findViewById(R.id.wet);
        gas_servicing = (RadioButton) v.findViewById(R.id.gas);
        normal_servicing = (RadioButton) v.findViewById(R.id.normalcleaning);
        mount_servicing = (RadioButton) v.findViewById(R.id.mounting);
        filter_servicing = (RadioButton) v.findViewById(R.id.filter);
        repair_servicing = (RadioButton) v.findViewById(R.id.repairs);
        check = getActivity().getSharedPreferences("Default_Language", getActivity().MODE_PRIVATE);
        value = check.getString("Value", "");

        etEmail = v.findViewById(R.id.ac_et_email);


        if (Serviceselectionpage.countryListGlobal != null && Serviceselectionpage.countryListGlobal.size() > 0) {
            countryList.clear();
            countryList.addAll(Serviceselectionpage.countryListGlobal);
        }
        countryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, countryList);
        spnCountry.setAdapter(countryAdapter);
        spnCountry.setOnItemSelectedListener(this);

        if (Serviceselectionpage.countryListGlobal == null || Serviceselectionpage.countryListGlobal.isEmpty())
            APIRequestHandler.getInstance().countryMaster(getActivity(), this);



        if (Utils.isValidStr(Serviceselectionpage.FIRSTNAME))
            firstname.setText(Serviceselectionpage.FIRSTNAME);

        if (Utils.isValidStr(Serviceselectionpage.LASTNAME))
            lastname.setText(Serviceselectionpage.LASTNAME);

        if (Utils.isValidStr(Serviceselectionpage.EMAIL))
            etEmail.setText(Serviceselectionpage.EMAIL);

        if (Utils.isValidStr(Serviceselectionpage.PHONENUM))
            mobile_number.setText(Serviceselectionpage.PHONENUM);
        /*if(value.equals("CHENNAI"))
        {
            //ac.setBackgroundResource(R.drawable.ed_button2);
            selectCountry.setText("CHENNAI");
            selectCountry.setEnabled(false);
        }
        else{selectCountry.setText("MUMBAI");}*/


        aircon.setOnClickListener(new Popup_fiveroom(getActivity(), aircon, "aircon"));
        date.setOnClickListener(new Date_picker(getActivity(), date));
        time.setOnClickListener(new TimePicker(getActivity(), time));

//        selectCountry.setOnClickListener(new Select_country(getActivity(), selectCountry));
        spnCountry.setOnItemSelectedListener(this);

        wet_cleaning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    gas_servicing.setChecked(false);
                    normal_servicing.setChecked(false);
                    mount_servicing.setChecked(false);
                    filter_servicing.setChecked(false);
                    repair_servicing.setChecked(false);
                }
            }
        });
        gas_servicing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    wet_cleaning.setChecked(false);
                    normal_servicing.setChecked(false);
                    mount_servicing.setChecked(false);
                    filter_servicing.setChecked(false);
                    repair_servicing.setChecked(false);
                }
            }
        });
        normal_servicing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    gas_servicing.setChecked(false);
                    wet_cleaning.setChecked(false);
                    mount_servicing.setChecked(false);
                    filter_servicing.setChecked(false);
                    repair_servicing.setChecked(false);
                }
            }
        });
        mount_servicing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    gas_servicing.setChecked(false);
                    normal_servicing.setChecked(false);
                    wet_cleaning.setChecked(false);
                    filter_servicing.setChecked(false);
                    repair_servicing.setChecked(false);
                }
            }
        });
        filter_servicing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    gas_servicing.setChecked(false);
                    normal_servicing.setChecked(false);
                    mount_servicing.setChecked(false);
                    wet_cleaning.setChecked(false);
                    repair_servicing.setChecked(false);
                }
            }
        });
        repair_servicing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    gas_servicing.setChecked(false);
                    normal_servicing.setChecked(false);
                    mount_servicing.setChecked(false);
                    filter_servicing.setChecked(false);
                    wet_cleaning.setChecked(false);
                }
            }
        });


        //select_city.setOnClickListener(new Select_city_listner(getActivity(),select_city,select_country.getText().toString()));


        ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidStr(USER_ID)) {

                    first_name = firstname.getText().toString();
                    last_name = lastname.getText().toString();
                    email_st = etEmail.getText().toString();
                    date_data = date.getText().toString();
                    air_con = aircon.getText().toString();
                    time_data = time.getText().toString();
                    mobileNumber = mobile_number.getText().toString();
                    address_1 = address1.getText().toString();
                    address_2 = address2.getText().toString();
                    selectedCity = spnCountry.getSelectedItem().toString();
                    Zipcode = zip_code.getText().toString();
//                    select_Country = selectCountry.getText().toString();
                    special_comments = specialComments.getText().toString();


                    if (first_name.equals("")) {
                        Toast.makeText(getActivity(), "Enter the firstname", Toast.LENGTH_SHORT).show();
                    } else if (last_name.equals("")) {
                        Toast.makeText(getActivity(), "Enter the lastname", Toast.LENGTH_SHORT).show();
                    } else if (email_st == null || email_st.equals("")) {
                        Toast.makeText(getActivity(), "Enter the Email Address", Toast.LENGTH_SHORT).show();
                    } else if (!validEmail(email_st)) {
                        Toast.makeText(getActivity(), "Enter the valid email id", Toast.LENGTH_SHORT).show();
                    } else if (!wet_cleaning.isChecked() && !gas_servicing.isChecked() && !repair_servicing.isChecked() && !normal_servicing.isChecked() && !mount_servicing.isChecked() && !filter_servicing.isChecked()) {
                        Toast.makeText(getActivity(), "Select the service type", Toast.LENGTH_SHORT).show();
                    } else if (air_con.equals("")) {
                        Toast.makeText(getActivity(), "Select the No of aircon", Toast.LENGTH_SHORT).show();
                    } else if (mobileNumber.equals("")) {
                        Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_SHORT).show();
                    } else if (mobileNumber.length() < 10) {
                        Toast.makeText(getActivity(), "Enter the valid mobile number", Toast.LENGTH_SHORT).show();
                    } else if (date_data.equals("")) {
                        Toast.makeText(getActivity(), "Select the Date", Toast.LENGTH_SHORT).show();
                    } else if (time_data.equals("")) {
                        Toast.makeText(getActivity(), "Select the Time", Toast.LENGTH_SHORT).show();
                    } else if (!Utils.isValidStr(selectedCity)) {
                        DialogManager.showToast(getActivity(), "Please select country");
                    } else if (selectedCity.equalsIgnoreCase(getString(R.string.select_city))) {
                        DialogManager.showToast(getActivity(), "Please select country");
                    } else if (Zipcode.length() == 0) {
                        Toast.makeText(getActivity(), "Enter the zipcode", Toast.LENGTH_SHORT).show();
                    } else if (Zipcode.length() < 6) {
                        Toast.makeText(getActivity(), "Enter valid zipcode", Toast.LENGTH_SHORT).show();
                    } else if (!isOnline()) {
                        Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                    } else {
                        if (wet_cleaning.isChecked()) {
                            typeofService = "Wet Cleaning";

                        }

                        if (gas_servicing.isChecked()) {
                            typeofService = "Gas refill";


                        }
                        if (normal_servicing.isChecked()) {
                            typeofService = "Normal Cleaning";


                        }
                        if (mount_servicing.isChecked()) {
                            typeofService = "Personal Home";


                        }
                        if (filter_servicing.isChecked()) {
                            typeofService = "Filter Installation";


                        }
                        if (repair_servicing.isChecked()) {
                            typeofService = "Repair";

                        }

                        new Mytask().execute();
                    }
                } else {
                    DialogManager.showSingleBtnPopup(getActivity(), new DialogMangerCallback() {
                        @Override
                        public void onOkClick(View view) {
                            onBackPressed();
                        }

                        @Override
                        public void onCancelClick(View view) {

                        }
                    }, getString(R.string.Service), getString(R.string.not_logged_in_msg), getString(R.string.ok));
                }

            }
        });


        return v;
    }

    private void getCountryMaster() {
        APIRequestHandler.getInstance().countryMaster(getActivity(), this);
    }

    @Override
    public void onBackPressed() {
        onDetach();
        BaseActivity.showHomePage(getActivity());
    }

    @Override
    public void onRequestSuccess(Object responseObj) {

        if (responseObj != null) {
            if (responseObj instanceof CountryMasterModel) {
                CountryMasterModel response = (CountryMasterModel) responseObj;
                if (response.isStatus()) {
                    CountryMasterModel.City actualResponse = response.getMessage();
                    if (Utils.isValidStr(actualResponse.getCity())) {
                        String[] stArray = actualResponse.getCity().split(",");
                        List<String> list = Arrays.asList(stArray);
                        countryList.clear();
                        countryList.add(getString(R.string.select_city));
                        countryList.addAll(list);
                        countryAdapter.notifyDataSetChanged();

                        Serviceselectionpage.countryListGlobal.clear();
                        Serviceselectionpage.countryListGlobal.addAll(list);
                    }
                } else {
                    DialogManager.showToast(getActivity(), "Something wrong happend with Country master Api call");
                }
            }
        }
    }

    /*private void showDiscAvailablePopup(CheckDiscountAvailablResponse.ActualResponse actualResponse) {

        if (getActivity() != null) {
            Dialog mDialog = new Dialog(getActivity());
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(R.layout.popup_discount_available);
        }

        // make api call to get discounted price
        APIRequestHandler.getInstance().getDiscountInfo("get_discount", USER_ID, jod_id, price, actualResponse.getCode(), AppConstants.COUPON, getActivity(), this);

    }*/

    @Override
    public void onRequestFailure(Object responseObj, Throwable errorCode) {
        if (responseObj != null) {
            if (responseObj instanceof CountryMasterModel) {
                gotoPriceScreen();
            }
        }
    }

    private void gotoPriceScreen() {
        Intent intent = new Intent(getActivity(), Getprice_page.class);
        intent.putExtra("job_id", jod_id);
        intent.putExtra("price", price);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.ac_spn_country:
                if (i > 0) {
                    spnCountry.setTag(countryList.get(i));
                } else
                    DialogManager.showToast(getActivity(), " Please select country");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


/*   public void clearRadioChecked() {
        wet_cleaning.setChecked(false);
        gas_servicing.setChecked(false);
        normal_servicing.setChecked(false);
        mount_servicing.setChecked(false);
        filter_servicing.setChecked(false);
        repair_servicing.setChecked(false);
    }*/


    private class Mytask extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            String path = getActivity().getResources().getString(R.string.url_service) + "Aircon_Json/data/?";

            DefaultHttpClient client = new DefaultHttpClient();

            /*POST /json/Aircon_Json/data/?service=insert_aircon_job&amp;
            userid=191&amp;
            firstname=Shrikar&amp;
            lastname=kodamgulawar&amp;
            email=k.shrikar@gmail.com&amp;
            frm_city=Chennai&amp;
            frm_zip=1212111&amp;
            phone=1234567899&amp;
            service_type=Wet Cleaning&amp;
            no_aircon=2&amp;
            pref_date=2020-02-26&amp;
            pref_time=3:00PM&amp;
            frm_address1=Hyderabad&amp;
            frm_address2=dsdsd
            sp_comments=wewewe&amp;*/

            URI uri = null;

            String email = "";
            if (EMAIL_ID != null && !EMAIL_ID.equals(""))
                email = EMAIL_ID;
            else if (email_st != null && !email_st.equals(""))
                email = email_st;

            date_data = Utils.formatDateToServer(date_data);
            time_data = Utils.formatTime24To12(time_data);

            try {
                uri = new URI("https", path + "service=" + "insert_aircon_job" +
                        "&userid=" + USER_ID +
                        "&firstname=" + first_name +
                        "&email=" + email +
                        "&frm_city=" + selectedCity +
                        "&frm_zip=" + Zipcode +
                        "&phone=" + mobileNumber +
                        "&service_type=" + typeofService +
                        "&no_aircon=" + air_con +
                        "&pref_date=" + date_data +
                        "&pref_time=" + time_data +
                        "&frm_address1=" + address_1 +
                        "&sp_comments=" + special_comments +
                        "&lastname=" + last_name +
                        "&frm_address2=" + address_2
                        , null);
                Log.e("uri", uri.toString());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("uri", uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post = new HttpPost(uri.toURL().toString());
                HttpResponse response = client.execute(post);
                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                status1 = object.getBoolean("status");
                message = object.getString("message");

            } catch (JSONException e) {
                e.printStackTrace();
            }
//            Log.e("status",status);

            if (status1 == true) {
                try {
                    jod_id = object.getString("jobid");
                    price = object.getString("price");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            return null;


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status1 == true) {
                Toast.makeText(getActivity(), "Your Booking is Successful", Toast.LENGTH_SHORT).show();

                if (progressDialog != null)
                    progressDialog.dismiss();

                Intent gotoDiscountChk = new Intent(getActivity(), DiscountInfoActivity.class);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_JOB_ID, jod_id);
                gotoDiscountChk.putExtra(DiscountInfoActivity.EXTRA_PRICE, price);
                startActivity(gotoDiscountChk);
                /*https://quichelp.com/json/Booking_Json/data/?service=rebate_data&userid=191*/
//                APIRequestHandler.getInstance().checkDiscountAvailable("rebate_data", USER_ID, getActivity(), (CommonInterface) this);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private boolean validEmail(String email) {

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}