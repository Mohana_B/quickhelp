package com.trifft.admin.quichelp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.trifft.admin.quichelp.R;


public class Sign_newactivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for changing status bar icon colors
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        setContentView(R.layout.login_screen);
    }

    public void onLoginClick(View View){
        startActivity(new Intent(this,Register_newactivity.class));
        overridePendingTransition(R.anim.slide_in_right,R.anim.stay);
    }
}
