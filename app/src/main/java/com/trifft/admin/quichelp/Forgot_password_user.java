package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by PSDeveloper on 12/26/2016.
 */

public class Forgot_password_user extends AppCompatActivity implements View.OnClickListener {

    String pattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String pattern1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

    ImageView language;
    LinearLayout backArrow;

    EditText email;
    private Context mContext;
    Button submit;
    ProgressDialog progressDialog;
    String EMAIL;
    String status,message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        backArrow = (LinearLayout) findViewById(R.id.imageArrow);
        language = (ImageView) findViewById(R.id.language);
        mContext = this;
        email=(EditText) findViewById(R.id.editText);

        submit=(Button) findViewById(R.id.b4);

        backArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Forgot_password_user.this,language));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EMAIL=email.getText().toString();


                if(EMAIL.length()==0||EMAIL.equals(""))
                {
                    Toast.makeText(getApplicationContext(), "Enter the email id" ,Toast.LENGTH_SHORT).show();
                }
                else if(!validEmail(EMAIL))
                {
                    Toast.makeText(getApplicationContext(), "Enter the valid email id" ,Toast.LENGTH_SHORT).show();
                }
                else if(!isOnline())
                {
                    Toast.makeText(getApplicationContext(), "No network connection" ,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    new MyTask_create_user(mContext).execute();
                }



            }
        });

    }


    private class MyTask_create_user extends AsyncTask<Void,Void,Void>
    {   Context context;

        public MyTask_create_user(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog=new ProgressDialog(Forgot_password_user.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            String result = null;

            //String path="//www.servfy.com/json/Register_Json/data?";

            String path=getResources().getString(R.string.url_service)+"Login_Json/data?";
            DefaultHttpClient client=new DefaultHttpClient();



            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"resetpassword"+"&email="+EMAIL,
                        null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post=new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsn_response;
            try {
                jsn_response=new JSONObject(result);
                status=jsn_response.getString("status");
                message=jsn_response.getString("message");

                Log.e("status",status);Log.e("message",message);

            } catch (JSONException e) {
                e.printStackTrace();
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog.dismiss();

            if(status.equals("true"))
            {
                AlertDialog.Builder goLogin   =  new AlertDialog.Builder(context)
                        .setTitle("Quic Help")
                        .setMessage("Password has been sent to your mail id..")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation
                                onBackPressed();

                                dialog.dismiss();
                            }
                        });

                // A null listener allows the button to dismiss the dialog and take no further action.
                goLogin.show();

            }
            else
            {
                Toast.makeText(getApplicationContext(), message ,Toast.LENGTH_SHORT).show();

            }
        }
    }

    private boolean validEmail(String email) {

        Pattern pattern = Pattern.compile(pattern2);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imageArrow:

                onBackPressed();
                break;

        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}
