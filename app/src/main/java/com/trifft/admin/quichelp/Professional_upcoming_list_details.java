package com.trifft.admin.quichelp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.trifft.admin.quichelp.activities.BaseActivity;
import com.trifft.admin.quichelp.activities.JobRecreateEditActivity;
import com.trifft.admin.quichelp.apiInterfaces.APIRequestHandler;
import com.trifft.admin.quichelp.apiInterfaces.CommonInterface;
import com.trifft.admin.quichelp.listeners.DialogMangerCallback;
import com.trifft.admin.quichelp.model.CommonResponse;
import com.trifft.admin.quichelp.utils.DialogManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by aduser on 12/13/2017.
 */

public class Professional_upcoming_list_details extends BaseActivity implements View.OnClickListener, CommonInterface {
    ImageView language;
    Button editBtn,cancel,paynow, payByCashBtn;
    LinearLayout imageArrow;
    ProgressDialog progressDialog;
    String JOD_ID,JOB_STATUS,DATE,TIME,PRICE,PAYMENT_STATUS;
    TextView jod_id,job_status,date,time,price,payment_status;

    String USER_ID=Serviceselectionpage.USER_ID;
    String EMAIL=Serviceselectionpage.EMAIL;
    String FIRSTNAME=Serviceselectionpage.FIRSTNAME;
    String LASTNAME=Serviceselectionpage.LASTNAME;

    LinearLayout edit_cancel,price_linear,payment_status_linear;
    View price_view;
    private Professional_upcoming_list_details thisActivity;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_callhistory_upcoming_list_details);
        thisActivity = this;

        edit_cancel= (LinearLayout) findViewById(R.id.edit_cancel_linear);
        edit_cancel.setVisibility(View.GONE);
        price_linear= (LinearLayout) findViewById(R.id.price_linear);
        price_linear.setVisibility(View.GONE);

        price_view=(View) findViewById(R.id.price_view);
        price_view.setVisibility(View.GONE);

        payment_status_linear= (LinearLayout) findViewById(R.id.payment_status_linear);
        payment_status_linear.setVisibility(View.INVISIBLE);

        imageArrow = (LinearLayout) findViewById(R.id.imageArrow);


        language = (ImageView) findViewById(R.id.language);
        jod_id=(TextView)findViewById(R.id.jod_id);
        job_status=(TextView)findViewById(R.id.job_status);
        date=(TextView)findViewById(R.id.date);
        time=(TextView)findViewById(R.id.time);
        price=(TextView)findViewById(R.id.price);
        payment_status=(TextView)findViewById(R.id.payment_status);

        cancel=(Button)findViewById(R.id.cancel);
        paynow=(Button)findViewById(R.id.paynow);

        editBtn = findViewById(R.id.edit);
        editBtn.setOnClickListener(this);

        payByCashBtn = findViewById(R.id.pay_by_cash);
        payByCashBtn.setOnClickListener(this);

        JOD_ID=getIntent().getExtras().getString("jod_id");
        JOB_STATUS=getIntent().getExtras().getString("job_status");
        DATE=getIntent().getExtras().getString("date");
        TIME=getIntent().getExtras().getString("time");
        //PRICE=getIntent().getExtras().getString("price");
        //PAYMENT_STATUS=getIntent().getExtras().getString("payment_status");

        // As this is used by professiona;l, so no need to show payment options
        paynow.setVisibility(View.GONE);
        payByCashBtn.setVisibility(View.GONE);

        imageArrow.setOnClickListener(this);
        language.setOnClickListener(new Language_change(Professional_upcoming_list_details.this,language));


        jod_id.setText(JOD_ID);
        job_status.setText(JOB_STATUS);
        price.setText(PRICE);
        date.setText(DATE);
        time.setText(TIME);
        payment_status.setText(PAYMENT_STATUS);
        Log.e("job_id",JOD_ID  +" "+ JOB_STATUS+" " +PRICE+" "+ DATE +" "+ TIME+" "+PAYMENT_STATUS);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Professional_upcoming_list_details.JobCancel().execute();

            }
        });
    }

    String status="",message="";
    private class JobCancel extends AsyncTask<Void,Void,Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(Professional_upcoming_list_details.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... voids) {


            String result = null;

            //String path="//www.servfy.com/json/Login_Json/data?";

            String path=getResources().getString(R.string.url_service)+"Clean_Json/data?";

            DefaultHttpClient client=new DefaultHttpClient();

            URI uri = null;
            try {
                uri=new URI("https",path+"service="+"job_cancel"+"&job_id="+JOD_ID+
                        "&userid="+USER_ID+"&email="+EMAIL+"&firstname="+FIRSTNAME+"&lastname="+LASTNAME,null);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            try {
                Log.e("result",uri.toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpPost post;
            try {
                post =new HttpPost(uri.toURL().toString());
                HttpResponse response=client.execute(post);
                HttpEntity entity=response.getEntity();
                result= EntityUtils.toString(entity);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Log.e("response",result);

            JSONObject jsn_response = null;
            try {
                jsn_response = new JSONObject(result);

                Log.d("json",jsn_response.toString());


                status=jsn_response.getString("status");
                message=jsn_response.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }




            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();

            if(status.equals("true"))
            {
                Toast.makeText(Professional_upcoming_list_details.this, "Your job has been cancelled", Toast.LENGTH_SHORT).show();

                Intent intent =new Intent(getApplicationContext(),Drawerlayout_callhistory.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                intent.putExtra("getPage","upcoming");
                startActivity(intent);
            }
            else
            {
                Toast.makeText(Professional_upcoming_list_details.this, message, Toast.LENGTH_SHORT).show();
            }
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imageArrow:
                back_function();
                break;
            case R.id.edit:
//                DialogManager.showToast(this, "Clicked on Edit");
                Intent gotoEditJob = new Intent(this, JobRecreateEditActivity.class);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_ID, JOD_ID);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_JOB_STATUS, JOB_STATUS);
                gotoEditJob.putExtra(JobRecreateEditActivity.EXTRA_RECREATE_OR_EDIT, getString(R.string.edit));
                startActivityForResult(gotoEditJob, JobRecreateEditActivity.JOB_EDIT_RECREATE_REQUEST);
                break;
            case R.id.pay_by_cash:
                // call pay by Cash Api
                APIRequestHandler.getInstance().payByCashJob("update_payment_state","app", "cash",JOD_ID, thisActivity, this);

                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        back_function();
    }


    private void back_function()
    {
        Intent in=new Intent(getApplicationContext(),Professional_job_history.class);
        in.putExtra("getPage","upcoming");
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(in);
    }


    @Override
    public void onRequestSuccess(Object responseObj) {
        super.onRequestSuccess(responseObj);
        if (responseObj instanceof CommonResponse) {
            CommonResponse response = (CommonResponse) responseObj;
            if (response.isStatus()) {
                DialogManager.showSingleBtnPopup(thisActivity, new DialogMangerCallback() {
                    @Override
                    public void onOkClick(View view) {
                        finish();
                    }

                    @Override
                    public void onCancelClick(View view) {

                    }
                }, getString(R.string.alert), response.getMessage(), getString(R.string.ok));
            }
        }
    }
}
