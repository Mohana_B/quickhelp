package com.trifft.admin.quichelp;

import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.fragment.app.Fragment;
import fragment_adapter.Drawer_message_inbox_listadapter;

/**
 * Created by Admin on 12/30/2016.
 */

public class Drawer_message_inbox extends Fragment {
    ListView list;
    //String[] from={"admin@servfy.com","amala@servfy.com","noble@servfy.com"};
    String[] subject = {"New Cleaning request", "New Cleaning request", "New Cleaning request"};
    String[] date = {"30-12-2016", "28-12-2016", "22-12-2016"};


    public static String SUBJECT = "subject";

    public static String DATE = "date";

    ImageView backArrow,Language,delete;

    List<WorldPopulation> worldpopulationlist = new ArrayList<WorldPopulation>();

    ImageView language_chande=Drawer_message.imPro;

    ArrayList<HashMap<String,String>> arraylia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.drawer_message_inbox, null);
        list = (ListView) v.findViewById(R.id.list);


        //ArrayList<HashMap<String,String>> arraylisr=new ArrayList<HashMap<String, String>>();

       /* for(int i=0;i<subject.length;i++)
        {
            HashMap<String,String> map=new HashMap<String, String>();
            map.put("subject",subject[i]);
            map.put("date",date[i]);
            arraylisr.add(map);

        }*/

        /*for (int i = 0; i < subject.length; i++) {
            WorldPopulation worldpopulation = new WorldPopulation(
                    subject[i], date[i]);
            worldpopulationlist.add(worldpopulation);
        }*/

        arraylia=new ArrayList<HashMap<String, String>>();

        for(int i=0;i<subject.length;i++)
        {
            HashMap<String,String> map=new HashMap<String, String>();
            map.put("subject",subject[i]);
            map.put("date",date[i]);
            arraylia.add(map);

        }

        final Drawer_message_inbox_listadapter adapter = new Drawer_message_inbox_listadapter(getActivity(),
                arraylia);
        list.setAdapter(adapter);


       /* list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {





                LayoutInflater in = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View vv = in.inflate(R.layout.delete_popup, null);

                final PopupWindow pop = new PopupWindow(vv, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

                pop.setOutsideTouchable(true);
                pop.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.edittext));
                pop.setFocusable(true);
                pop.showAtLocation(vv,Gravity.TOP,0,0);

                backArrow=(ImageView)vv.findViewById(R.id.arrow);
                delete=(ImageView)vv.findViewById(R.id.delete);
                Language=(ImageView)vv.findViewById(R.id.language);



                list.setSelector(R.color.list);


                backArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        pop.dismiss();

                    }
                });




                        Language.setOnClickListener(new Language_change(getActivity(),language_chande));


                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        arraylia.remove(position);

                        adapter.notifyDataSetChanged();

                        list.setSelector(R.color.white);
                        //list.clearChoices();
                        pop.dismiss();




                    }
                });

                pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {

                        adapter.notifyDataSetChanged();

                        list.setSelector(R.color.white);

                       // list.clearChoices();
                    }
                });


                return true;
            }
        });*/


        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        // Capture ListView item click


        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode,

                                                  int position, long id, boolean checked) {
                // Capture total checked items







                   // final int checkedCount = list.getCheckedItemCount();
                    // Set the  CAB title according to total checked items

                   // mode.setTitle(checkedCount + " Selected");
                    // Calls toggleSelection method from ListViewAdapter Class

                    adapter.toggleSelection(position);








            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {


                    case R.id.delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        SparseBooleanArray selected = adapter
                                .getSelectedIds();
                        Log.e("sise",String.valueOf(selected.size()));
                        // Captures all selected ids with a loop
                        //int b=selected.size()-1;
                        for (int i = (selected.size()-1); i>=0; i--) {
                            if (selected.valueAt(i)) {
                               // arraylia = adapter.
                                // Remove selected items following the ids
                                adapter.remove(selected.keyAt(i));
                                list.clearChoices();
                            }
                        }
                        // Close CAB
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {

                MenuInflater mn = getActivity().getMenuInflater();

                mn.inflate(R.menu.message_menu, menu);

              /*  LayoutInflater in = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View vv = in.inflate(R.layout.delete_popup, null);

                final PopupWindow pop = new PopupWindow(vv, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

                pop.setOutsideTouchable(true);
                pop.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.edittext));
                pop.setFocusable(true);
                pop.showAtLocation(vv, Gravity.TOP,0,0);

                backArrow=(ImageView)vv.findViewById(R.id.arrow);
                delete=(ImageView)vv.findViewById(R.id.delete);
                Language=(ImageView)vv.findViewById(R.id.language);*/


                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // TODO Auto-generated method stub
                adapter.removeSelection();
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }
        });





   /*     final Drawable upArrow = getResources().getDrawable(R.drawable.newl);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);*/






        return v;

    }
}