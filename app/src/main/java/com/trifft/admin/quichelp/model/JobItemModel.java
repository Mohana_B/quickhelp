package com.trifft.admin.quichelp.model;

import java.io.Serializable;

public class JobItemModel implements Serializable {
    private String job_id;
    private String job_status;
    private String pref_date;
    private String pref_time;
    private String price;
    private String payment_status;

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJob_status() {
        return job_status;
    }

    public void setJob_status(String job_status) {
        this.job_status = job_status;
    }

    public String getPref_date() {
        return pref_date;
    }

    public void setPref_date(String pref_date) {
        this.pref_date = pref_date;
    }

    public String getPref_time() {
        return pref_time;
    }

    public void setPref_time(String pref_time) {
        this.pref_time = pref_time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }
}
